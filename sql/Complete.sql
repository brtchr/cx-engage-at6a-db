USE [AT6A_VOKAL]
GO
/****** Object:  Schema [aggregated]    Script Date: 12.04.2019 13.14.32 ******/
CREATE SCHEMA [aggregated]
GO
/****** Object:  Schema [at]    Script Date: 12.04.2019 13.14.32 ******/
CREATE SCHEMA [at]
GO
/****** Object:  Schema [export]    Script Date: 12.04.2019 13.14.32 ******/
CREATE SCHEMA [export]
GO
/****** Object:  Schema [external]    Script Date: 12.04.2019 13.14.32 ******/
CREATE SCHEMA [external]
GO
/****** Object:  Schema [org]    Script Date: 12.04.2019 13.14.32 ******/
CREATE SCHEMA [org]
GO
/****** Object:  Schema [raw]    Script Date: 12.04.2019 13.14.32 ******/
CREATE SCHEMA [raw]
GO
/****** Object:  Table [at].[Survey]    Script Date: 12.04.2019 13.14.32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[Survey](
	[SurveyID] [int] IDENTITY(1,1) NOT NULL,
	[ActivityID] [int] NOT NULL,
	[HierarchyID] [int] NULL,
	[StartDate] [smalldatetime] NOT NULL,
	[EndDate] [smalldatetime] NOT NULL,
	[Anonymous] [smallint] NOT NULL,
	[Status] [smallint] NOT NULL,
	[ShowBack] [bit] NOT NULL,
	[LanguageID] [int] NOT NULL,
	[ButtonPlacement] [smallint] NOT NULL,
	[UsePageNo] [bit] NOT NULL,
	[LinkURL] [nvarchar](512) NOT NULL,
	[FinishURL] [nvarchar](512) NOT NULL,
	[CreateResult] [bit] NOT NULL,
	[ReportDB] [varchar](64) NOT NULL,
	[ReportServer] [varchar](64) NOT NULL,
	[StyleSheet] [varchar](128) NOT NULL,
	[Type] [smallint] NOT NULL,
	[Created] [smalldatetime] NOT NULL,
	[LastProcessed] [datetime] NOT NULL,
	[ReProcessOLAP] [smallint] NOT NULL,
	[No] [smallint] NOT NULL,
	[OLAPServer] [nvarchar](64) NOT NULL,
	[OLAPDB] [nvarchar](64) NOT NULL,
	[PeriodID] [int] NULL,
	[ProcessCategorys] [smallint] NOT NULL,
	[DeleteResultOnUserDelete] [bit] NOT NULL,
	[FromSurveyID] [int] NULL,
	[LastProcessedResultID] [bigint] NULL,
	[LastProcessedAnswerID] [bigint] NULL,
	[ExtId] [nvarchar](256) NOT NULL,
	[Tag] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_Survey] PRIMARY KEY CLUSTERED 
(
	[SurveyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Answer]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Answer](
	[AnswerID] [bigint] IDENTITY(1,1) NOT NULL,
	[ResultID] [bigint] NOT NULL,
	[QuestionID] [int] NOT NULL,
	[AlternativeID] [int] NOT NULL,
	[Value] [float] NOT NULL,
	[DateValue] [datetime] NULL,
	[No] [smallint] NOT NULL,
	[ItemID] [int] NULL,
	[Free] [nvarchar](max) NOT NULL,
	[CustomerID] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[LastUpdatedBy] [int] NULL,
	[Created] [datetime] NULL,
	[CreatedBy] [int] NULL,
 CONSTRAINT [PK_Answer] PRIMARY KEY CLUSTERED 
(
	[AnswerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Result]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Result](
	[ResultID] [bigint] IDENTITY(1,1) NOT NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[RoleID] [int] NOT NULL,
	[UserID] [int] NULL,
	[UserGroupID] [int] NULL,
	[SurveyID] [int] NOT NULL,
	[BatchID] [int] NOT NULL,
	[LanguageID] [int] NOT NULL,
	[PageNo] [smallint] NOT NULL,
	[DepartmentID] [int] NOT NULL,
	[Anonymous] [smallint] NOT NULL,
	[ShowBack] [bit] NOT NULL,
	[ResultKey] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NOT NULL,
	[chk] [timestamp] NOT NULL,
	[Created] [datetime] NOT NULL,
	[LastUpdated] [datetime] NOT NULL,
	[LastUpdatedBy] [int] NOT NULL,
	[StatusTypeID] [int] NULL,
	[ValidFrom] [datetime2](7) NULL,
	[ValidTo] [datetime2](7) NULL,
	[DueDate] [datetime2](7) NULL,
	[ParentResultID] [bigint] NULL,
	[ParentResultSurveyID] [int] NULL,
	[CustomerID] [int] NULL,
	[CreatedBy] [int] NULL,
	[Deleted] [datetime2](7) NULL,
	[EntityStatusID] [int] NULL,
	[EntityStatusReasonID] [int] NULL,
 CONSTRAINT [Result_PK] PRIMARY KEY CLUSTERED 
(
	[ResultID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [at].[Period]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[Period](
	[PeriodID] [int] IDENTITY(1,1) NOT NULL,
	[Ownerid] [int] NOT NULL,
	[Created] [smalldatetime] NOT NULL,
	[No] [smallint] NOT NULL,
	[Startdate] [datetime] NULL,
	[Enddate] [datetime] NULL,
	[ExtID] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_Period] PRIMARY KEY CLUSTERED 
(
	[PeriodID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [external].[NasjonaleProver8]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE view [external].[NasjonaleProver8]
AS
Select p.extid as SkoleAar,r.departmentid,'9:' + cast(r.customerid as nvarchar(8)) as refererToken, count(r.resultid) As AntallElever, 
    round(isnull(Avg(a.value),0),2) as SnittSkalapoengLesing,Sum(a.value) as SumSkalapoengLesing,Count(a.value) as AntallSkalapoengLesing,
	round(isnull(Avg(a2.value),0),2) as SnittSkalapoengEngelsk,Sum(a2.value) as SumSkalapoengEngelsk,Count(a2.value) as AntallSkalapoengEngelsk,
	round(isnull(Avg(a3.value),0),2) as SnittSkalapoengRegning,isnull(Sum(a3.value),0) as SumSkalapoengRegning,Count(a3.value) as AntallSkalapoengRegning
from dbo.result r
Join at.survey s on r.surveyid = s.surveyid and s.activityid = 783
join at.period p on s.periodid = p.periodid
left outer Join dbo.answer a on r.resultid = a.resultid and a.questionid = 78446 and a.alternativeid = 115547  
left outer Join dbo.answer a2 on r.resultid = a2.resultid and a2.questionid = 78447 and a2.alternativeid = 115535  
 left outer Join dbo.answer a3 on r.resultid = a3.resultid and a3.questionid = 78448 and a3.alternativeid = 115543   
 where r.CustomerID = 50
group by p.extid,r.departmentid,'9:' + cast(r.customerid as nvarchar(8))

GO
/****** Object:  Table [at].[Batch]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[Batch](
	[BatchID] [int] IDENTITY(1,1) NOT NULL,
	[SurveyID] [int] NOT NULL,
	[UserID] [int] NULL,
	[DepartmentID] [int] NULL,
	[Name] [nvarchar](256) NOT NULL,
	[No] [smallint] NOT NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Status] [smallint] NOT NULL,
	[MailStatus] [smallint] NOT NULL,
	[Created] [smalldatetime] NOT NULL,
	[ExtID] [nvarchar](256) NOT NULL,
 CONSTRAINT [TMP_PK_Batch] PRIMARY KEY CLUSTERED 
(
	[BatchID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [external].[NasjonaleProver_8_trinn_2014]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [external].[NasjonaleProver_8_trinn_2014]
as
Select r.userid, p.extid as SkoleAar,b.Departmentid,'9:' + cast(r.customerid as nvarchar(8)) as refererToken,r.customerid,
isnull(a.value,0) 'LesingPoeng',
isnull(a2.value,0) 'EngelskPoeng',
isnull(a3.value,0) 'RegningPoeng',
isnull(a4.value,0) 'LesingMestringsnivå',
isnull(a5.value,0) 'EngelskMestringsnivå',
isnull(a6.value,0) 'RegningMestringsnivå'
from dbo.result r
Join at.survey s on r.surveyid = s.surveyid and s.activityid = 783 and extid <> 'GLOBAL'
Join at.batch b on b.batchid = r.batchid
join at.period p on s.periodid = p.periodid
left outer Join dbo.answer a on r.resultid = a.resultid and a.questionid = 78449    
left outer Join dbo.answer a2 on r.resultid = a2.resultid and a2.questionid = 78450
left outer Join dbo.answer a3 on r.resultid = a3.resultid and a3.questionid = 78451  
left outer Join dbo.answer a4 on r.resultid = a4.resultid and a4.questionid = 78452 
left outer Join dbo.answer a5 on r.resultid = a5.resultid and a5.questionid = 78453
left outer Join dbo.answer a6 on r.resultid = a6.resultid and a6.questionid = 78454 
where a.value is not null and isnull(r.userid,0) > 0
GO
/****** Object:  View [external].[NasjonaleProver9]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE view [external].[NasjonaleProver9]
AS
Select p.extid as SkoleAar,r.departmentid,'9:' + cast(r.customerid as nvarchar(8)) as refererToken, count(r.resultid) As AntallElever, 
    round(isnull(Avg(a.value),0),2) as SnittSkalapoengLesing,Sum(a.value) as SumSkalapoengLesing,Count(a.value) as AntallSkalapoengLesing,
	round(isnull(Avg(a3.value),0),2) as SnittSkalapoengRegning,Sum(a3.value) as SumSkalapoengRegning,Count(a3.value) as AntallSkalapoengRegning
from dbo.result r
Join at.survey s on r.surveyid = s.surveyid and s.activityid = 784
join at.period p on s.periodid = p.periodid
left outer Join dbo.answer a on r.resultid = a.resultid and a.questionid = 78472 and a.alternativeid = 115580   
 left outer Join dbo.answer a3 on r.resultid = a3.resultid and a3.questionid = 78474 and a3.alternativeid = 115576 
 where r.CustomerID = 50   
group by p.extid,r.departmentid,'9:' + cast(r.customerid as nvarchar(8))
GO
/****** Object:  View [external].[NasjonaleProver_9_trinn_2014]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [external].[NasjonaleProver_9_trinn_2014]
as
Select r.userid, p.extid as SkoleAar,b.Departmentid,'9:' + cast(r.customerid as nvarchar(8)) as refererToken,r.customerid,
isnull(a.value,0) 'LesingPoeng',
isnull(a3.value,0) 'RegningPoeng',
isnull(a4.value,0) 'LesingMestringsnivå',
isnull(a6.value,0) 'RegningMestringsnivå'
from dbo.result r
Join at.survey s on r.surveyid = s.surveyid and s.activityid = 784 and extid <> 'GLOBAL'
Join at.batch b on b.batchid = r.batchid
join at.period p on s.periodid = p.periodid
left outer Join dbo.answer a on r.resultid = a.resultid and a.questionid = 78475    
left outer Join dbo.answer a3 on r.resultid = a3.resultid and a3.questionid = 78477  
left outer Join dbo.answer a4 on r.resultid = a4.resultid and a4.questionid = 78478 
left outer Join dbo.answer a6 on r.resultid = a6.resultid and a6.questionid = 78480 
where a.value is not null and isnull(r.userid,0) > 0
GO
/****** Object:  View [external].[Leseferdighet1]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE view [external].[Leseferdighet1]
AS
Select p.extid as SkoleAar,r.departmentid,'9:' + cast(r.customerid as nvarchar(8)) as refererToken,r.customerid, count(r.resultid) As AntallElever
  , count(a.value) as 'SkriveBokstaverUnder'
  , count(a2.value) as 'SkriveBokstaverOver'
   , count(a3.value) as 'FinneLyderUnder'
  , count(a4.value) as 'FinneLyderOver'
    , count(a5.value) as 'TrekkeLyderSammenUnder'
  , count(a6.value) as 'TrekkeLyderSammenOver'
     , count(a7.value) as 'StaveOrdUnder'
  , count(a8.value) as 'StaveOrdOver'
     , count(a9.value) as 'LeseOrdUnder'
  , count(a10.value) as 'LeseOrdOver'
       , count(a11.value) as 'LeseErAaForstaaUnder'
  , count(a12.value) as 'LeseErAaForstaaOver'
from dbo.result r
Join at.survey s on r.surveyid = s.surveyid and s.activityid = 691 and extid <> 'GLOBAL'
join at.period p on s.periodid = p.periodid
left outer Join dbo.answer a on r.resultid = a.resultid and a.questionid = 70337 and a.alternativeid = 105488 
left outer Join dbo.answer a2 on r.resultid = a2.resultid and a2.questionid = 70337 and a2.alternativeid = 105489 
left outer Join dbo.answer a3 on r.resultid = a3.resultid and a3.questionid = 70338 and a3.alternativeid = 105488 
left outer Join dbo.answer a4 on r.resultid = a4.resultid and a4.questionid = 70338 and a4.alternativeid = 105489 
left outer Join dbo.answer a5 on r.resultid = a5.resultid and a5.questionid = 70339 and a5.alternativeid = 105488 
left outer Join dbo.answer a6 on r.resultid = a6.resultid and a6.questionid = 70339 and a6.alternativeid = 105489 
left outer Join dbo.answer a7 on r.resultid = a7.resultid and a7.questionid = 70340 and a7.alternativeid = 105488 
left outer Join dbo.answer a8 on r.resultid = a8.resultid and a8.questionid = 70340 and a8.alternativeid = 105489 
left outer Join dbo.answer a9 on r.resultid = a9.resultid and a9.questionid = 70341 and a9.alternativeid = 105488 
left outer Join dbo.answer a10 on r.resultid = a10.resultid and a10.questionid = 70341 and a10.alternativeid = 105489 
left outer Join dbo.answer a11 on r.resultid = a11.resultid and a11.questionid = 70341 and a11.alternativeid = 105488 
left outer Join dbo.answer a12 on r.resultid = a12.resultid and a12.questionid = 70341 and a12.alternativeid = 105489
where r.CustomerID = 50 
group by p.extid,r.departmentid,'9:' + cast(r.customerid as nvarchar(8)),r.customerid
GO
/****** Object:  Table [dbo].[ProveResultater]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProveResultater](
	[Periode] [nvarchar](256) NOT NULL,
	[Customerid] [int] NOT NULL,
	[Departmentid] [int] NULL,
	[Kunde] [nvarchar](4000) NULL,
	[Skole] [nvarchar](4000) NULL,
	[Forlag] [nvarchar](512) NOT NULL,
	[Hovedområde] [nvarchar](512) NOT NULL,
	[Underområde] [nvarchar](512) NOT NULL,
	[Prøve/Verktøy] [nvarchar](4000) NULL,
	[Trinn] [nvarchar](256) NOT NULL,
	[Måned] [int] NULL,
	[År] [int] NULL,
	[Resultater] [int] NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  View [external].[ProveResultater]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE   View [external].[ProveResultater]
AS
--select p.ExtID AS Periode, c.Customerid,
--       Replace(c.Name, ',', '') AS Kunde, 
--       Replace(b.Name, ',', '') AS Skole, 
--       isnull(lxForlag.Name, '') AS Forlag,
--       isnull(lxHoved.Name, '') AS Hovedområde,
--       isnull(lxUnder.Name, '') AS Underområde,
--       Replace(la.Name, ',', '') AS 'Prøve/Verktøy', 
--	   lr.Name AS Trinn,
--       DATEPART(month, r.Created) AS Måned, DATEPART(year, r.Created) AS År, 
--       count(r.ResultID) AS Resultater
--from dbo.Result r
--JOIN at.Survey s ON r.SurveyID = s.SurveyID
--JOIN at.LT_Activity la ON s.ActivityID = la.ActivityID AND la.LanguageID = 1
--JOIN at.Period p ON s.PeriodID = p.PeriodID
--JOIN at.Batch b ON r.BatchID = b.BatchID
--Join org.Department d on d.DepartmentID = r.DepartmentID
--JOIN org.Customer c ON d.CustomerID = c.CustomerID
--JOIN at.LT_Role lr ON r.RoleID = lr.RoleID AND lr.LanguageID = 1
--LEFT JOIN at.XC_A xForlag ON s.ActivityID = xForlag.ActivityID AND xForlag.XCID IN (105, 109, 115)
--LEFT JOIN at.LT_XCategory lxForlag ON xForlag.XCID = lxForlag.XCID AND lxForlag.LanguageID = 1
--LEFT JOIN at.XC_A xHoved ON s.ActivityID = xHoved.ActivityID AND xHoved.XCID IN (80, 154, 147, 159, 175, 307, 319)
--LEFT JOIN at.LT_XCategory lxHoved ON xHoved.XCID = lxHoved.XCID AND lxHoved.LanguageID = 1
--LEFT JOIN at.XC_A xUnder ON s.ActivityID = xUnder.ActivityID AND xUnder.XCID IN (SELECT x.XCID FROM at.XCategory x WHERE x.ParentID = xHoved.XCID )
--LEFT JOIN at.LT_XCategory lxUnder ON xUnder.XCID = lxUnder.XCID AND lxUnder.LanguageID = 1
--WHERE  r.EntityStatusID = 1 AND r.StatusTypeID IN (1,4,6)
--GROUP BY p.ExtID,c.customerid, c.Name, b.Name, lxForlag.Name, lxHoved.Name, isnull(lxUnder.Name, ''), la.Name, lr.Name, DATEPART(month, r.Created), DATEPART(year, r.Created)
--ORDER BY p.ExtID, c.Name, b.Name, la.Name


Select id,Periode, Customerid, Kunde,isnull(departmentid,0) as Departmentid, Skole, Forlag, Hovedområde, Underområde, [Prøve/Verktøy], Trinn, Måned, År, Resultater
from [dbo].[ProveResultater]
--where customerid = 50
GO
/****** Object:  Table [org].[Customer]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [org].[Customer](
	[CustomerID] [int] IDENTITY(1,1) NOT NULL,
	[OwnerID] [int] NOT NULL,
	[LanguageID] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Status] [smallint] NOT NULL,
	[ExtID] [nvarchar](256) NOT NULL,
	[Created] [smalldatetime] NOT NULL,
	[HasUserIntegration] [bit] NOT NULL,
	[RootMenuID] [int] NULL,
	[CodeName] [nvarchar](256) NULL,
	[Logo] [nvarchar](max) NULL,
	[CssVariables] [nvarchar](max) NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [external].[Customer]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE view [external].[Customer]
as
  select distinct c.CustomerId, c.Name , '9:' + cast(c.customerid as nvarchar(8)) as referertoken
  from org.Customer c
 
  
GO
/****** Object:  Table [dbo].[Loginstats]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Loginstats](
	[year] [int] NULL,
	[week] [int] NULL,
	[CustomerID] [int] NULL,
	[Customer] [varchar](250) NULL,
	[SchoolOwner] [int] NULL,
	[UniqeSchoolOwner] [int] NULL,
	[SchoolOwners] [int] NULL,
	[Teacher] [int] NULL,
	[UniqueTeacher] [int] NULL,
	[Teachers] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  View [external].[LoginStats]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE   View [external].[LoginStats]
AS
Select l.[year], l.week, l.CustomerID, l.Customer, l.SchoolOwner, l.UniqeSchoolOwner, l.SchoolOwners, l.Teacher, l.UniqueTeacher, l.Teachers
from [dbo].[Loginstats] l

GO
/****** Object:  Table [org].[TMP_UserGroup]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [org].[TMP_UserGroup](
	[UserGroupID] [int] IDENTITY(1,1) NOT NULL,
	[OwnerID] [int] NOT NULL,
	[DepartmentID] [int] NULL,
	[SurveyId] [int] NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[Created] [smalldatetime] NOT NULL,
	[ExtID] [nvarchar](256) NOT NULL,
	[PeriodID] [int] NULL,
	[UserID] [int] NULL,
	[UserGroupTypeID] [int] NULL,
	[Tag] [nvarchar](max) NOT NULL,
	[EntityVersion] [timestamp] NOT NULL,
	[LastUpdated] [datetime2](7) NOT NULL,
	[LastUpdatedBy] [int] NULL,
	[LastSynchronized] [datetime2](7) NOT NULL,
	[ArchetypeID] [int] NULL,
	[Deleted] [datetime2](7) NULL,
	[EntityStatusID] [int] NULL,
	[EntityStatusReasonID] [int] NULL,
 CONSTRAINT [PK_TMP_UserGroup] PRIMARY KEY CLUSTERED 
(
	[UserGroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [org].[TMP_UG_U]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [org].[TMP_UG_U](
	[UserGroupID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
 CONSTRAINT [PK_TMP_UG_U] PRIMARY KEY CLUSTERED 
(
	[UserGroupID] ASC,
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [org].[TMP_user_refererToken]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





Create View [org].[TMP_user_refererToken]
AS
Select  distinct ugu.userid,
SUBSTRING(extid,charindex('=',extid)+1,charindex('&',extid)-charindex('=',extid)-1) AS refererToken 
from org.[TMP_usergroup] ug
join org.TMP_ug_u ugu on ug.usergroupid = ugu.UserGroupID
where usergrouptypeid = 6 --and (ExtID like '%/corporate' or name = 'findmynext.com')
GO
/****** Object:  Table [org].[UG_U]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [org].[UG_U](
	[UserGroupID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
 CONSTRAINT [PK_UG_U] PRIMARY KEY CLUSTERED 
(
	[UserGroupID] ASC,
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [org].[UserGroup]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [org].[UserGroup](
	[UserGroupID] [int] IDENTITY(1,1) NOT NULL,
	[OwnerID] [int] NOT NULL,
	[DepartmentID] [int] NULL,
	[SurveyId] [int] NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[Created] [smalldatetime] NOT NULL,
	[ExtID] [nvarchar](64) NOT NULL,
	[PeriodID] [int] NULL,
	[UserID] [int] NULL,
	[UserGroupTypeID] [int] NULL,
	[Tag] [nvarchar](max) NOT NULL,
	[EntityVersion] [timestamp] NOT NULL,
	[LastUpdated] [datetime2](7) NOT NULL,
	[LastUpdatedBy] [int] NULL,
	[LastSynchronized] [datetime2](7) NOT NULL,
	[ArchetypeID] [int] NULL,
	[Deleted] [datetime2](7) NULL,
	[EntityStatusID] [int] NULL,
	[EntityStatusReasonID] [int] NULL,
 CONSTRAINT [PK_UserGroup] PRIMARY KEY CLUSTERED 
(
	[UserGroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [org].[user_refererToken]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




Create View [org].[user_refererToken]
AS
Select  distinct ugu.userid,
SUBSTRING(extid,charindex('=',extid)+1,charindex('&',extid)-charindex('=',extid)-1) AS refererToken 
from org.[usergroup] ug
join org.ug_u ugu on ug.usergroupid = ugu.UserGroupID
where usergrouptypeid = 6 --and (ExtID like '%/corporate' or name = 'findmynext.com')
GO
/****** Object:  View [external].[Leseferdighet_1_trinn_2014_2018]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [external].[Leseferdighet_1_trinn_2014_2018]
as
Select r.userid, p.extid as SkoleAar,b.Departmentid,'9:' + cast(r.customerid as nvarchar(8)) as refererToken,r.customerid,
isnull(a.value,0) 'AaSkriveBokstaver',
isnull(a2.value,0) 'AaFinneLyderIOrd',
isnull(a3.value,0) 'AATrekkeLyderSammenTilOrd',
isnull(a4.value,0) 'AaStaveOrd',
isnull(a5.value,0) 'AaLeseOrd',
isnull(a6.value,0) 'AaLeseErAaForstå'
from dbo.result r
Join at.survey s on r.surveyid = s.surveyid and s.activityid = 691 and extid <> 'GLOBAL'
Join at.batch b on b.batchid = r.batchid
join at.period p on s.periodid = p.periodid
left outer Join dbo.answer a on r.resultid = a.resultid and a.questionid = 70218  
left outer Join dbo.answer a2 on r.resultid = a2.resultid and a2.questionid = 70219 
left outer Join dbo.answer a3 on r.resultid = a3.resultid and a3.questionid = 70220 
left outer Join dbo.answer a4 on r.resultid = a4.resultid and a4.questionid = 70221 
left outer Join dbo.answer a5 on r.resultid = a5.resultid and a5.questionid = 70222 
left outer Join dbo.answer a6 on r.resultid = a6.resultid and a6.questionid = 70223 
where a.value is not null and isnull(r.userid,0) > 0

GO
/****** Object:  View [org].[Usergroup_Referer]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE View [org].[Usergroup_Referer]
AS
Select  distinct usergroupid, SUBSTRING(extid,charindex('=',extid)+1,charindex('&',extid)-charindex('=',extid)-1) AS refererToken 
from org.[usergroup] where usergrouptypeid = 6 and (ExtID like '%/corporate')
GO
/****** Object:  Table [org].[User]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [org].[User](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[Ownerid] [int] NOT NULL,
	[DepartmentID] [int] NOT NULL,
	[LanguageID] [int] NOT NULL,
	[RoleID] [int] NULL,
	[UserName] [nvarchar](128) NOT NULL,
	[Password] [nvarchar](64) NOT NULL,
	[LastName] [nvarchar](64) NOT NULL,
	[FirstName] [nvarchar](64) NOT NULL,
	[Email] [nvarchar](256) NOT NULL,
	[Mobile] [nvarchar](16) NOT NULL,
	[ExtID] [nvarchar](256) NOT NULL,
	[Tag] [nvarchar](128) NOT NULL,
	[Locked] [smallint] NOT NULL,
	[Created] [smalldatetime] NOT NULL,
	[SSN] [nvarchar](64) NOT NULL,
	[ChangePassword] [bit] NOT NULL,
	[Gender] [smallint] NULL,
	[DateOfBirth] [date] NULL,
	[HashPassword] [nvarchar](128) NOT NULL,
	[SaltPassword] [nvarchar](64) NOT NULL,
	[OneTimePassword] [nvarchar](64) NOT NULL,
	[OTPExpireTime] [smalldatetime] NULL,
	[CountryCode] [int] NULL,
	[ForceUserLoginAgain] [bit] NOT NULL,
	[EntityVersion] [timestamp] NOT NULL,
	[LastUpdated] [datetime2](7) NOT NULL,
	[LastUpdatedBy] [int] NULL,
	[LastSynchronized] [datetime2](7) NOT NULL,
	[ArchetypeID] [int] NULL,
	[Deleted] [datetime2](7) NULL,
	[EntityStatusID] [int] NULL,
	[EntityStatusReasonID] [int] NULL,
	[CustomerID] [int] NULL,
 CONSTRAINT [PK_TMP_User] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [TMP_IX_User] UNIQUE NONCLUSTERED 
(
	[UserName] ASC,
	[Ownerid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [external].[Learner]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [external].[Learner]
as
Select  u.userid, u.customerid,'9:' + cast(u.customerid as nvarchar(8)) as refererToken, Case when Gender = 0 then 'Gutt' when Gender = 1 then 'Jente' else 'Ukjent' end as Gender,
 isnull(u.[DateOfBirth],'1900-01-01') as DateOfBirth
from org.[user] u
where archetypeid = 2
GO
/****** Object:  Table [org].[Department]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [org].[Department](
	[DepartmentID] [int] IDENTITY(1,1) NOT NULL,
	[LanguageID] [int] NOT NULL,
	[OwnerID] [int] NOT NULL,
	[CustomerID] [int] NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[Adress] [nvarchar](512) NOT NULL,
	[PostalCode] [nvarchar](32) NOT NULL,
	[City] [nvarchar](64) NOT NULL,
	[OrgNo] [varchar](16) NOT NULL,
	[Created] [smalldatetime] NOT NULL,
	[ExtID] [nvarchar](256) NOT NULL,
	[Tag] [nvarchar](max) NOT NULL,
	[Locked] [smallint] NOT NULL,
	[CountryCode] [int] NOT NULL,
	[EntityVersion] [timestamp] NOT NULL,
	[LastUpdated] [datetime2](7) NOT NULL,
	[LastUpdatedBy] [int] NULL,
	[LastSynchronized] [datetime2](7) NOT NULL,
	[ArchetypeID] [int] NULL,
	[Deleted] [datetime2](7) NULL,
	[EntityStatusID] [int] NULL,
	[EntityStatusReasonID] [int] NULL,
 CONSTRAINT [PK_Department] PRIMARY KEY CLUSTERED 
(
	[DepartmentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [org].[DT_D]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [org].[DT_D](
	[DepartmentTypeID] [int] NOT NULL,
	[DepartmentID] [int] NOT NULL,
 CONSTRAINT [PK_DT_D] PRIMARY KEY CLUSTERED 
(
	[DepartmentTypeID] ASC,
	[DepartmentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [external].[Department]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE view [external].[Department]
as
  select distinct d.DepartmentId,d.orgno, d.Name , '9:' + cast(d.customerid as nvarchar(8)) as referertoken,d.customerid
  from org.department d
  Join org.dt_d dtd on dtd.departmentid = d.departmentid and dtd.departmenttypeid  IN (35,36) 
  --where customerid = 50
  
GO
/****** Object:  View [external].[Leseferdighet_2_trinn_2014_2018]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [external].[Leseferdighet_2_trinn_2014_2018]
as
Select r.userid, p.extid as SkoleAar,b.Departmentid,'9:' + cast(r.customerid as nvarchar(8)) as refererToken,r.customerid,
isnull(a.value,0) 'AaLeseOrd',
isnull(a2.value,0) 'AaForstaaOrd',
isnull(a3.value,0) 'AaStaveOrd',
isnull(a4.value,0) 'AaLeseErAaForstaaDel'
from dbo.result r
Join at.survey s on r.surveyid = s.surveyid and s.activityid = 696 and extid <> 'GLOBAL'
Join at.batch b on b.batchid = r.batchid
join at.period p on s.periodid = p.periodid
left outer Join dbo.answer a on r.resultid = a.resultid and a.questionid = 70615   
left outer Join dbo.answer a2 on r.resultid = a2.resultid and a2.questionid = 70616
left outer Join dbo.answer a3 on r.resultid = a3.resultid and a3.questionid = 70617 
left outer Join dbo.answer a4 on r.resultid = a4.resultid and a4.questionid = 70618 
where a.value is not null and isnull(r.userid,0) > 0
GO
/****** Object:  View [external].[Leseferdighet_3_trinn_2014_2018]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [external].[Leseferdighet_3_trinn_2014_2018]
as
Select r.userid, p.extid as SkoleAar,b.Departmentid,'9:' + cast(r.customerid as nvarchar(8)) as refererToken,r.customerid,
isnull(a.value,0) 'AaLeseOrd',
isnull(a2.value,0) 'AaStaveOrd',
isnull(a3.value,0) 'AaForstaaOrd ',
isnull(a4.value,0) 'AaLeseErAaForstaa'
from dbo.result r
Join at.survey s on r.surveyid = s.surveyid and s.activityid = 698 and extid <> 'GLOBAL'
Join at.batch b on b.batchid = r.batchid
join at.period p on s.periodid = p.periodid
left outer Join dbo.answer a on r.resultid = a.resultid and a.questionid = 70782   
left outer Join dbo.answer a2 on r.resultid = a2.resultid and a2.questionid = 70783
left outer Join dbo.answer a3 on r.resultid = a3.resultid and a3.questionid = 70784 
left outer Join dbo.answer a4 on r.resultid = a4.resultid and a4.questionid = 70785 
where a.value is not null and isnull(r.userid,0) > 0
GO
/****** Object:  View [external].[NasjonaleProver5]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [external].[NasjonaleProver5]
AS
Select p.extid as SkoleAar,r.departmentid,'9:' + cast(r.customerid as nvarchar(8)) as refererToken, 
    count(r.resultid) As AntallElever, 
    round(Avg(isnull(a.value,0)),2) as SnittSkalapoengLesing,Sum(a.value) as SumSkalapoengLesing,Count(a.value) as AntallSkalapoengLesing,
	round(Avg(a2.value),2) as SnittSkalapoengEngelsk,Sum(a2.value) as SumSkalapoengEngelsk,Count(a2.value) as AntallSkalapoengEngelsk,
	round(Avg(a3.value),2) as SnittSkalapoengRegning,Sum(a3.value) as SumSkalapoengRegning,Count(a3.value) as AntallSkalapoengRegning
from dbo.result r
Join at.survey s on r.surveyid = s.surveyid and s.activityid = 779
join at.period p on s.periodid = p.periodid
left outer Join dbo.answer a on r.resultid = a.resultid and a.questionid = 78220 and a.alternativeid = 115158 
left outer Join dbo.answer a2 on r.resultid = a2.resultid and a2.questionid = 78221 and a2.alternativeid = 115136 
 left outer Join dbo.answer a3 on r.resultid = a3.resultid and a3.questionid = 78222 and a3.alternativeid = 115149  
 where r.CustomerID = 50
group by p.extid,r.departmentid,'9:' + cast(r.customerid as nvarchar(8))
GO
/****** Object:  View [external].[NasjonaleProver_5_trinn_2014]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [external].[NasjonaleProver_5_trinn_2014]
as
Select r.userid, p.extid as SkoleAar,b.Departmentid,'9:' + cast(r.customerid as nvarchar(8)) as refererToken,r.customerid,
isnull(a.value,0) 'LesingPoeng',
isnull(a2.value,0) 'EngelskPoeng',
isnull(a3.value,0) 'RegningPoeng',
isnull(a4.value,0) 'LesingMestringsnivå',
isnull(a5.value,0) 'EngelskMestringsnivå',
isnull(a6.value,0) 'RegningMestringsnivå'
from dbo.result r
Join at.survey s on r.surveyid = s.surveyid and s.activityid = 779 and extid <> 'GLOBAL'
Join at.batch b on b.batchid = r.batchid
join at.period p on s.periodid = p.periodid
left outer Join dbo.answer a on r.resultid = a.resultid and a.questionid = 78223    
left outer Join dbo.answer a2 on r.resultid = a2.resultid and a2.questionid = 78224
left outer Join dbo.answer a3 on r.resultid = a3.resultid and a3.questionid = 78225  
left outer Join dbo.answer a4 on r.resultid = a4.resultid and a4.questionid = 78236 
left outer Join dbo.answer a5 on r.resultid = a5.resultid and a5.questionid = 78237
left outer Join dbo.answer a6 on r.resultid = a6.resultid and a6.questionid = 78238 
where a.value is not null and isnull(r.userid,0) > 0
GO
/****** Object:  Table [at].[Activity]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[Activity](
	[ActivityID] [int] IDENTITY(1,1) NOT NULL,
	[LanguageID] [int] NOT NULL,
	[OwnerID] [int] NOT NULL,
	[StyleSheet] [nvarchar](128) NOT NULL,
	[Type] [smallint] NOT NULL,
	[No] [smallint] NOT NULL,
	[TooltipType] [smallint] NOT NULL,
	[Listview] [smallint] NOT NULL,
	[Descview] [smallint] NOT NULL,
	[Chartview] [smallint] NOT NULL,
	[OLAPServer] [nvarchar](64) NOT NULL,
	[OLAPDB] [nvarchar](64) NOT NULL,
	[Created] [smalldatetime] NOT NULL,
	[SelectionHeaderType] [smallint] NOT NULL,
	[ReportServer] [nvarchar](64) NOT NULL,
	[ReportDB] [nvarchar](64) NOT NULL,
	[ExtID] [nvarchar](256) NOT NULL,
	[UseOLAP] [bit] NULL,
	[ArchetypeID] [int] NULL,
 CONSTRAINT [TMP_PK_Activity] PRIMARY KEY CLUSTERED 
(
	[ActivityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [at].[Alternative]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[Alternative](
	[AlternativeID] [int] IDENTITY(1,1) NOT NULL,
	[ScaleID] [int] NOT NULL,
	[AGID] [int] NULL,
	[Type] [smallint] NOT NULL,
	[No] [smallint] NOT NULL,
	[Value] [float] NOT NULL,
	[InvertedValue] [float] NOT NULL,
	[Calc] [bit] NOT NULL,
	[SC] [bit] NOT NULL,
	[MinValue] [float] NOT NULL,
	[MaxValue] [float] NOT NULL,
	[Format] [varchar](64) NOT NULL,
	[Size] [smallint] NOT NULL,
	[CssClass] [varchar](64) NOT NULL,
	[Created] [smalldatetime] NOT NULL,
	[DefaultValue] [nvarchar](128) NOT NULL,
	[Tag] [nvarchar](128) NOT NULL,
	[ExtID] [nvarchar](256) NOT NULL,
	[Width] [nvarchar](32) NOT NULL,
	[ParentID] [int] NULL,
	[OwnerColorID] [int] NULL,
	[DefaultCalcType] [smallint] NOT NULL,
	[UseEncryption] [bit] NOT NULL,
 CONSTRAINT [TMP_Alternative_PK] PRIMARY KEY CLUSTERED 
(
	[AlternativeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [at].[Category]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[Category](
	[CategoryID] [int] IDENTITY(1,1) NOT NULL,
	[ActivityID] [int] NOT NULL,
	[ParentID] [int] NULL,
	[Tag] [nvarchar](128) NOT NULL,
	[Type] [smallint] NOT NULL,
	[No] [smallint] NOT NULL,
	[Fillcolor] [varchar](16) NOT NULL,
	[BorderColor] [varchar](16) NOT NULL,
	[Weight] [float] NOT NULL,
	[Created] [smalldatetime] NOT NULL,
	[NType] [smallint] NOT NULL,
	[ProcessCategory] [bit] NOT NULL,
	[ExtID] [nvarchar](256) NOT NULL,
	[AnswerItemID] [int] NULL,
	[RenderAsDefault] [bit] NOT NULL,
	[AvgType] [smallint] NOT NULL,
	[DefaultState] [bit] NOT NULL,
 CONSTRAINT [TMP_PK_Category] PRIMARY KEY CLUSTERED 
(
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [at].[LevelGroup]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[LevelGroup](
	[LevelGroupID] [int] IDENTITY(1,1) NOT NULL,
	[ActivityID] [int] NOT NULL,
	[Tag] [nvarchar](128) NOT NULL,
	[No] [smallint] NOT NULL,
	[CustomerID] [int] NULL,
	[Departmentid] [int] NULL,
	[Roleid] [int] NULL,
	[Created] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_LevelGroup] PRIMARY KEY CLUSTERED 
(
	[LevelGroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [at].[LevelLimit]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[LevelLimit](
	[LevelLimitID] [int] IDENTITY(1,1) NOT NULL,
	[LevelGroupID] [int] NULL,
	[CategoryID] [int] NULL,
	[QuestionID] [int] NULL,
	[AlternativeID] [int] NULL,
	[MinValue] [float] NOT NULL,
	[MaxValue] [float] NOT NULL,
	[Sigchange] [float] NOT NULL,
	[OwnerColorID] [int] NOT NULL,
	[Created] [smalldatetime] NOT NULL,
	[NegativeTrend] [bit] NOT NULL,
	[ExtID] [nvarchar](256) NOT NULL,
	[ItemID] [int] NULL,
	[MatchingType] [int] NOT NULL,
 CONSTRAINT [PK_LevelLimit] PRIMARY KEY CLUSTERED 
(
	[LevelLimitID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [at].[LT_Activity]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[LT_Activity](
	[LanguageID] [int] NOT NULL,
	[ActivityID] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Info] [nvarchar](max) NOT NULL,
	[StartText] [nvarchar](max) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[RoleName] [nvarchar](128) NOT NULL,
	[SurveyName] [nvarchar](128) NOT NULL,
	[BatchName] [nvarchar](128) NOT NULL,
	[DisplayName] [nvarchar](256) NOT NULL,
	[ShortName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_TMP_LT_Activity] PRIMARY KEY CLUSTERED 
(
	[LanguageID] ASC,
	[ActivityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [at].[LT_Alternative]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[LT_Alternative](
	[LanguageID] [int] NOT NULL,
	[AlternativeID] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Label] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_LT_Alternative] PRIMARY KEY CLUSTERED 
(
	[LanguageID] ASC,
	[AlternativeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [at].[LT_Category]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[LT_Category](
	[LanguageID] [int] NOT NULL,
	[CategoryID] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[DescriptionExtended] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_TMP_LT_Category] PRIMARY KEY CLUSTERED 
(
	[LanguageID] ASC,
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [at].[LT_LevelGroup]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[LT_LevelGroup](
	[LanguageID] [int] NOT NULL,
	[LevelGroupID] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_LT_LevelGroup] PRIMARY KEY CLUSTERED 
(
	[LanguageID] ASC,
	[LevelGroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [at].[LT_LevelLimit]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[LT_LevelLimit](
	[LevelLimitID] [int] NOT NULL,
	[LanguageID] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_LT_LevelLimit] PRIMARY KEY CLUSTERED 
(
	[LevelLimitID] ASC,
	[LanguageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [at].[LT_Page]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[LT_Page](
	[LanguageID] [int] NOT NULL,
	[PageID] [int] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Info] [nvarchar](max) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_LT_Page] PRIMARY KEY CLUSTERED 
(
	[LanguageID] ASC,
	[PageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [at].[LT_Question]    Script Date: 12.04.2019 13.14.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[LT_Question](
	[LanguageID] [int] NOT NULL,
	[QuestionID] [int] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[ReportText] [nvarchar](512) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[ShortName] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_LT_Question] PRIMARY KEY CLUSTERED 
(
	[LanguageID] ASC,
	[QuestionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [at].[LT_Role]    Script Date: 12.04.2019 13.14.34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[LT_Role](
	[LanguageID] [int] NOT NULL,
	[RoleID] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_LT_Role] PRIMARY KEY CLUSTERED 
(
	[LanguageID] ASC,
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [at].[LT_Scale]    Script Date: 12.04.2019 13.14.34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[LT_Scale](
	[LanguageID] [int] NOT NULL,
	[ScaleID] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_LT_SCALE] PRIMARY KEY CLUSTERED 
(
	[LanguageID] ASC,
	[ScaleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [at].[LT_Survey]    Script Date: 12.04.2019 13.14.34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[LT_Survey](
	[LanguageID] [int] NOT NULL,
	[SurveyID] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[Info] [nvarchar](max) NOT NULL,
	[FinishText] [nvarchar](max) NOT NULL,
	[DisplayName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_LT_Execution] PRIMARY KEY CLUSTERED 
(
	[LanguageID] ASC,
	[SurveyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [at].[LT_XCategory]    Script Date: 12.04.2019 13.14.34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[LT_XCategory](
	[LanguageID] [int] NOT NULL,
	[XCID] [int] NOT NULL,
	[Name] [nvarchar](512) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_LT_XCategory] PRIMARY KEY CLUSTERED 
(
	[LanguageID] ASC,
	[XCID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [at].[Page]    Script Date: 12.04.2019 13.14.34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[Page](
	[PageID] [int] IDENTITY(1,1) NOT NULL,
	[ActivityID] [int] NULL,
	[No] [smallint] NOT NULL,
	[Created] [smalldatetime] NULL,
	[Type] [smallint] NOT NULL,
	[CssClass] [nvarchar](64) NOT NULL,
	[ExtID] [nvarchar](256) NOT NULL,
	[Tag] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_Page] PRIMARY KEY CLUSTERED 
(
	[PageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [at].[Q_C]    Script Date: 12.04.2019 13.14.34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[Q_C](
	[QuestionID] [int] NOT NULL,
	[CategoryID] [int] NOT NULL,
	[Weight] [float] NOT NULL,
	[No] [smallint] NOT NULL,
	[Visible] [smallint] NOT NULL,
	[ItemID] [int] NULL,
 CONSTRAINT [PK_Q_C] PRIMARY KEY CLUSTERED 
(
	[QuestionID] ASC,
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [at].[Question]    Script Date: 12.04.2019 13.14.34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[Question](
	[QuestionID] [int] IDENTITY(1,1) NOT NULL,
	[PageID] [int] NOT NULL,
	[ScaleID] [int] NULL,
	[No] [smallint] NOT NULL,
	[Type] [smallint] NOT NULL,
	[Inverted] [bit] NOT NULL,
	[Mandatory] [bit] NOT NULL,
	[Status] [smallint] NOT NULL,
	[CssClass] [nvarchar](64) NOT NULL,
	[ExtId] [nvarchar](256) NOT NULL,
	[Tag] [nvarchar](128) NOT NULL,
	[Created] [smalldatetime] NOT NULL,
	[SectionID] [int] NULL,
 CONSTRAINT [Question_PK] PRIMARY KEY CLUSTERED 
(
	[QuestionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [at].[Role]    Script Date: 12.04.2019 13.14.34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[Role](
	[RoleID] [int] IDENTITY(1,1) NOT NULL,
	[Ownerid] [int] NOT NULL,
	[Created] [smalldatetime] NOT NULL,
	[No] [smallint] NOT NULL,
	[Type] [smallint] NOT NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [at].[Scale]    Script Date: 12.04.2019 13.14.34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[Scale](
	[ScaleID] [int] IDENTITY(1,1) NOT NULL,
	[ActivityID] [int] NULL,
	[MinSelect] [smallint] NOT NULL,
	[MaxSelect] [smallint] NOT NULL,
	[Type] [smallint] NOT NULL,
	[MinValue] [float] NOT NULL,
	[MaxValue] [float] NOT NULL,
	[Created] [smalldatetime] NOT NULL,
	[Tag] [nvarchar](128) NOT NULL,
	[ExtID] [nvarchar](256) NOT NULL,
 CONSTRAINT [AlternativeGroup_PK] PRIMARY KEY CLUSTERED 
(
	[ScaleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [at].[TMP_Activity]    Script Date: 12.04.2019 13.14.34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[TMP_Activity](
	[ActivityID] [int] IDENTITY(1,1) NOT NULL,
	[LanguageID] [int] NOT NULL,
	[OwnerID] [int] NOT NULL,
	[StyleSheet] [nvarchar](128) NOT NULL,
	[Type] [smallint] NOT NULL,
	[No] [smallint] NOT NULL,
	[TooltipType] [smallint] NOT NULL,
	[Listview] [smallint] NOT NULL,
	[Descview] [smallint] NOT NULL,
	[Chartview] [smallint] NOT NULL,
	[OLAPServer] [nvarchar](64) NOT NULL,
	[OLAPDB] [nvarchar](64) NOT NULL,
	[Created] [smalldatetime] NOT NULL,
	[SelectionHeaderType] [smallint] NOT NULL,
	[ReportServer] [nvarchar](64) NOT NULL,
	[ReportDB] [nvarchar](64) NOT NULL,
	[ExtID] [nvarchar](256) NOT NULL,
	[UseOLAP] [bit] NULL,
	[ArchetypeID] [int] NULL,
 CONSTRAINT [PK_Activity] PRIMARY KEY CLUSTERED 
(
	[ActivityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [at].[TMP_Alternative]    Script Date: 12.04.2019 13.14.34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[TMP_Alternative](
	[AlternativeID] [int] IDENTITY(1,1) NOT NULL,
	[ScaleID] [int] NOT NULL,
	[AGID] [int] NULL,
	[Type] [smallint] NOT NULL,
	[No] [smallint] NOT NULL,
	[Value] [float] NOT NULL,
	[InvertedValue] [float] NOT NULL,
	[Calc] [bit] NOT NULL,
	[SC] [bit] NOT NULL,
	[MinValue] [float] NOT NULL,
	[MaxValue] [float] NOT NULL,
	[Format] [varchar](64) NOT NULL,
	[Size] [smallint] NOT NULL,
	[CssClass] [varchar](64) NOT NULL,
	[Created] [smalldatetime] NOT NULL,
	[DefaultValue] [nvarchar](128) NOT NULL,
	[Tag] [nvarchar](128) NOT NULL,
	[ExtID] [nvarchar](256) NOT NULL,
	[Width] [nvarchar](32) NOT NULL,
	[ParentID] [int] NULL,
	[OwnerColorID] [int] NULL,
	[DefaultCalcType] [smallint] NOT NULL,
	[UseEncryption] [bit] NOT NULL,
 CONSTRAINT [Alternative_PK] PRIMARY KEY CLUSTERED 
(
	[AlternativeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [at].[TMP_Batch]    Script Date: 12.04.2019 13.14.34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[TMP_Batch](
	[BatchID] [int] IDENTITY(1,1) NOT NULL,
	[SurveyID] [int] NOT NULL,
	[UserID] [int] NULL,
	[DepartmentID] [int] NULL,
	[Name] [nvarchar](256) NOT NULL,
	[No] [smallint] NOT NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Status] [smallint] NOT NULL,
	[MailStatus] [smallint] NOT NULL,
	[Created] [smalldatetime] NOT NULL,
	[ExtID] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_Batch] PRIMARY KEY CLUSTERED 
(
	[BatchID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [at].[TMP_Category]    Script Date: 12.04.2019 13.14.34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[TMP_Category](
	[CategoryID] [int] IDENTITY(1,1) NOT NULL,
	[ActivityID] [int] NOT NULL,
	[ParentID] [int] NULL,
	[Tag] [nvarchar](128) NOT NULL,
	[Type] [smallint] NOT NULL,
	[No] [smallint] NOT NULL,
	[Fillcolor] [varchar](16) NOT NULL,
	[BorderColor] [varchar](16) NOT NULL,
	[Weight] [float] NOT NULL,
	[Created] [smalldatetime] NOT NULL,
	[NType] [smallint] NOT NULL,
	[ProcessCategory] [bit] NOT NULL,
	[ExtID] [nvarchar](256) NOT NULL,
	[AnswerItemID] [int] NULL,
	[RenderAsDefault] [bit] NOT NULL,
	[AvgType] [smallint] NOT NULL,
	[DefaultState] [bit] NOT NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [at].[TMP_LevelGroup]    Script Date: 12.04.2019 13.14.34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[TMP_LevelGroup](
	[LevelGroupID] [int] IDENTITY(1,1) NOT NULL,
	[ActivityID] [int] NOT NULL,
	[Tag] [nvarchar](128) NOT NULL,
	[No] [smallint] NOT NULL,
	[CustomerID] [int] NULL,
	[Departmentid] [int] NULL,
	[Roleid] [int] NULL,
	[Created] [smalldatetime] NOT NULL,
 CONSTRAINT [TMP_PK_LevelGroup] PRIMARY KEY CLUSTERED 
(
	[LevelGroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [at].[TMP_LevelLimit]    Script Date: 12.04.2019 13.14.34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[TMP_LevelLimit](
	[LevelLimitID] [int] IDENTITY(1,1) NOT NULL,
	[LevelGroupID] [int] NULL,
	[CategoryID] [int] NULL,
	[QuestionID] [int] NULL,
	[AlternativeID] [int] NULL,
	[MinValue] [float] NOT NULL,
	[MaxValue] [float] NOT NULL,
	[Sigchange] [float] NOT NULL,
	[OwnerColorID] [int] NOT NULL,
	[Created] [smalldatetime] NOT NULL,
	[NegativeTrend] [bit] NOT NULL,
	[ExtID] [nvarchar](256) NOT NULL,
	[ItemID] [int] NULL,
	[MatchingType] [int] NOT NULL,
 CONSTRAINT [TMP_PK_LevelLimit] PRIMARY KEY CLUSTERED 
(
	[LevelLimitID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [at].[TMP_LT_Activity]    Script Date: 12.04.2019 13.14.34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[TMP_LT_Activity](
	[LanguageID] [int] NOT NULL,
	[ActivityID] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Info] [nvarchar](max) NOT NULL,
	[StartText] [nvarchar](max) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[RoleName] [nvarchar](128) NOT NULL,
	[SurveyName] [nvarchar](128) NOT NULL,
	[BatchName] [nvarchar](128) NOT NULL,
	[DisplayName] [nvarchar](256) NOT NULL,
	[ShortName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_LT_Activity] PRIMARY KEY CLUSTERED 
(
	[LanguageID] ASC,
	[ActivityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [at].[TMP_LT_Alternative]    Script Date: 12.04.2019 13.14.34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[TMP_LT_Alternative](
	[LanguageID] [int] NOT NULL,
	[AlternativeID] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Label] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_TMP_LT_Alternative] PRIMARY KEY CLUSTERED 
(
	[LanguageID] ASC,
	[AlternativeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [at].[TMP_LT_Category]    Script Date: 12.04.2019 13.14.34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[TMP_LT_Category](
	[LanguageID] [int] NOT NULL,
	[CategoryID] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[DescriptionExtended] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_LT_Category] PRIMARY KEY CLUSTERED 
(
	[LanguageID] ASC,
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [at].[TMP_LT_LevelGroup]    Script Date: 12.04.2019 13.14.34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[TMP_LT_LevelGroup](
	[LanguageID] [int] NOT NULL,
	[LevelGroupID] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_TMP_LT_LevelGroup] PRIMARY KEY CLUSTERED 
(
	[LanguageID] ASC,
	[LevelGroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [at].[TMP_LT_LevelLimit]    Script Date: 12.04.2019 13.14.34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[TMP_LT_LevelLimit](
	[LevelLimitID] [int] NOT NULL,
	[LanguageID] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_TMP_LT_LevelLimit] PRIMARY KEY CLUSTERED 
(
	[LevelLimitID] ASC,
	[LanguageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [at].[TMP_LT_Page]    Script Date: 12.04.2019 13.14.34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[TMP_LT_Page](
	[LanguageID] [int] NOT NULL,
	[PageID] [int] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Info] [nvarchar](max) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_TMP_LT_Page] PRIMARY KEY CLUSTERED 
(
	[LanguageID] ASC,
	[PageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [at].[TMP_LT_Question]    Script Date: 12.04.2019 13.14.34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[TMP_LT_Question](
	[LanguageID] [int] NOT NULL,
	[QuestionID] [int] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[ReportText] [nvarchar](512) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[ShortName] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_TMP_LT_Question] PRIMARY KEY CLUSTERED 
(
	[LanguageID] ASC,
	[QuestionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [at].[TMP_LT_Role]    Script Date: 12.04.2019 13.14.35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[TMP_LT_Role](
	[LanguageID] [int] NOT NULL,
	[RoleID] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_TMP_LT_Role] PRIMARY KEY CLUSTERED 
(
	[LanguageID] ASC,
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [at].[TMP_LT_Scale]    Script Date: 12.04.2019 13.14.35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[TMP_LT_Scale](
	[LanguageID] [int] NOT NULL,
	[ScaleID] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_TMP_LT_SCALE] PRIMARY KEY CLUSTERED 
(
	[LanguageID] ASC,
	[ScaleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [at].[TMP_LT_Survey]    Script Date: 12.04.2019 13.14.35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[TMP_LT_Survey](
	[LanguageID] [int] NOT NULL,
	[SurveyID] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[Info] [nvarchar](max) NOT NULL,
	[FinishText] [nvarchar](max) NOT NULL,
	[DisplayName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_TMP_LT_Execution] PRIMARY KEY CLUSTERED 
(
	[LanguageID] ASC,
	[SurveyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [at].[TMP_LT_XCategory]    Script Date: 12.04.2019 13.14.35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[TMP_LT_XCategory](
	[LanguageID] [int] NOT NULL,
	[XCID] [int] NOT NULL,
	[Name] [nvarchar](512) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
 CONSTRAINT [TMP_PK_LT_XCategory] PRIMARY KEY CLUSTERED 
(
	[LanguageID] ASC,
	[XCID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [at].[TMP_Page]    Script Date: 12.04.2019 13.14.35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[TMP_Page](
	[PageID] [int] IDENTITY(1,1) NOT NULL,
	[ActivityID] [int] NULL,
	[No] [smallint] NOT NULL,
	[Created] [smalldatetime] NULL,
	[Type] [smallint] NOT NULL,
	[CssClass] [nvarchar](64) NOT NULL,
	[ExtID] [nvarchar](256) NOT NULL,
	[Tag] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_TMP_Page] PRIMARY KEY CLUSTERED 
(
	[PageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [at].[TMP_Period]    Script Date: 12.04.2019 13.14.35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[TMP_Period](
	[PeriodID] [int] IDENTITY(1,1) NOT NULL,
	[Ownerid] [int] NOT NULL,
	[Created] [smalldatetime] NOT NULL,
	[No] [smallint] NOT NULL,
	[Startdate] [datetime] NULL,
	[Enddate] [datetime] NULL,
	[ExtID] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_TMP_Period] PRIMARY KEY CLUSTERED 
(
	[PeriodID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [at].[TMP_Q_C]    Script Date: 12.04.2019 13.14.35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[TMP_Q_C](
	[QuestionID] [int] NOT NULL,
	[CategoryID] [int] NOT NULL,
	[Weight] [float] NOT NULL,
	[No] [smallint] NOT NULL,
	[Visible] [smallint] NOT NULL,
	[ItemID] [int] NULL,
 CONSTRAINT [PK_TMP_Q_C] PRIMARY KEY CLUSTERED 
(
	[QuestionID] ASC,
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [at].[TMP_Question]    Script Date: 12.04.2019 13.14.35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[TMP_Question](
	[QuestionID] [int] IDENTITY(1,1) NOT NULL,
	[PageID] [int] NOT NULL,
	[ScaleID] [int] NULL,
	[No] [smallint] NOT NULL,
	[Type] [smallint] NOT NULL,
	[Inverted] [bit] NOT NULL,
	[Mandatory] [bit] NOT NULL,
	[Status] [smallint] NOT NULL,
	[CssClass] [nvarchar](64) NOT NULL,
	[ExtId] [nvarchar](256) NOT NULL,
	[Tag] [nvarchar](128) NOT NULL,
	[Created] [smalldatetime] NOT NULL,
	[SectionID] [int] NULL,
 CONSTRAINT [PK_TMP_Question] PRIMARY KEY CLUSTERED 
(
	[QuestionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [at].[TMP_Role]    Script Date: 12.04.2019 13.14.35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[TMP_Role](
	[RoleID] [int] IDENTITY(1,1) NOT NULL,
	[Ownerid] [int] NOT NULL,
	[Created] [smalldatetime] NOT NULL,
	[No] [smallint] NOT NULL,
	[Type] [smallint] NOT NULL,
 CONSTRAINT [PK_TMP_Role] PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [at].[TMP_Scale]    Script Date: 12.04.2019 13.14.35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[TMP_Scale](
	[ScaleID] [int] IDENTITY(1,1) NOT NULL,
	[ActivityID] [int] NULL,
	[MinSelect] [smallint] NOT NULL,
	[MaxSelect] [smallint] NOT NULL,
	[Type] [smallint] NOT NULL,
	[MinValue] [float] NOT NULL,
	[MaxValue] [float] NOT NULL,
	[Created] [smalldatetime] NOT NULL,
	[Tag] [nvarchar](128) NOT NULL,
	[ExtID] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_TMP_AlternativeGroup] PRIMARY KEY CLUSTERED 
(
	[ScaleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [at].[TMP_Survey]    Script Date: 12.04.2019 13.14.35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[TMP_Survey](
	[SurveyID] [int] IDENTITY(1,1) NOT NULL,
	[ActivityID] [int] NOT NULL,
	[HierarchyID] [int] NULL,
	[StartDate] [smalldatetime] NOT NULL,
	[EndDate] [smalldatetime] NOT NULL,
	[Anonymous] [smallint] NOT NULL,
	[Status] [smallint] NOT NULL,
	[ShowBack] [bit] NOT NULL,
	[LanguageID] [int] NOT NULL,
	[ButtonPlacement] [smallint] NOT NULL,
	[UsePageNo] [bit] NOT NULL,
	[LinkURL] [nvarchar](512) NOT NULL,
	[FinishURL] [nvarchar](512) NOT NULL,
	[CreateResult] [bit] NOT NULL,
	[ReportDB] [varchar](64) NOT NULL,
	[ReportServer] [varchar](64) NOT NULL,
	[StyleSheet] [varchar](128) NOT NULL,
	[Type] [smallint] NOT NULL,
	[Created] [smalldatetime] NOT NULL,
	[LastProcessed] [datetime] NOT NULL,
	[ReProcessOLAP] [smallint] NOT NULL,
	[No] [smallint] NOT NULL,
	[OLAPServer] [nvarchar](64) NOT NULL,
	[OLAPDB] [nvarchar](64) NOT NULL,
	[PeriodID] [int] NULL,
	[ProcessCategorys] [smallint] NOT NULL,
	[DeleteResultOnUserDelete] [bit] NOT NULL,
	[FromSurveyID] [int] NULL,
	[LastProcessedResultID] [bigint] NULL,
	[LastProcessedAnswerID] [bigint] NULL,
	[ExtId] [nvarchar](256) NOT NULL,
	[Tag] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_TMP_Survey] PRIMARY KEY CLUSTERED 
(
	[SurveyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [at].[TMP_XC_A]    Script Date: 12.04.2019 13.14.35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[TMP_XC_A](
	[XCID] [int] NOT NULL,
	[ActivityID] [int] NOT NULL,
	[No] [smallint] NOT NULL,
	[Listview] [smallint] NOT NULL,
	[Descview] [smallint] NOT NULL,
	[Chartview] [smallint] NOT NULL,
	[FillColor] [varchar](16) NOT NULL,
	[BorderColor] [varchar](16) NOT NULL,
 CONSTRAINT [TMP_PK_XC_A] PRIMARY KEY CLUSTERED 
(
	[XCID] ASC,
	[ActivityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [at].[TMP_XCategory]    Script Date: 12.04.2019 13.14.35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[TMP_XCategory](
	[XCID] [int] IDENTITY(1,1) NOT NULL,
	[ParentID] [int] NULL,
	[OwnerID] [int] NOT NULL,
	[Type] [smallint] NOT NULL,
	[Status] [smallint] NOT NULL,
	[No] [smallint] NOT NULL,
	[Created] [smalldatetime] NOT NULL,
	[ExtId] [nvarchar](256) NOT NULL,
	[MasterID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [TMP_PK_XCategory] PRIMARY KEY CLUSTERED 
(
	[XCID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [at].[XC_A]    Script Date: 12.04.2019 13.14.35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[XC_A](
	[XCID] [int] NOT NULL,
	[ActivityID] [int] NOT NULL,
	[No] [smallint] NOT NULL,
	[Listview] [smallint] NOT NULL,
	[Descview] [smallint] NOT NULL,
	[Chartview] [smallint] NOT NULL,
	[FillColor] [varchar](16) NOT NULL,
	[BorderColor] [varchar](16) NOT NULL,
 CONSTRAINT [PK_XC_A] PRIMARY KEY CLUSTERED 
(
	[XCID] ASC,
	[ActivityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [at].[XCategory]    Script Date: 12.04.2019 13.14.35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [at].[XCategory](
	[XCID] [int] IDENTITY(1,1) NOT NULL,
	[ParentID] [int] NULL,
	[OwnerID] [int] NOT NULL,
	[Type] [smallint] NOT NULL,
	[Status] [smallint] NOT NULL,
	[No] [smallint] NOT NULL,
	[Created] [smalldatetime] NOT NULL,
	[ExtId] [nvarchar](256) NOT NULL,
	[MasterID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_XCategory] PRIMARY KEY CLUSTERED 
(
	[XCID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Archetype]    Script Date: 12.04.2019 13.14.35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Archetype](
	[ArchetypeID] [int] NOT NULL,
	[CodeName] [nvarchar](128) NOT NULL,
	[TableTypeID] [smallint] NULL,
 CONSTRAINT [Archetype_PK] PRIMARY KEY CLUSTERED 
(
	[ArchetypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer_Activity]    Script Date: 12.04.2019 13.14.35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer_Activity](
	[CustomerID] [int] NOT NULL,
	[ActivityID] [int] NOT NULL,
 CONSTRAINT [PK_Customer_Activity] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC,
	[ActivityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer_Category]    Script Date: 12.04.2019 13.14.35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer_Category](
	[CustomerID] [int] NOT NULL,
	[CategoryID] [int] NOT NULL,
 CONSTRAINT [PK_Customer_Category] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC,
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer_Page]    Script Date: 12.04.2019 13.14.35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer_Page](
	[CustomerID] [int] NOT NULL,
	[PageID] [int] NOT NULL,
 CONSTRAINT [PK_Customer_Page] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC,
	[PageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer_Question]    Script Date: 12.04.2019 13.14.35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer_Question](
	[CustomerID] [int] NOT NULL,
	[QuestionID] [int] NOT NULL,
 CONSTRAINT [PK_Customer_Question] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC,
	[QuestionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EntityStatus]    Script Date: 12.04.2019 13.14.35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EntityStatus](
	[EntityStatusID] [int] NOT NULL,
	[CodeName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_EntityStatus] PRIMARY KEY CLUSTERED 
(
	[EntityStatusID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EntityStatusReason]    Script Date: 12.04.2019 13.14.35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EntityStatusReason](
	[EntityStatusReasonID] [int] NOT NULL,
	[EntityStatusID] [int] NOT NULL,
	[CodeName] [nvarchar](256) NOT NULL,
	[TableTypeID] [smallint] NOT NULL,
	[ArchetypeID] [int] NOT NULL,
 CONSTRAINT [PK_EntityStatusReason] PRIMARY KEY CLUSTERED 
(
	[EntityStatusReasonID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Language]    Script Date: 12.04.2019 13.14.35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Language](
	[LanguageID] [int] NOT NULL,
	[LanguageCode] [varchar](5) NOT NULL,
	[Dir] [varchar](3) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[NativeName] [nvarchar](64) NOT NULL,
	[Created] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_Language] PRIMARY KEY CLUSTERED 
(
	[LanguageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LT_EntityStatus]    Script Date: 12.04.2019 13.14.35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LT_EntityStatus](
	[LanguageID] [int] NOT NULL,
	[EntityStatusID] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_LT_EntityStatus] PRIMARY KEY CLUSTERED 
(
	[LanguageID] ASC,
	[EntityStatusID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LT_EntityStatusReason]    Script Date: 12.04.2019 13.14.35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LT_EntityStatusReason](
	[LanguageID] [int] NOT NULL,
	[EntityStatusReasonID] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_LT_EntityStatusReason] PRIMARY KEY CLUSTERED 
(
	[LanguageID] ASC,
	[EntityStatusReasonID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TMP_Answer]    Script Date: 12.04.2019 13.14.35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TMP_Answer](
	[AnswerID] [bigint] IDENTITY(1,1) NOT NULL,
	[ResultID] [bigint] NOT NULL,
	[QuestionID] [int] NOT NULL,
	[AlternativeID] [int] NOT NULL,
	[Value] [float] NOT NULL,
	[DateValue] [datetime] NULL,
	[No] [smallint] NOT NULL,
	[ItemID] [int] NULL,
	[Free] [nvarchar](max) NOT NULL,
	[CustomerID] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[LastUpdatedBy] [int] NULL,
	[Created] [datetime] NULL,
	[CreatedBy] [int] NULL,
 CONSTRAINT [PK_TMP_Answer] PRIMARY KEY CLUSTERED 
(
	[AnswerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TMP_Archetype]    Script Date: 12.04.2019 13.14.36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TMP_Archetype](
	[ArchetypeID] [int] NOT NULL,
	[CodeName] [nvarchar](128) NOT NULL,
	[TableTypeID] [smallint] NULL,
 CONSTRAINT [PK_TMP_Archetype] PRIMARY KEY CLUSTERED 
(
	[ArchetypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TMP_Customer_Activity]    Script Date: 12.04.2019 13.14.36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TMP_Customer_Activity](
	[CustomerID] [int] NOT NULL,
	[ActivityID] [int] NOT NULL,
 CONSTRAINT [PK_TMP_Customer_Activity] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC,
	[ActivityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TMP_Customer_Category]    Script Date: 12.04.2019 13.14.36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TMP_Customer_Category](
	[CustomerID] [int] NOT NULL,
	[CategoryID] [int] NOT NULL,
 CONSTRAINT [PK_TMP_Customer_Category] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC,
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TMP_Customer_Page]    Script Date: 12.04.2019 13.14.36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TMP_Customer_Page](
	[CustomerID] [int] NOT NULL,
	[PageID] [int] NOT NULL,
 CONSTRAINT [PK_TMP_Customer_Page] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC,
	[PageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TMP_Customer_Question]    Script Date: 12.04.2019 13.14.36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TMP_Customer_Question](
	[CustomerID] [int] NOT NULL,
	[QuestionID] [int] NOT NULL,
 CONSTRAINT [PK_TMP_Customer_Question] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC,
	[QuestionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TMP_EntityStatus]    Script Date: 12.04.2019 13.14.36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TMP_EntityStatus](
	[EntityStatusID] [int] NOT NULL,
	[CodeName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_TMP_EntityStatus] PRIMARY KEY CLUSTERED 
(
	[EntityStatusID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TMP_EntityStatusReason]    Script Date: 12.04.2019 13.14.36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TMP_EntityStatusReason](
	[EntityStatusReasonID] [int] NOT NULL,
	[EntityStatusID] [int] NOT NULL,
	[CodeName] [nvarchar](256) NOT NULL,
	[TableTypeID] [smallint] NOT NULL,
	[ArchetypeID] [int] NOT NULL,
 CONSTRAINT [PK_TMP_EntityStatusReason] PRIMARY KEY CLUSTERED 
(
	[EntityStatusReasonID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TMP_Language]    Script Date: 12.04.2019 13.14.36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TMP_Language](
	[LanguageID] [int] NOT NULL,
	[LanguageCode] [varchar](5) NOT NULL,
	[Dir] [varchar](3) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[NativeName] [nvarchar](64) NOT NULL,
	[Created] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_TMP_Language] PRIMARY KEY CLUSTERED 
(
	[LanguageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TMP_LT_EntityStatus]    Script Date: 12.04.2019 13.14.36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TMP_LT_EntityStatus](
	[LanguageID] [int] NOT NULL,
	[EntityStatusID] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_TMP_LT_EntityStatus] PRIMARY KEY CLUSTERED 
(
	[LanguageID] ASC,
	[EntityStatusID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TMP_LT_EntityStatusReason]    Script Date: 12.04.2019 13.14.36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TMP_LT_EntityStatusReason](
	[LanguageID] [int] NOT NULL,
	[EntityStatusReasonID] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_TMP_LT_EntityStatusReason] PRIMARY KEY CLUSTERED 
(
	[LanguageID] ASC,
	[EntityStatusReasonID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TMP_ProveResultater]    Script Date: 12.04.2019 13.14.36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TMP_ProveResultater](
	[Periode] [nvarchar](256) NOT NULL,
	[Customerid] [int] NOT NULL,
	[Departmentid] [int] NULL,
	[Kunde] [nvarchar](4000) NULL,
	[Skole] [nvarchar](4000) NULL,
	[Forlag] [nvarchar](512) NOT NULL,
	[Hovedområde] [nvarchar](512) NOT NULL,
	[Underområde] [nvarchar](512) NOT NULL,
	[Prøve/Verktøy] [nvarchar](4000) NULL,
	[Trinn] [nvarchar](256) NOT NULL,
	[Måned] [int] NULL,
	[År] [int] NULL,
	[Resultater] [int] NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TMP_Result]    Script Date: 12.04.2019 13.14.36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TMP_Result](
	[ResultID] [bigint] IDENTITY(1,1) NOT NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[RoleID] [int] NOT NULL,
	[UserID] [int] NULL,
	[UserGroupID] [int] NULL,
	[SurveyID] [int] NOT NULL,
	[BatchID] [int] NOT NULL,
	[LanguageID] [int] NOT NULL,
	[PageNo] [smallint] NOT NULL,
	[DepartmentID] [int] NOT NULL,
	[Anonymous] [smallint] NOT NULL,
	[ShowBack] [bit] NOT NULL,
	[ResultKey] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NOT NULL,
	[chk] [timestamp] NOT NULL,
	[Created] [datetime] NOT NULL,
	[LastUpdated] [datetime] NOT NULL,
	[LastUpdatedBy] [int] NOT NULL,
	[StatusTypeID] [int] NULL,
	[ValidFrom] [datetime2](7) NULL,
	[ValidTo] [datetime2](7) NULL,
	[DueDate] [datetime2](7) NULL,
	[ParentResultID] [bigint] NULL,
	[ParentResultSurveyID] [int] NULL,
	[CustomerID] [int] NULL,
	[CreatedBy] [int] NULL,
	[Deleted] [datetime2](7) NULL,
	[EntityStatusID] [int] NULL,
	[EntityStatusReasonID] [int] NULL,
 CONSTRAINT [PK_TMP_Result] PRIMARY KEY CLUSTERED 
(
	[ResultID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [org].[DepartmentType]    Script Date: 12.04.2019 13.14.36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [org].[DepartmentType](
	[DepartmentTypeID] [int] IDENTITY(1,1) NOT NULL,
	[OwnerID] [int] NOT NULL,
	[ExtID] [nvarchar](256) NOT NULL,
	[No] [smallint] NOT NULL,
	[Created] [smalldatetime] NOT NULL,
	[ArchetypeID] [int] NULL,
	[ParentID] [int] NULL,
 CONSTRAINT [PK_DepartmentType] PRIMARY KEY CLUSTERED 
(
	[DepartmentTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [org].[H_D]    Script Date: 12.04.2019 13.14.36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [org].[H_D](
	[HDID] [int] IDENTITY(1,1) NOT NULL,
	[HierarchyID] [int] NOT NULL,
	[DepartmentID] [int] NOT NULL,
	[ParentID] [int] NULL,
	[Path] [nvarchar](900) NOT NULL,
	[PathName] [nvarchar](4000) NOT NULL,
	[Created] [smalldatetime] NOT NULL,
	[Deleted] [smallint] NOT NULL,
 CONSTRAINT [PK_H_D] PRIMARY KEY CLUSTERED 
(
	[HDID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_H_D] UNIQUE NONCLUSTERED 
(
	[HierarchyID] ASC,
	[DepartmentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [org].[Hierarchy]    Script Date: 12.04.2019 13.14.36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [org].[Hierarchy](
	[HierarchyID] [int] IDENTITY(1,1) NOT NULL,
	[OwnerID] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[Type] [smallint] NOT NULL,
	[Deleted] [bit] NOT NULL,
	[Created] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_Hierarchy] PRIMARY KEY CLUSTERED 
(
	[HierarchyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [org].[LT_DepartmentType]    Script Date: 12.04.2019 13.14.36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [org].[LT_DepartmentType](
	[LanguageID] [int] NOT NULL,
	[DepartmentTypeID] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_LT_DepartmentType] PRIMARY KEY CLUSTERED 
(
	[LanguageID] ASC,
	[DepartmentTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [org].[LT_UserGroupType]    Script Date: 12.04.2019 13.14.36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [org].[LT_UserGroupType](
	[LanguageID] [int] NOT NULL,
	[UserGroupTypeID] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_LT_UserGroupType] PRIMARY KEY CLUSTERED 
(
	[LanguageID] ASC,
	[UserGroupTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [org].[LT_UserType]    Script Date: 12.04.2019 13.14.36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [org].[LT_UserType](
	[LanguageID] [int] NOT NULL,
	[UserTypeID] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_LT_UserType] PRIMARY KEY CLUSTERED 
(
	[LanguageID] ASC,
	[UserTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [org].[Owner]    Script Date: 12.04.2019 13.14.36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [org].[Owner](
	[OwnerID] [int] IDENTITY(1,1) NOT NULL,
	[LanguageID] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[ReportServer] [varchar](64) NOT NULL,
	[ReportDB] [varchar](64) NOT NULL,
	[MainHierarchyID] [int] NULL,
	[OLAPServer] [varchar](64) NOT NULL,
	[OLAPDB] [varchar](64) NOT NULL,
	[Css] [nvarchar](128) NOT NULL,
	[Url] [nvarchar](128) NOT NULL,
	[LoginType] [int] NOT NULL,
	[Prefix] [nvarchar](8) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[Logging] [smallint] NOT NULL,
	[Created] [smalldatetime] NOT NULL,
	[OTPLength] [int] NOT NULL,
	[OTPCharacters] [nvarchar](512) NOT NULL,
	[OTPAllowLowercase] [bit] NOT NULL,
	[OTPAllowUppercase] [bit] NOT NULL,
	[UseOTPCaseSensitive] [bit] NOT NULL,
	[UseHashPassword] [smallint] NOT NULL,
	[UseOTP] [bit] NOT NULL,
	[DefaultHashMethod] [int] NOT NULL,
	[OTPDuration] [int] NOT NULL,
 CONSTRAINT [PK_Owner] PRIMARY KEY CLUSTERED 
(
	[OwnerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [org].[TMP_Customer]    Script Date: 12.04.2019 13.14.36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [org].[TMP_Customer](
	[CustomerID] [int] IDENTITY(1,1) NOT NULL,
	[OwnerID] [int] NOT NULL,
	[LanguageID] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Status] [smallint] NOT NULL,
	[ExtID] [nvarchar](256) NOT NULL,
	[Created] [smalldatetime] NOT NULL,
	[HasUserIntegration] [bit] NOT NULL,
	[RootMenuID] [int] NULL,
	[CodeName] [nvarchar](256) NULL,
	[Logo] [nvarchar](max) NULL,
	[CssVariables] [nvarchar](max) NULL,
 CONSTRAINT [PK_TMP_Customer] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [org].[TMP_Department]    Script Date: 12.04.2019 13.14.36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [org].[TMP_Department](
	[DepartmentID] [int] IDENTITY(1,1) NOT NULL,
	[LanguageID] [int] NOT NULL,
	[OwnerID] [int] NOT NULL,
	[CustomerID] [int] NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[Adress] [nvarchar](512) NOT NULL,
	[PostalCode] [nvarchar](32) NOT NULL,
	[City] [nvarchar](64) NOT NULL,
	[OrgNo] [varchar](16) NOT NULL,
	[Created] [smalldatetime] NOT NULL,
	[ExtID] [nvarchar](256) NOT NULL,
	[Tag] [nvarchar](max) NOT NULL,
	[Locked] [smallint] NOT NULL,
	[CountryCode] [int] NOT NULL,
	[EntityVersion] [timestamp] NOT NULL,
	[LastUpdated] [datetime2](7) NOT NULL,
	[LastUpdatedBy] [int] NULL,
	[LastSynchronized] [datetime2](7) NOT NULL,
	[ArchetypeID] [int] NULL,
	[Deleted] [datetime2](7) NULL,
	[EntityStatusID] [int] NULL,
	[EntityStatusReasonID] [int] NULL,
 CONSTRAINT [PK_TMP_Department] PRIMARY KEY CLUSTERED 
(
	[DepartmentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [org].[TMP_DepartmentType]    Script Date: 12.04.2019 13.14.36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [org].[TMP_DepartmentType](
	[DepartmentTypeID] [int] IDENTITY(1,1) NOT NULL,
	[OwnerID] [int] NOT NULL,
	[ExtID] [nvarchar](256) NOT NULL,
	[No] [smallint] NOT NULL,
	[Created] [smalldatetime] NOT NULL,
	[ArchetypeID] [int] NULL,
	[ParentID] [int] NULL,
 CONSTRAINT [PK_TMP_DepartmentType] PRIMARY KEY CLUSTERED 
(
	[DepartmentTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [org].[TMP_DT_D]    Script Date: 12.04.2019 13.14.36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [org].[TMP_DT_D](
	[DepartmentTypeID] [int] NOT NULL,
	[DepartmentID] [int] NOT NULL,
 CONSTRAINT [PK_TMP_DT_D] PRIMARY KEY CLUSTERED 
(
	[DepartmentTypeID] ASC,
	[DepartmentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [org].[TMP_H_D]    Script Date: 12.04.2019 13.14.36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [org].[TMP_H_D](
	[HDID] [int] IDENTITY(1,1) NOT NULL,
	[HierarchyID] [int] NOT NULL,
	[DepartmentID] [int] NOT NULL,
	[ParentID] [int] NULL,
	[Path] [nvarchar](900) NOT NULL,
	[PathName] [nvarchar](4000) NOT NULL,
	[Created] [smalldatetime] NOT NULL,
	[Deleted] [smallint] NOT NULL,
 CONSTRAINT [PK_TMP_H_D] PRIMARY KEY CLUSTERED 
(
	[HDID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [TMP_IX_H_D] UNIQUE NONCLUSTERED 
(
	[HierarchyID] ASC,
	[DepartmentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [org].[TMP_Hierarchy]    Script Date: 12.04.2019 13.14.36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [org].[TMP_Hierarchy](
	[HierarchyID] [int] IDENTITY(1,1) NOT NULL,
	[OwnerID] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[Type] [smallint] NOT NULL,
	[Deleted] [bit] NOT NULL,
	[Created] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_TMP_Hierarchy] PRIMARY KEY CLUSTERED 
(
	[HierarchyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [org].[TMP_LT_DepartmentType]    Script Date: 12.04.2019 13.14.36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [org].[TMP_LT_DepartmentType](
	[LanguageID] [int] NOT NULL,
	[DepartmentTypeID] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_TMP_LT_DepartmentType] PRIMARY KEY CLUSTERED 
(
	[LanguageID] ASC,
	[DepartmentTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [org].[TMP_LT_UserGroupType]    Script Date: 12.04.2019 13.14.37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [org].[TMP_LT_UserGroupType](
	[LanguageID] [int] NOT NULL,
	[UserGroupTypeID] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
 CONSTRAINT [TMP_PK_LT_UserGroupType] PRIMARY KEY CLUSTERED 
(
	[LanguageID] ASC,
	[UserGroupTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [org].[TMP_LT_UserType]    Script Date: 12.04.2019 13.14.37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [org].[TMP_LT_UserType](
	[LanguageID] [int] NOT NULL,
	[UserTypeID] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
 CONSTRAINT [TMP_PK_LT_UserType] PRIMARY KEY CLUSTERED 
(
	[LanguageID] ASC,
	[UserTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [org].[TMP_Owner]    Script Date: 12.04.2019 13.14.37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [org].[TMP_Owner](
	[OwnerID] [int] IDENTITY(1,1) NOT NULL,
	[LanguageID] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[ReportServer] [varchar](64) NOT NULL,
	[ReportDB] [varchar](64) NOT NULL,
	[MainHierarchyID] [int] NULL,
	[OLAPServer] [varchar](64) NOT NULL,
	[OLAPDB] [varchar](64) NOT NULL,
	[Css] [nvarchar](128) NOT NULL,
	[Url] [nvarchar](128) NOT NULL,
	[LoginType] [int] NOT NULL,
	[Prefix] [nvarchar](8) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[Logging] [smallint] NOT NULL,
	[Created] [smalldatetime] NOT NULL,
	[OTPLength] [int] NOT NULL,
	[OTPCharacters] [nvarchar](512) NOT NULL,
	[OTPAllowLowercase] [bit] NOT NULL,
	[OTPAllowUppercase] [bit] NOT NULL,
	[UseOTPCaseSensitive] [bit] NOT NULL,
	[UseHashPassword] [smallint] NOT NULL,
	[UseOTP] [bit] NOT NULL,
	[DefaultHashMethod] [int] NOT NULL,
	[OTPDuration] [int] NOT NULL,
 CONSTRAINT [PK_TMP_Owner] PRIMARY KEY CLUSTERED 
(
	[OwnerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [org].[TMP_UGMember]    Script Date: 12.04.2019 13.14.37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [org].[TMP_UGMember](
	[UserGroupID] [int] NOT NULL,
	[UserID] [int] NULL,
	[UGMemberID] [bigint] IDENTITY(1,1) NOT NULL,
	[Created] [smalldatetime] NULL,
	[CreatedBy] [int] NULL,
	[MemberRoleID] [int] NULL,
	[ValidFrom] [datetime2](7) NULL,
	[ValidTo] [datetime2](7) NULL,
	[EntityVersion] [timestamp] NULL,
	[LastUpdated] [datetime2](7) NULL,
	[LastUpdatedBy] [int] NULL,
	[LastSynchronized] [datetime2](7) NULL,
	[EntityStatusID] [int] NULL,
	[EntityStatusReasonID] [int] NULL,
	[CustomerID] [int] NULL,
	[PeriodID] [int] NULL,
	[ExtID] [nvarchar](256) NOT NULL,
	[Deleted] [datetime2](7) NULL,
	[DisplayName] [nvarchar](max) NULL,
	[ReferrerResource] [nvarchar](512) NOT NULL,
	[ReferrerToken] [nvarchar](512) NOT NULL,
	[ReferrerArchetypeID] [int] NULL,
 CONSTRAINT [TMP_UPK_UGMemberID] PRIMARY KEY CLUSTERED 
(
	[UGMemberID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [org].[TMP_User]    Script Date: 12.04.2019 13.14.37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [org].[TMP_User](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[Ownerid] [int] NOT NULL,
	[DepartmentID] [int] NOT NULL,
	[LanguageID] [int] NOT NULL,
	[RoleID] [int] NULL,
	[UserName] [nvarchar](128) NOT NULL,
	[Password] [nvarchar](64) NOT NULL,
	[LastName] [nvarchar](64) NOT NULL,
	[FirstName] [nvarchar](64) NOT NULL,
	[Email] [nvarchar](256) NOT NULL,
	[Mobile] [nvarchar](16) NOT NULL,
	[ExtID] [nvarchar](256) NOT NULL,
	[Tag] [nvarchar](128) NOT NULL,
	[Locked] [smallint] NOT NULL,
	[Created] [smalldatetime] NOT NULL,
	[SSN] [nvarchar](64) NOT NULL,
	[ChangePassword] [bit] NOT NULL,
	[Gender] [smallint] NULL,
	[DateOfBirth] [date] NULL,
	[HashPassword] [nvarchar](128) NOT NULL,
	[SaltPassword] [nvarchar](64) NOT NULL,
	[OneTimePassword] [nvarchar](64) NOT NULL,
	[OTPExpireTime] [smalldatetime] NULL,
	[CountryCode] [int] NULL,
	[ForceUserLoginAgain] [bit] NOT NULL,
	[EntityVersion] [timestamp] NOT NULL,
	[LastUpdated] [datetime2](7) NOT NULL,
	[LastUpdatedBy] [int] NULL,
	[LastSynchronized] [datetime2](7) NOT NULL,
	[ArchetypeID] [int] NULL,
	[Deleted] [datetime2](7) NULL,
	[EntityStatusID] [int] NULL,
	[EntityStatusReasonID] [int] NULL,
	[CustomerID] [int] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_User] UNIQUE NONCLUSTERED 
(
	[UserName] ASC,
	[Ownerid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [org].[TMP_user_referrerToken]    Script Date: 12.04.2019 13.14.37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [org].[TMP_user_referrerToken](
	[userid] [int] NOT NULL,
	[refererToken] [nvarchar](256) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [org].[TMP_UserGroupType]    Script Date: 12.04.2019 13.14.37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [org].[TMP_UserGroupType](
	[UserGroupTypeID] [int] NOT NULL,
	[OwnerID] [int] NOT NULL,
	[ExtID] [nvarchar](256) NOT NULL,
	[No] [smallint] NOT NULL,
	[Created] [smalldatetime] NOT NULL,
	[MasterID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [TMP_PK_UsergroupType] PRIMARY KEY CLUSTERED 
(
	[UserGroupTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [org].[TMP_UserType]    Script Date: 12.04.2019 13.14.37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [org].[TMP_UserType](
	[UserTypeID] [int] IDENTITY(1,1) NOT NULL,
	[OwnerID] [int] NOT NULL,
	[ExtID] [nvarchar](256) NULL,
	[No] [smallint] NOT NULL,
	[Created] [smalldatetime] NOT NULL,
	[ArchetypeID] [int] NULL,
	[ParentID] [int] NULL,
 CONSTRAINT [TMP_PK_UserType] PRIMARY KEY CLUSTERED 
(
	[UserTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [org].[TMP_UT_U]    Script Date: 12.04.2019 13.14.37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [org].[TMP_UT_U](
	[UserTypeID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
 CONSTRAINT [TMP_PK_UT_U] PRIMARY KEY CLUSTERED 
(
	[UserTypeID] ASC,
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [org].[UGMember]    Script Date: 12.04.2019 13.14.37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [org].[UGMember](
	[UserGroupID] [int] NOT NULL,
	[UserID] [int] NULL,
	[UGMemberID] [bigint] IDENTITY(1,1) NOT NULL,
	[Created] [smalldatetime] NULL,
	[CreatedBy] [int] NULL,
	[MemberRoleID] [int] NULL,
	[ValidFrom] [datetime2](7) NULL,
	[ValidTo] [datetime2](7) NULL,
	[EntityVersion] [timestamp] NULL,
	[LastUpdated] [datetime2](7) NULL,
	[LastUpdatedBy] [int] NULL,
	[LastSynchronized] [datetime2](7) NULL,
	[EntityStatusID] [int] NULL,
	[EntityStatusReasonID] [int] NULL,
	[CustomerID] [int] NULL,
	[PeriodID] [int] NULL,
	[ExtID] [nvarchar](256) NOT NULL,
	[Deleted] [datetime2](7) NULL,
	[DisplayName] [nvarchar](max) NULL,
	[ReferrerResource] [nvarchar](512) NOT NULL,
	[ReferrerToken] [nvarchar](512) NOT NULL,
	[ReferrerArchetypeID] [int] NULL,
 CONSTRAINT [PK_UGMemberID] PRIMARY KEY CLUSTERED 
(
	[UGMemberID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [org].[user_referrerToken]    Script Date: 12.04.2019 13.14.37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [org].[user_referrerToken](
	[userid] [int] NOT NULL,
	[refererToken] [nvarchar](256) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [org].[UserGroupType]    Script Date: 12.04.2019 13.14.37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [org].[UserGroupType](
	[UserGroupTypeID] [int] NOT NULL,
	[OwnerID] [int] NOT NULL,
	[ExtID] [nvarchar](256) NOT NULL,
	[No] [smallint] NOT NULL,
	[Created] [smalldatetime] NOT NULL,
	[MasterID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_UsergroupType] PRIMARY KEY CLUSTERED 
(
	[UserGroupTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [org].[UserType]    Script Date: 12.04.2019 13.14.37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [org].[UserType](
	[UserTypeID] [int] IDENTITY(1,1) NOT NULL,
	[OwnerID] [int] NOT NULL,
	[ExtID] [nvarchar](256) NULL,
	[No] [smallint] NOT NULL,
	[Created] [smalldatetime] NOT NULL,
	[ArchetypeID] [int] NULL,
	[ParentID] [int] NULL,
 CONSTRAINT [PK_UserType] PRIMARY KEY CLUSTERED 
(
	[UserTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [org].[UT_U]    Script Date: 12.04.2019 13.14.37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [org].[UT_U](
	[UserTypeID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
 CONSTRAINT [PK_UT_U] PRIMARY KEY CLUSTERED 
(
	[UserTypeID] ASC,
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [at].[Activity] ADD  CONSTRAINT [TMP_DF_Activity_LanguageID]  DEFAULT ((1)) FOR [LanguageID]
GO
ALTER TABLE [at].[Activity] ADD  CONSTRAINT [TMP_DF_Activity_StyleSheet]  DEFAULT ('') FOR [StyleSheet]
GO
ALTER TABLE [at].[Activity] ADD  CONSTRAINT [TMP_DF_Activity_Type]  DEFAULT ((0)) FOR [Type]
GO
ALTER TABLE [at].[Activity] ADD  CONSTRAINT [TMP_DF_Activity_No]  DEFAULT ((0)) FOR [No]
GO
ALTER TABLE [at].[Activity] ADD  CONSTRAINT [TMP_DF_Activity_TooltipType]  DEFAULT ((0)) FOR [TooltipType]
GO
ALTER TABLE [at].[Activity] ADD  CONSTRAINT [TMP_DF_Activity_Listview]  DEFAULT ((0)) FOR [Listview]
GO
ALTER TABLE [at].[Activity] ADD  CONSTRAINT [TMP_DF_Activity_Descview]  DEFAULT ((0)) FOR [Descview]
GO
ALTER TABLE [at].[Activity] ADD  CONSTRAINT [TMP_DF_Activity_Chartview]  DEFAULT ((0)) FOR [Chartview]
GO
ALTER TABLE [at].[Activity] ADD  CONSTRAINT [TMP_DF_Activity_OLAPServer]  DEFAULT ('') FOR [OLAPServer]
GO
ALTER TABLE [at].[Activity] ADD  CONSTRAINT [TMP_DF_Activity_OLAPDB]  DEFAULT ('') FOR [OLAPDB]
GO
ALTER TABLE [at].[Activity] ADD  CONSTRAINT [TMP_DF_Activity_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [at].[Activity] ADD  CONSTRAINT [TMP_DF_SelectionHeaderType]  DEFAULT ((0)) FOR [SelectionHeaderType]
GO
ALTER TABLE [at].[Activity] ADD  CONSTRAINT [TMP_DF_Activity_ReportServer]  DEFAULT ('') FOR [ReportServer]
GO
ALTER TABLE [at].[Activity] ADD  CONSTRAINT [TMP_DF_Activity_ReportDB]  DEFAULT ('') FOR [ReportDB]
GO
ALTER TABLE [at].[Activity] ADD  CONSTRAINT [TMP_DF_Activity_ExtID]  DEFAULT ('') FOR [ExtID]
GO
ALTER TABLE [at].[Activity] ADD  CONSTRAINT [TMP_DF_Activity_UseOLAP]  DEFAULT ((1)) FOR [UseOLAP]
GO
ALTER TABLE [at].[Activity] ADD  CONSTRAINT [TMP_DF_Activity_ArchetypeID]  DEFAULT (NULL) FOR [ArchetypeID]
GO
ALTER TABLE [at].[Alternative] ADD  CONSTRAINT [TMP_DF_Alternative_Type]  DEFAULT ((0)) FOR [Type]
GO
ALTER TABLE [at].[Alternative] ADD  CONSTRAINT [TMP_DF_Alternative_AltNo]  DEFAULT ((0)) FOR [No]
GO
ALTER TABLE [at].[Alternative] ADD  CONSTRAINT [TMP_DF_Alternative_Weight]  DEFAULT ((0)) FOR [Value]
GO
ALTER TABLE [at].[Alternative] ADD  CONSTRAINT [TMP_DF_Alternative_InvertedValue]  DEFAULT ((0)) FOR [InvertedValue]
GO
ALTER TABLE [at].[Alternative] ADD  CONSTRAINT [TMP_DF_Alternative_Calc]  DEFAULT ((1)) FOR [Calc]
GO
ALTER TABLE [at].[Alternative] ADD  CONSTRAINT [TMP_DF_Alternative_SC]  DEFAULT ((0)) FOR [SC]
GO
ALTER TABLE [at].[Alternative] ADD  CONSTRAINT [TMP_DF_Alternative_MinValue]  DEFAULT ((0)) FOR [MinValue]
GO
ALTER TABLE [at].[Alternative] ADD  CONSTRAINT [TMP_DF_Alternative_MaxValue]  DEFAULT ((0)) FOR [MaxValue]
GO
ALTER TABLE [at].[Alternative] ADD  CONSTRAINT [TMP_DF_Alternative_Format]  DEFAULT ('') FOR [Format]
GO
ALTER TABLE [at].[Alternative] ADD  CONSTRAINT [TMP_DF_Alternative_Size]  DEFAULT ((0)) FOR [Size]
GO
ALTER TABLE [at].[Alternative] ADD  CONSTRAINT [TMP_DF_Alternative_CssClass]  DEFAULT ('') FOR [CssClass]
GO
ALTER TABLE [at].[Alternative] ADD  CONSTRAINT [TMP_DF_Alternative_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [at].[Alternative] ADD  CONSTRAINT [TMP_DF_Alternative_DefaultValue]  DEFAULT ('') FOR [DefaultValue]
GO
ALTER TABLE [at].[Alternative] ADD  CONSTRAINT [TMP_DF_Alternative_Tag]  DEFAULT ('') FOR [Tag]
GO
ALTER TABLE [at].[Alternative] ADD  CONSTRAINT [TMP_DF_Alternative_ExtID]  DEFAULT ('') FOR [ExtID]
GO
ALTER TABLE [at].[Alternative] ADD  CONSTRAINT [TMP_DF_Alternative_Width]  DEFAULT ('') FOR [Width]
GO
ALTER TABLE [at].[Alternative] ADD  CONSTRAINT [TMP_DF_Alternative_DefaultCalcType]  DEFAULT ((-1)) FOR [DefaultCalcType]
GO
ALTER TABLE [at].[Alternative] ADD  CONSTRAINT [TMP_DF_Alternative_UseEncryption]  DEFAULT ((0)) FOR [UseEncryption]
GO
ALTER TABLE [at].[Batch] ADD  CONSTRAINT [TMP_DF_Batch_No]  DEFAULT ((0)) FOR [No]
GO
ALTER TABLE [at].[Batch] ADD  CONSTRAINT [TMP_DF_Batch_Status]  DEFAULT ((0)) FOR [Status]
GO
ALTER TABLE [at].[Batch] ADD  CONSTRAINT [TMP_DF_Batch_MailStatus]  DEFAULT ((0)) FOR [MailStatus]
GO
ALTER TABLE [at].[Batch] ADD  CONSTRAINT [TMP_DF_Batch_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [at].[Batch] ADD  CONSTRAINT [TMP_DF_Batch_ExtID]  DEFAULT ('') FOR [ExtID]
GO
ALTER TABLE [at].[Category] ADD  CONSTRAINT [TMP_DF_Category_Tag]  DEFAULT ('') FOR [Tag]
GO
ALTER TABLE [at].[Category] ADD  CONSTRAINT [TMP_DF_Category_Type]  DEFAULT ((0)) FOR [Type]
GO
ALTER TABLE [at].[Category] ADD  CONSTRAINT [TMP_DF_Category_No]  DEFAULT ((0)) FOR [No]
GO
ALTER TABLE [at].[Category] ADD  CONSTRAINT [TMP_DF_Category_Fillcolor]  DEFAULT ('') FOR [Fillcolor]
GO
ALTER TABLE [at].[Category] ADD  CONSTRAINT [TMP_DF_Category_BorderColor]  DEFAULT ('') FOR [BorderColor]
GO
ALTER TABLE [at].[Category] ADD  CONSTRAINT [TMP_DF_Category_Weight]  DEFAULT ((1.0)) FOR [Weight]
GO
ALTER TABLE [at].[Category] ADD  CONSTRAINT [TMP_DF_Category_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [at].[Category] ADD  CONSTRAINT [TMP_DF_Category_NType]  DEFAULT ((0)) FOR [NType]
GO
ALTER TABLE [at].[Category] ADD  CONSTRAINT [TMP_DF_Category_ProcessCategory]  DEFAULT ((0)) FOR [ProcessCategory]
GO
ALTER TABLE [at].[Category] ADD  CONSTRAINT [TMP_DF_Category_ExtID]  DEFAULT ('') FOR [ExtID]
GO
ALTER TABLE [at].[Category] ADD  CONSTRAINT [TMP_DF_Category_RenderAsDefault]  DEFAULT ((1)) FOR [RenderAsDefault]
GO
ALTER TABLE [at].[Category] ADD  CONSTRAINT [TMP_DF_category_AvgType]  DEFAULT ((0)) FOR [AvgType]
GO
ALTER TABLE [at].[Category] ADD  CONSTRAINT [TMP_DF_Category_DefaultState]  DEFAULT ((0)) FOR [DefaultState]
GO
ALTER TABLE [at].[LevelGroup] ADD  CONSTRAINT [DF_LevelGroup_Tag]  DEFAULT ('') FOR [Tag]
GO
ALTER TABLE [at].[LevelGroup] ADD  CONSTRAINT [DF_LevelGroup_No]  DEFAULT ((0)) FOR [No]
GO
ALTER TABLE [at].[LevelGroup] ADD  CONSTRAINT [DF_LevelGroup_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [at].[LevelLimit] ADD  CONSTRAINT [DF_LevelLimit_Sigchange]  DEFAULT ((0)) FOR [Sigchange]
GO
ALTER TABLE [at].[LevelLimit] ADD  CONSTRAINT [DF_LevelLimit_Fillcolor]  DEFAULT ('') FOR [OwnerColorID]
GO
ALTER TABLE [at].[LevelLimit] ADD  CONSTRAINT [DF_LevelLimit_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [at].[LevelLimit] ADD  CONSTRAINT [DF__LevelLimi__Negat__5AF01967]  DEFAULT ((0)) FOR [NegativeTrend]
GO
ALTER TABLE [at].[LevelLimit] ADD  CONSTRAINT [DF_LevelLimit_ExtID]  DEFAULT ('') FOR [ExtID]
GO
ALTER TABLE [at].[LevelLimit] ADD  CONSTRAINT [DF__TMP_Level__Match__5A4F643B]  DEFAULT ((0)) FOR [MatchingType]
GO
ALTER TABLE [at].[LT_Activity] ADD  CONSTRAINT [TMP_DF_LT_Activity_Info]  DEFAULT ('') FOR [Info]
GO
ALTER TABLE [at].[LT_Activity] ADD  CONSTRAINT [TMP_DF_LT_Activity_StartText]  DEFAULT ('') FOR [StartText]
GO
ALTER TABLE [at].[LT_Activity] ADD  CONSTRAINT [TMP_DF_LT_Activity_Description]  DEFAULT ('') FOR [Description]
GO
ALTER TABLE [at].[LT_Activity] ADD  CONSTRAINT [TMP_DF_LT_Activity_RoleName]  DEFAULT ('') FOR [RoleName]
GO
ALTER TABLE [at].[LT_Activity] ADD  CONSTRAINT [TMP_DF_LT_Activity_SurveyName]  DEFAULT ('') FOR [SurveyName]
GO
ALTER TABLE [at].[LT_Activity] ADD  CONSTRAINT [TMP_DF_LT_Activity_BatchName]  DEFAULT ('') FOR [BatchName]
GO
ALTER TABLE [at].[LT_Activity] ADD  CONSTRAINT [TMP_DF_LT_Activity_DisplayName]  DEFAULT ('') FOR [DisplayName]
GO
ALTER TABLE [at].[LT_Activity] ADD  CONSTRAINT [TMP_DF_LT_Activity_ShortName]  DEFAULT ('') FOR [ShortName]
GO
ALTER TABLE [at].[LT_Alternative] ADD  CONSTRAINT [DF_LT_Alternative_Description]  DEFAULT ('') FOR [Description]
GO
ALTER TABLE [at].[LT_Category] ADD  CONSTRAINT [TMP_DF_LT_Category_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [at].[LT_Category] ADD  CONSTRAINT [TMP_DF__LT_Catego__Descr__489AC854]  DEFAULT ('') FOR [Description]
GO
ALTER TABLE [at].[LT_Category] ADD  CONSTRAINT [TMP_DF_LT_Category_DescriptionExtended]  DEFAULT ('') FOR [DescriptionExtended]
GO
ALTER TABLE [at].[LT_LevelGroup] ADD  CONSTRAINT [DF_LT_LevelGroup_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [at].[LT_LevelGroup] ADD  CONSTRAINT [DF_LT_LevelGroup_Description]  DEFAULT ('') FOR [Description]
GO
ALTER TABLE [at].[LT_LevelLimit] ADD  CONSTRAINT [DF_LT_LevelLimit_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [at].[LT_LevelLimit] ADD  CONSTRAINT [DF_LT_LevelLimit_Description]  DEFAULT ('') FOR [Description]
GO
ALTER TABLE [at].[LT_Page] ADD  CONSTRAINT [DF_LT_Page_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [at].[LT_Page] ADD  CONSTRAINT [DF_LT_Page_Info]  DEFAULT ('') FOR [Info]
GO
ALTER TABLE [at].[LT_Page] ADD  CONSTRAINT [DF_LT_Page_ToolTip]  DEFAULT ('') FOR [Description]
GO
ALTER TABLE [at].[LT_Question] ADD  CONSTRAINT [DF_LT_Question_Text]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [at].[LT_Question] ADD  CONSTRAINT [DF_LT_Question_HeadLine]  DEFAULT ('') FOR [Title]
GO
ALTER TABLE [at].[LT_Question] ADD  CONSTRAINT [DF_LT_Question_ReportText]  DEFAULT ('') FOR [ReportText]
GO
ALTER TABLE [at].[LT_Question] ADD  CONSTRAINT [DF_LT_Question_ToolTip]  DEFAULT ('') FOR [Description]
GO
ALTER TABLE [at].[LT_Question] ADD  CONSTRAINT [DF__LT_Questi__Short__30F9DF9B]  DEFAULT ('') FOR [ShortName]
GO
ALTER TABLE [at].[LT_Role] ADD  CONSTRAINT [DF_LT_Role_Description]  DEFAULT ('') FOR [Description]
GO
ALTER TABLE [at].[LT_Scale] ADD  CONSTRAINT [DF_LT_AlternativeGroup_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [at].[LT_Scale] ADD  CONSTRAINT [DF_LT_AlternativeGroup_Title]  DEFAULT ('') FOR [Title]
GO
ALTER TABLE [at].[LT_Scale] ADD  CONSTRAINT [DF_LT_Scale_Description]  DEFAULT ('') FOR [Description]
GO
ALTER TABLE [at].[LT_Survey] ADD  CONSTRAINT [DF_LT_Execution_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [at].[LT_Survey] ADD  CONSTRAINT [DF_LT_Execution_Description]  DEFAULT ('') FOR [Description]
GO
ALTER TABLE [at].[LT_Survey] ADD  CONSTRAINT [DF_LT_Execution_Info]  DEFAULT ('') FOR [Info]
GO
ALTER TABLE [at].[LT_Survey] ADD  CONSTRAINT [DF_LT_Execution_FinishText]  DEFAULT ('') FOR [FinishText]
GO
ALTER TABLE [at].[LT_Survey] ADD  CONSTRAINT [DF__LT_Survey__Displ__4E00329D]  DEFAULT ('') FOR [DisplayName]
GO
ALTER TABLE [at].[LT_XCategory] ADD  CONSTRAINT [DF_LT_XCategory_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [at].[LT_XCategory] ADD  CONSTRAINT [DF_LT_XCategory_Description]  DEFAULT ('') FOR [Description]
GO
ALTER TABLE [at].[Page] ADD  CONSTRAINT [DF_Page_No]  DEFAULT ((1)) FOR [No]
GO
ALTER TABLE [at].[Page] ADD  CONSTRAINT [DF_Page_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [at].[Page] ADD  CONSTRAINT [DF_Page_Type]  DEFAULT ((0)) FOR [Type]
GO
ALTER TABLE [at].[Page] ADD  CONSTRAINT [DF__Page__CssClass__501D8539]  DEFAULT ('') FOR [CssClass]
GO
ALTER TABLE [at].[Page] ADD  CONSTRAINT [DF_Page_ExtID]  DEFAULT ('') FOR [ExtID]
GO
ALTER TABLE [at].[Page] ADD  CONSTRAINT [DF_Page_Tag]  DEFAULT ('') FOR [Tag]
GO
ALTER TABLE [at].[Period] ADD  CONSTRAINT [DF_Period_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [at].[Period] ADD  CONSTRAINT [DF_Period_No]  DEFAULT ((1)) FOR [No]
GO
ALTER TABLE [at].[Period] ADD  CONSTRAINT [DF__Period__ExtID__6AF17706]  DEFAULT ('') FOR [ExtID]
GO
ALTER TABLE [at].[Q_C] ADD  CONSTRAINT [DF_Q_C_Weight]  DEFAULT ((1)) FOR [Weight]
GO
ALTER TABLE [at].[Q_C] ADD  CONSTRAINT [DF_Q_C_No]  DEFAULT ((0)) FOR [No]
GO
ALTER TABLE [at].[Q_C] ADD  CONSTRAINT [DF_Q_C_Visible]  DEFAULT ((1)) FOR [Visible]
GO
ALTER TABLE [at].[Question] ADD  CONSTRAINT [DF_Question_No]  DEFAULT ((1)) FOR [No]
GO
ALTER TABLE [at].[Question] ADD  CONSTRAINT [DF_Question_QuestionType]  DEFAULT ((0)) FOR [Type]
GO
ALTER TABLE [at].[Question] ADD  CONSTRAINT [DF_Question_Inverted]  DEFAULT ((0)) FOR [Inverted]
GO
ALTER TABLE [at].[Question] ADD  CONSTRAINT [DF_Question_Mandatory]  DEFAULT ((0)) FOR [Mandatory]
GO
ALTER TABLE [at].[Question] ADD  CONSTRAINT [DF_Question_Status]  DEFAULT ((0)) FOR [Status]
GO
ALTER TABLE [at].[Question] ADD  CONSTRAINT [DF_Question_CssClass]  DEFAULT ('') FOR [CssClass]
GO
ALTER TABLE [at].[Question] ADD  CONSTRAINT [DF_Question_Variable]  DEFAULT ('') FOR [ExtId]
GO
ALTER TABLE [at].[Question] ADD  CONSTRAINT [DF_Question_Tag]  DEFAULT ('') FOR [Tag]
GO
ALTER TABLE [at].[Question] ADD  CONSTRAINT [DF_Question_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [at].[Role] ADD  CONSTRAINT [DF_Role_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [at].[Role] ADD  CONSTRAINT [DF_Role_No]  DEFAULT ((1)) FOR [No]
GO
ALTER TABLE [at].[Role] ADD  CONSTRAINT [DF_Role_Type]  DEFAULT ((0)) FOR [Type]
GO
ALTER TABLE [at].[Scale] ADD  CONSTRAINT [DF_Scale_MinSelect]  DEFAULT ((1)) FOR [MinSelect]
GO
ALTER TABLE [at].[Scale] ADD  CONSTRAINT [DF_Scale_MaxSelect]  DEFAULT ((1)) FOR [MaxSelect]
GO
ALTER TABLE [at].[Scale] ADD  CONSTRAINT [DF_AlternativeGroup_Type]  DEFAULT ((0)) FOR [Type]
GO
ALTER TABLE [at].[Scale] ADD  CONSTRAINT [DF_AlternativeGroup_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [at].[Scale] ADD  CONSTRAINT [DF_Scale_Tag]  DEFAULT ('') FOR [Tag]
GO
ALTER TABLE [at].[Scale] ADD  CONSTRAINT [DF_Scale_Extid]  DEFAULT ('') FOR [ExtID]
GO
ALTER TABLE [at].[Survey] ADD  CONSTRAINT [DF_Execution_Anonymous]  DEFAULT ((0)) FOR [Anonymous]
GO
ALTER TABLE [at].[Survey] ADD  CONSTRAINT [DF_Execution_Status]  DEFAULT ((0)) FOR [Status]
GO
ALTER TABLE [at].[Survey] ADD  CONSTRAINT [DF_Execution_ShowBack]  DEFAULT ((1)) FOR [ShowBack]
GO
ALTER TABLE [at].[Survey] ADD  CONSTRAINT [DF_Execution_DefLanguageID]  DEFAULT ((1)) FOR [LanguageID]
GO
ALTER TABLE [at].[Survey] ADD  CONSTRAINT [DF_Execution_ButtonPlacement]  DEFAULT ((0)) FOR [ButtonPlacement]
GO
ALTER TABLE [at].[Survey] ADD  CONSTRAINT [DF_Execution_UsePageNo]  DEFAULT ((1)) FOR [UsePageNo]
GO
ALTER TABLE [at].[Survey] ADD  CONSTRAINT [DF_Execution_LinkURL]  DEFAULT ('') FOR [LinkURL]
GO
ALTER TABLE [at].[Survey] ADD  CONSTRAINT [DF_Execution_FinishURL]  DEFAULT ('') FOR [FinishURL]
GO
ALTER TABLE [at].[Survey] ADD  CONSTRAINT [DF_Survey_CreateResult]  DEFAULT ((0)) FOR [CreateResult]
GO
ALTER TABLE [at].[Survey] ADD  CONSTRAINT [DF_Survey_ReportDB]  DEFAULT ('') FOR [ReportDB]
GO
ALTER TABLE [at].[Survey] ADD  CONSTRAINT [DF_Survey_ReportServer]  DEFAULT ('') FOR [ReportServer]
GO
ALTER TABLE [at].[Survey] ADD  CONSTRAINT [DF_Survey_StyleSheet]  DEFAULT ('') FOR [StyleSheet]
GO
ALTER TABLE [at].[Survey] ADD  CONSTRAINT [DF_Survey_Type]  DEFAULT ((0)) FOR [Type]
GO
ALTER TABLE [at].[Survey] ADD  CONSTRAINT [DF_Execution_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [at].[Survey] ADD  CONSTRAINT [DF__Survey__LastProc__0B129727]  DEFAULT ('1900-01-01') FOR [LastProcessed]
GO
ALTER TABLE [at].[Survey] ADD  CONSTRAINT [DF_Survey_ReProcessOLAP]  DEFAULT ((0)) FOR [ReProcessOLAP]
GO
ALTER TABLE [at].[Survey] ADD  CONSTRAINT [DF_Survey_No]  DEFAULT ((0)) FOR [No]
GO
ALTER TABLE [at].[Survey] ADD  CONSTRAINT [DF_Survey_OLAPServer]  DEFAULT ('') FOR [OLAPServer]
GO
ALTER TABLE [at].[Survey] ADD  CONSTRAINT [DF_Survey_OLAPDB]  DEFAULT ('') FOR [OLAPDB]
GO
ALTER TABLE [at].[Survey] ADD  CONSTRAINT [DF_Survey_ProcessCategorys]  DEFAULT ((0)) FOR [ProcessCategorys]
GO
ALTER TABLE [at].[Survey] ADD  CONSTRAINT [DF__Survey__DeleteRe__7BB1EEB4]  DEFAULT ((0)) FOR [DeleteResultOnUserDelete]
GO
ALTER TABLE [at].[Survey] ADD  CONSTRAINT [DF_Survey_ExtId]  DEFAULT ('') FOR [ExtId]
GO
ALTER TABLE [at].[Survey] ADD  CONSTRAINT [DF_Survey_Tag]  DEFAULT ('') FOR [Tag]
GO
ALTER TABLE [at].[TMP_Activity] ADD  CONSTRAINT [DF_Activity_LanguageID]  DEFAULT ((1)) FOR [LanguageID]
GO
ALTER TABLE [at].[TMP_Activity] ADD  CONSTRAINT [DF_Activity_StyleSheet]  DEFAULT ('') FOR [StyleSheet]
GO
ALTER TABLE [at].[TMP_Activity] ADD  CONSTRAINT [DF_Activity_Type]  DEFAULT ((0)) FOR [Type]
GO
ALTER TABLE [at].[TMP_Activity] ADD  CONSTRAINT [DF_Activity_No]  DEFAULT ((0)) FOR [No]
GO
ALTER TABLE [at].[TMP_Activity] ADD  CONSTRAINT [DF_Activity_TooltipType]  DEFAULT ((0)) FOR [TooltipType]
GO
ALTER TABLE [at].[TMP_Activity] ADD  CONSTRAINT [DF_Activity_Listview]  DEFAULT ((0)) FOR [Listview]
GO
ALTER TABLE [at].[TMP_Activity] ADD  CONSTRAINT [DF_Activity_Descview]  DEFAULT ((0)) FOR [Descview]
GO
ALTER TABLE [at].[TMP_Activity] ADD  CONSTRAINT [DF_Activity_Chartview]  DEFAULT ((0)) FOR [Chartview]
GO
ALTER TABLE [at].[TMP_Activity] ADD  CONSTRAINT [DF_Activity_OLAPServer]  DEFAULT ('') FOR [OLAPServer]
GO
ALTER TABLE [at].[TMP_Activity] ADD  CONSTRAINT [DF_Activity_OLAPDB]  DEFAULT ('') FOR [OLAPDB]
GO
ALTER TABLE [at].[TMP_Activity] ADD  CONSTRAINT [DF_Activity_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [at].[TMP_Activity] ADD  CONSTRAINT [DF_SelectionHeaderType]  DEFAULT ((0)) FOR [SelectionHeaderType]
GO
ALTER TABLE [at].[TMP_Activity] ADD  CONSTRAINT [DF_Activity_ReportServer]  DEFAULT ('') FOR [ReportServer]
GO
ALTER TABLE [at].[TMP_Activity] ADD  CONSTRAINT [DF_Activity_ReportDB]  DEFAULT ('') FOR [ReportDB]
GO
ALTER TABLE [at].[TMP_Activity] ADD  CONSTRAINT [DF_Activity_ExtID]  DEFAULT ('') FOR [ExtID]
GO
ALTER TABLE [at].[TMP_Activity] ADD  CONSTRAINT [DF_Activity_UseOLAP]  DEFAULT ((1)) FOR [UseOLAP]
GO
ALTER TABLE [at].[TMP_Activity] ADD  CONSTRAINT [DF_Activity_ArchetypeID]  DEFAULT (NULL) FOR [ArchetypeID]
GO
ALTER TABLE [at].[TMP_Alternative] ADD  CONSTRAINT [DF_Alternative_Type]  DEFAULT ((0)) FOR [Type]
GO
ALTER TABLE [at].[TMP_Alternative] ADD  CONSTRAINT [DF_Alternative_AltNo]  DEFAULT ((0)) FOR [No]
GO
ALTER TABLE [at].[TMP_Alternative] ADD  CONSTRAINT [DF_Alternative_Weight]  DEFAULT ((0)) FOR [Value]
GO
ALTER TABLE [at].[TMP_Alternative] ADD  CONSTRAINT [DF_Alternative_InvertedValue]  DEFAULT ((0)) FOR [InvertedValue]
GO
ALTER TABLE [at].[TMP_Alternative] ADD  CONSTRAINT [DF_Alternative_Calc]  DEFAULT ((1)) FOR [Calc]
GO
ALTER TABLE [at].[TMP_Alternative] ADD  CONSTRAINT [DF_Alternative_SC]  DEFAULT ((0)) FOR [SC]
GO
ALTER TABLE [at].[TMP_Alternative] ADD  CONSTRAINT [DF_Alternative_MinValue]  DEFAULT ((0)) FOR [MinValue]
GO
ALTER TABLE [at].[TMP_Alternative] ADD  CONSTRAINT [DF_Alternative_MaxValue]  DEFAULT ((0)) FOR [MaxValue]
GO
ALTER TABLE [at].[TMP_Alternative] ADD  CONSTRAINT [DF_Alternative_Format]  DEFAULT ('') FOR [Format]
GO
ALTER TABLE [at].[TMP_Alternative] ADD  CONSTRAINT [DF_Alternative_Size]  DEFAULT ((0)) FOR [Size]
GO
ALTER TABLE [at].[TMP_Alternative] ADD  CONSTRAINT [DF_Alternative_CssClass]  DEFAULT ('') FOR [CssClass]
GO
ALTER TABLE [at].[TMP_Alternative] ADD  CONSTRAINT [DF_Alternative_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [at].[TMP_Alternative] ADD  CONSTRAINT [DF_Alternative_DefaultValue]  DEFAULT ('') FOR [DefaultValue]
GO
ALTER TABLE [at].[TMP_Alternative] ADD  CONSTRAINT [DF_Alternative_Tag]  DEFAULT ('') FOR [Tag]
GO
ALTER TABLE [at].[TMP_Alternative] ADD  CONSTRAINT [DF_Alternative_ExtID]  DEFAULT ('') FOR [ExtID]
GO
ALTER TABLE [at].[TMP_Alternative] ADD  CONSTRAINT [DF_Alternative_Width]  DEFAULT ('') FOR [Width]
GO
ALTER TABLE [at].[TMP_Alternative] ADD  CONSTRAINT [DF_Alternative_DefaultCalcType]  DEFAULT ((-1)) FOR [DefaultCalcType]
GO
ALTER TABLE [at].[TMP_Alternative] ADD  CONSTRAINT [DF_Alternative_UseEncryption]  DEFAULT ((0)) FOR [UseEncryption]
GO
ALTER TABLE [at].[TMP_Batch] ADD  CONSTRAINT [DF_Batch_No]  DEFAULT ((0)) FOR [No]
GO
ALTER TABLE [at].[TMP_Batch] ADD  CONSTRAINT [DF_Batch_Status]  DEFAULT ((0)) FOR [Status]
GO
ALTER TABLE [at].[TMP_Batch] ADD  CONSTRAINT [DF_Batch_MailStatus]  DEFAULT ((0)) FOR [MailStatus]
GO
ALTER TABLE [at].[TMP_Batch] ADD  CONSTRAINT [DF_Batch_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [at].[TMP_Batch] ADD  CONSTRAINT [DF_Batch_ExtID]  DEFAULT ('') FOR [ExtID]
GO
ALTER TABLE [at].[TMP_Category] ADD  CONSTRAINT [DF_Category_Tag]  DEFAULT ('') FOR [Tag]
GO
ALTER TABLE [at].[TMP_Category] ADD  CONSTRAINT [DF_Category_Type]  DEFAULT ((0)) FOR [Type]
GO
ALTER TABLE [at].[TMP_Category] ADD  CONSTRAINT [DF_Category_No]  DEFAULT ((0)) FOR [No]
GO
ALTER TABLE [at].[TMP_Category] ADD  CONSTRAINT [DF_Category_Fillcolor]  DEFAULT ('') FOR [Fillcolor]
GO
ALTER TABLE [at].[TMP_Category] ADD  CONSTRAINT [DF_Category_BorderColor]  DEFAULT ('') FOR [BorderColor]
GO
ALTER TABLE [at].[TMP_Category] ADD  CONSTRAINT [DF_Category_Weight]  DEFAULT ((1.0)) FOR [Weight]
GO
ALTER TABLE [at].[TMP_Category] ADD  CONSTRAINT [DF_Category_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [at].[TMP_Category] ADD  CONSTRAINT [DF__Category__NType__20CE64CA]  DEFAULT ((0)) FOR [NType]
GO
ALTER TABLE [at].[TMP_Category] ADD  CONSTRAINT [DF_Category_ProcessCategory]  DEFAULT ((0)) FOR [ProcessCategory]
GO
ALTER TABLE [at].[TMP_Category] ADD  CONSTRAINT [DF_Category_ExtID]  DEFAULT ('') FOR [ExtID]
GO
ALTER TABLE [at].[TMP_Category] ADD  CONSTRAINT [DF_Category_RenderAsDefault]  DEFAULT ((1)) FOR [RenderAsDefault]
GO
ALTER TABLE [at].[TMP_Category] ADD  CONSTRAINT [DF_category_AvgType]  DEFAULT ((0)) FOR [AvgType]
GO
ALTER TABLE [at].[TMP_Category] ADD  CONSTRAINT [DF_Category_DefaultState]  DEFAULT ((0)) FOR [DefaultState]
GO
ALTER TABLE [at].[TMP_LevelGroup] ADD  CONSTRAINT [TMP_DF_LevelGroup_Tag]  DEFAULT ('') FOR [Tag]
GO
ALTER TABLE [at].[TMP_LevelGroup] ADD  CONSTRAINT [TMP_DF_LevelGroup_No]  DEFAULT ((0)) FOR [No]
GO
ALTER TABLE [at].[TMP_LevelGroup] ADD  CONSTRAINT [TMP_DF_LevelGroup_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [at].[TMP_LevelLimit] ADD  CONSTRAINT [TMP_DF_LevelLimit_Sigchange]  DEFAULT ((0)) FOR [Sigchange]
GO
ALTER TABLE [at].[TMP_LevelLimit] ADD  CONSTRAINT [TMP_DF_LevelLimit_Fillcolor]  DEFAULT ('') FOR [OwnerColorID]
GO
ALTER TABLE [at].[TMP_LevelLimit] ADD  CONSTRAINT [TMP_DF_LevelLimit_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [at].[TMP_LevelLimit] ADD  CONSTRAINT [TMP_DF__LevelLimi__Negat__5AF01967]  DEFAULT ((0)) FOR [NegativeTrend]
GO
ALTER TABLE [at].[TMP_LevelLimit] ADD  CONSTRAINT [TMP_DF_LevelLimit_ExtID]  DEFAULT ('') FOR [ExtID]
GO
ALTER TABLE [at].[TMP_LevelLimit] ADD  CONSTRAINT [DF__LevelLimi__Match__4F12BBB9]  DEFAULT ((0)) FOR [MatchingType]
GO
ALTER TABLE [at].[TMP_LT_Activity] ADD  CONSTRAINT [DF_LT_Activity_Info]  DEFAULT ('') FOR [Info]
GO
ALTER TABLE [at].[TMP_LT_Activity] ADD  CONSTRAINT [DF_LT_Activity_StartText]  DEFAULT ('') FOR [StartText]
GO
ALTER TABLE [at].[TMP_LT_Activity] ADD  CONSTRAINT [DF_LT_Activity_Description]  DEFAULT ('') FOR [Description]
GO
ALTER TABLE [at].[TMP_LT_Activity] ADD  CONSTRAINT [DF_LT_Activity_RoleName]  DEFAULT ('') FOR [RoleName]
GO
ALTER TABLE [at].[TMP_LT_Activity] ADD  CONSTRAINT [DF_LT_Activity_SurveyName]  DEFAULT ('') FOR [SurveyName]
GO
ALTER TABLE [at].[TMP_LT_Activity] ADD  CONSTRAINT [DF_LT_Activity_BatchName]  DEFAULT ('') FOR [BatchName]
GO
ALTER TABLE [at].[TMP_LT_Activity] ADD  CONSTRAINT [DF_LT_Activity_DisplayName]  DEFAULT ('') FOR [DisplayName]
GO
ALTER TABLE [at].[TMP_LT_Activity] ADD  CONSTRAINT [DF_LT_Activity_ShortName]  DEFAULT ('') FOR [ShortName]
GO
ALTER TABLE [at].[TMP_LT_Alternative] ADD  CONSTRAINT [TMP_DF_LT_Alternative_Description]  DEFAULT ('') FOR [Description]
GO
ALTER TABLE [at].[TMP_LT_Category] ADD  CONSTRAINT [DF_LT_Category_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [at].[TMP_LT_Category] ADD  CONSTRAINT [DF__LT_Catego__Descr__489AC854]  DEFAULT ('') FOR [Description]
GO
ALTER TABLE [at].[TMP_LT_Category] ADD  CONSTRAINT [DF_LT_Category_DescriptionExtended]  DEFAULT ('') FOR [DescriptionExtended]
GO
ALTER TABLE [at].[TMP_LT_LevelGroup] ADD  CONSTRAINT [TMP_DF_LT_LevelGroup_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [at].[TMP_LT_LevelLimit] ADD  CONSTRAINT [TMP_DF_LT_LevelLimit_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [at].[TMP_LT_LevelLimit] ADD  CONSTRAINT [TMP_DF_LT_LevelLimit_Description]  DEFAULT ('') FOR [Description]
GO
ALTER TABLE [at].[TMP_LT_Page] ADD  CONSTRAINT [TMP_DF_LT_Page_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [at].[TMP_LT_Page] ADD  CONSTRAINT [TMP_DF_LT_Page_Info]  DEFAULT ('') FOR [Info]
GO
ALTER TABLE [at].[TMP_LT_Page] ADD  CONSTRAINT [TMP_DF_LT_Page_ToolTip]  DEFAULT ('') FOR [Description]
GO
ALTER TABLE [at].[TMP_LT_Question] ADD  CONSTRAINT [TMP_DF_LT_Question_Text]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [at].[TMP_LT_Question] ADD  CONSTRAINT [TMP_DF_LT_Question_HeadLine]  DEFAULT ('') FOR [Title]
GO
ALTER TABLE [at].[TMP_LT_Question] ADD  CONSTRAINT [TMP_DF_LT_Question_ReportText]  DEFAULT ('') FOR [ReportText]
GO
ALTER TABLE [at].[TMP_LT_Question] ADD  CONSTRAINT [TMP_DF_LT_Question_ToolTip]  DEFAULT ('') FOR [Description]
GO
ALTER TABLE [at].[TMP_LT_Question] ADD  CONSTRAINT [TMP_DF__LT_Questi__Short__30F9DF9B]  DEFAULT ('') FOR [ShortName]
GO
ALTER TABLE [at].[TMP_LT_Role] ADD  CONSTRAINT [TMP_DF_LT_Role_Description]  DEFAULT ('') FOR [Description]
GO
ALTER TABLE [at].[TMP_LT_Scale] ADD  CONSTRAINT [TMP_DF_LT_AlternativeGroup_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [at].[TMP_LT_Scale] ADD  CONSTRAINT [TMP_DF_LT_AlternativeGroup_Title]  DEFAULT ('') FOR [Title]
GO
ALTER TABLE [at].[TMP_LT_Scale] ADD  CONSTRAINT [TMP_DF_LT_Scale_Description]  DEFAULT ('') FOR [Description]
GO
ALTER TABLE [at].[TMP_LT_Survey] ADD  CONSTRAINT [TMP_DF_LT_Execution_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [at].[TMP_LT_Survey] ADD  CONSTRAINT [TMP_DF_LT_Execution_Description]  DEFAULT ('') FOR [Description]
GO
ALTER TABLE [at].[TMP_LT_Survey] ADD  CONSTRAINT [TMP_DF_LT_Execution_Info]  DEFAULT ('') FOR [Info]
GO
ALTER TABLE [at].[TMP_LT_Survey] ADD  CONSTRAINT [TMP_DF_LT_Execution_FinishText]  DEFAULT ('') FOR [FinishText]
GO
ALTER TABLE [at].[TMP_LT_Survey] ADD  CONSTRAINT [TMP_DF__LT_Survey__Displ__4E00329D]  DEFAULT ('') FOR [DisplayName]
GO
ALTER TABLE [at].[TMP_LT_XCategory] ADD  CONSTRAINT [TMP_DF_LT_XCategory_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [at].[TMP_LT_XCategory] ADD  CONSTRAINT [TMP_DF_LT_XCategory_Description]  DEFAULT ('') FOR [Description]
GO
ALTER TABLE [at].[TMP_Page] ADD  CONSTRAINT [TMP_DF_Page_No]  DEFAULT ((1)) FOR [No]
GO
ALTER TABLE [at].[TMP_Page] ADD  CONSTRAINT [TMP_DF_Page_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [at].[TMP_Page] ADD  CONSTRAINT [TMP_DF_Page_Type]  DEFAULT ((0)) FOR [Type]
GO
ALTER TABLE [at].[TMP_Page] ADD  CONSTRAINT [TMP_DF__Page__CssClass__501D8539]  DEFAULT ('') FOR [CssClass]
GO
ALTER TABLE [at].[TMP_Page] ADD  CONSTRAINT [TMP_DF_Page_ExtID]  DEFAULT ('') FOR [ExtID]
GO
ALTER TABLE [at].[TMP_Page] ADD  CONSTRAINT [TMP_DF_Page_Tag]  DEFAULT ('') FOR [Tag]
GO
ALTER TABLE [at].[TMP_Period] ADD  CONSTRAINT [TMP_DF_Period_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [at].[TMP_Period] ADD  CONSTRAINT [TMP_DF_Period_No]  DEFAULT ((1)) FOR [No]
GO
ALTER TABLE [at].[TMP_Period] ADD  CONSTRAINT [TMP_DF__Period__ExtID__6AF17706]  DEFAULT ('') FOR [ExtID]
GO
ALTER TABLE [at].[TMP_Q_C] ADD  CONSTRAINT [TMP_DF_Q_C_Weight]  DEFAULT ((1)) FOR [Weight]
GO
ALTER TABLE [at].[TMP_Q_C] ADD  CONSTRAINT [TMP_DF_Q_C_No]  DEFAULT ((0)) FOR [No]
GO
ALTER TABLE [at].[TMP_Q_C] ADD  CONSTRAINT [TMP_DF_Q_C_Visible]  DEFAULT ((1)) FOR [Visible]
GO
ALTER TABLE [at].[TMP_Question] ADD  CONSTRAINT [TMP_DF_Question_No]  DEFAULT ((1)) FOR [No]
GO
ALTER TABLE [at].[TMP_Question] ADD  CONSTRAINT [TMP_DF_Question_QuestionType]  DEFAULT ((0)) FOR [Type]
GO
ALTER TABLE [at].[TMP_Question] ADD  CONSTRAINT [TMP_DF_Question_Inverted]  DEFAULT ((0)) FOR [Inverted]
GO
ALTER TABLE [at].[TMP_Question] ADD  CONSTRAINT [TMP_DF_Question_Mandatory]  DEFAULT ((0)) FOR [Mandatory]
GO
ALTER TABLE [at].[TMP_Question] ADD  CONSTRAINT [TMP_DF_Question_Status]  DEFAULT ((0)) FOR [Status]
GO
ALTER TABLE [at].[TMP_Question] ADD  CONSTRAINT [TMP_DF_Question_CssClass]  DEFAULT ('') FOR [CssClass]
GO
ALTER TABLE [at].[TMP_Question] ADD  CONSTRAINT [TMP_DF_Question_Variable]  DEFAULT ('') FOR [ExtId]
GO
ALTER TABLE [at].[TMP_Question] ADD  CONSTRAINT [TMP_DF_Question_Tag]  DEFAULT ('') FOR [Tag]
GO
ALTER TABLE [at].[TMP_Question] ADD  CONSTRAINT [TMP_DF_Question_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [at].[TMP_Role] ADD  CONSTRAINT [TMP_DF_Role_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [at].[TMP_Role] ADD  CONSTRAINT [TMP_DF_Role_No]  DEFAULT ((1)) FOR [No]
GO
ALTER TABLE [at].[TMP_Role] ADD  CONSTRAINT [TMP_DF_Role_Type]  DEFAULT ((0)) FOR [Type]
GO
ALTER TABLE [at].[TMP_Scale] ADD  CONSTRAINT [TMP_DF_Scale_MinSelect]  DEFAULT ((1)) FOR [MinSelect]
GO
ALTER TABLE [at].[TMP_Scale] ADD  CONSTRAINT [TMP_DF_Scale_MaxSelect]  DEFAULT ((1)) FOR [MaxSelect]
GO
ALTER TABLE [at].[TMP_Scale] ADD  CONSTRAINT [TMP_DF_AlternativeGroup_Type]  DEFAULT ((0)) FOR [Type]
GO
ALTER TABLE [at].[TMP_Scale] ADD  CONSTRAINT [TMP_DF_AlternativeGroup_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [at].[TMP_Scale] ADD  CONSTRAINT [TMP_DF_Scale_Tag]  DEFAULT ('') FOR [Tag]
GO
ALTER TABLE [at].[TMP_Scale] ADD  CONSTRAINT [TMP_DF_Scale_Extid]  DEFAULT ('') FOR [ExtID]
GO
ALTER TABLE [at].[TMP_Survey] ADD  CONSTRAINT [TMP_DF_Execution_Anonymous]  DEFAULT ((0)) FOR [Anonymous]
GO
ALTER TABLE [at].[TMP_Survey] ADD  CONSTRAINT [TMP_DF_Execution_Status]  DEFAULT ((0)) FOR [Status]
GO
ALTER TABLE [at].[TMP_Survey] ADD  CONSTRAINT [TMP_DF_Execution_ShowBack]  DEFAULT ((1)) FOR [ShowBack]
GO
ALTER TABLE [at].[TMP_Survey] ADD  CONSTRAINT [TMP_DF_Execution_DefLanguageID]  DEFAULT ((1)) FOR [LanguageID]
GO
ALTER TABLE [at].[TMP_Survey] ADD  CONSTRAINT [TMP_DF_Execution_ButtonPlacement]  DEFAULT ((0)) FOR [ButtonPlacement]
GO
ALTER TABLE [at].[TMP_Survey] ADD  CONSTRAINT [TMP_DF_Execution_UsePageNo]  DEFAULT ((1)) FOR [UsePageNo]
GO
ALTER TABLE [at].[TMP_Survey] ADD  CONSTRAINT [TMP_DF_Execution_LinkURL]  DEFAULT ('') FOR [LinkURL]
GO
ALTER TABLE [at].[TMP_Survey] ADD  CONSTRAINT [TMP_DF_Execution_FinishURL]  DEFAULT ('') FOR [FinishURL]
GO
ALTER TABLE [at].[TMP_Survey] ADD  CONSTRAINT [TMP_DF_Survey_CreateResult]  DEFAULT ((0)) FOR [CreateResult]
GO
ALTER TABLE [at].[TMP_Survey] ADD  CONSTRAINT [TMP_DF_Survey_ReportDB]  DEFAULT ('') FOR [ReportDB]
GO
ALTER TABLE [at].[TMP_Survey] ADD  CONSTRAINT [TMP_DF_Survey_ReportServer]  DEFAULT ('') FOR [ReportServer]
GO
ALTER TABLE [at].[TMP_Survey] ADD  CONSTRAINT [TMP_DF_Survey_StyleSheet]  DEFAULT ('') FOR [StyleSheet]
GO
ALTER TABLE [at].[TMP_Survey] ADD  CONSTRAINT [TMP_DF_Survey_Type]  DEFAULT ((0)) FOR [Type]
GO
ALTER TABLE [at].[TMP_Survey] ADD  CONSTRAINT [TMP_DF_Execution_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [at].[TMP_Survey] ADD  CONSTRAINT [TMP_DF__Survey__LastProc__0B129727]  DEFAULT ('1900-01-01') FOR [LastProcessed]
GO
ALTER TABLE [at].[TMP_Survey] ADD  CONSTRAINT [TMP_DF_Survey_No]  DEFAULT ((0)) FOR [No]
GO
ALTER TABLE [at].[TMP_Survey] ADD  CONSTRAINT [TMP_DF_Survey_OLAPServer]  DEFAULT ('') FOR [OLAPServer]
GO
ALTER TABLE [at].[TMP_Survey] ADD  CONSTRAINT [TMP_DF_Survey_OLAPDB]  DEFAULT ('') FOR [OLAPDB]
GO
ALTER TABLE [at].[TMP_Survey] ADD  CONSTRAINT [TMP_DF_Survey_ProcessCategorys]  DEFAULT ((0)) FOR [ProcessCategorys]
GO
ALTER TABLE [at].[TMP_Survey] ADD  CONSTRAINT [TMP_DF__Survey__DeleteRe__7BB1EEB4]  DEFAULT ((0)) FOR [DeleteResultOnUserDelete]
GO
ALTER TABLE [at].[TMP_Survey] ADD  CONSTRAINT [TMP_DF_Survey_ExtId]  DEFAULT ('') FOR [ExtId]
GO
ALTER TABLE [at].[TMP_Survey] ADD  CONSTRAINT [TMP_DF_Survey_Tag]  DEFAULT ('') FOR [Tag]
GO
ALTER TABLE [at].[TMP_XC_A] ADD  CONSTRAINT [TMP_DF_XC_A_No]  DEFAULT ((0)) FOR [No]
GO
ALTER TABLE [at].[TMP_XC_A] ADD  CONSTRAINT [TMP_DF_XC_A_Listview]  DEFAULT ((0)) FOR [Listview]
GO
ALTER TABLE [at].[TMP_XC_A] ADD  CONSTRAINT [TMP_DF_XC_A_Descview]  DEFAULT ((0)) FOR [Descview]
GO
ALTER TABLE [at].[TMP_XC_A] ADD  CONSTRAINT [TMP_DF_XC_A_Chartview]  DEFAULT ((0)) FOR [Chartview]
GO
ALTER TABLE [at].[TMP_XC_A] ADD  CONSTRAINT [TMP_DF_XC_A_Fillcolor]  DEFAULT ('') FOR [FillColor]
GO
ALTER TABLE [at].[TMP_XC_A] ADD  CONSTRAINT [TMP_DF_XC_A_BorderColor]  DEFAULT ('') FOR [BorderColor]
GO
ALTER TABLE [at].[TMP_XCategory] ADD  CONSTRAINT [TMP_DF_XCategory_Type]  DEFAULT ((0)) FOR [Type]
GO
ALTER TABLE [at].[TMP_XCategory] ADD  CONSTRAINT [TMP_DF_XCategory_Status]  DEFAULT ((0)) FOR [Status]
GO
ALTER TABLE [at].[TMP_XCategory] ADD  CONSTRAINT [TMP_DF_XCategory_No]  DEFAULT ((0)) FOR [No]
GO
ALTER TABLE [at].[TMP_XCategory] ADD  CONSTRAINT [TMP_DF_XCategory_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [at].[TMP_XCategory] ADD  CONSTRAINT [TMP_DF_XCategory_ExtID]  DEFAULT ('') FOR [ExtId]
GO
ALTER TABLE [at].[TMP_XCategory] ADD  CONSTRAINT [TMP_DF_XCategory_MasterID]  DEFAULT (newid()) FOR [MasterID]
GO
ALTER TABLE [at].[XC_A] ADD  CONSTRAINT [DF_XC_A_No]  DEFAULT ((0)) FOR [No]
GO
ALTER TABLE [at].[XC_A] ADD  CONSTRAINT [DF_XC_A_Listview]  DEFAULT ((0)) FOR [Listview]
GO
ALTER TABLE [at].[XC_A] ADD  CONSTRAINT [DF_XC_A_Descview]  DEFAULT ((0)) FOR [Descview]
GO
ALTER TABLE [at].[XC_A] ADD  CONSTRAINT [DF_XC_A_Chartview]  DEFAULT ((0)) FOR [Chartview]
GO
ALTER TABLE [at].[XC_A] ADD  CONSTRAINT [DF_XC_A_Fillcolor]  DEFAULT ('') FOR [FillColor]
GO
ALTER TABLE [at].[XC_A] ADD  CONSTRAINT [DF_XC_A_BorderColor]  DEFAULT ('') FOR [BorderColor]
GO
ALTER TABLE [at].[XCategory] ADD  CONSTRAINT [DF_XCategory_Type]  DEFAULT ((0)) FOR [Type]
GO
ALTER TABLE [at].[XCategory] ADD  CONSTRAINT [DF_XCategory_Status]  DEFAULT ((0)) FOR [Status]
GO
ALTER TABLE [at].[XCategory] ADD  CONSTRAINT [DF_XCategory_No]  DEFAULT ((0)) FOR [No]
GO
ALTER TABLE [at].[XCategory] ADD  CONSTRAINT [DF_XCategory_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [at].[XCategory] ADD  CONSTRAINT [DF_XCategory_ExtID]  DEFAULT ('') FOR [ExtId]
GO
ALTER TABLE [at].[XCategory] ADD  CONSTRAINT [DF_XCategory_MasterID]  DEFAULT (newid()) FOR [MasterID]
GO
ALTER TABLE [dbo].[Answer] ADD  DEFAULT ((0)) FOR [No]
GO
ALTER TABLE [dbo].[Answer] ADD  CONSTRAINT [DF_Answer_Free]  DEFAULT ('') FOR [Free]
GO
ALTER TABLE [dbo].[Answer] ADD  CONSTRAINT [DF_Answer_LastUpdated]  DEFAULT (getdate()) FOR [LastUpdated]
GO
ALTER TABLE [dbo].[Answer] ADD  CONSTRAINT [DF_Answer_LastUpdatedBy]  DEFAULT ((0)) FOR [LastUpdatedBy]
GO
ALTER TABLE [dbo].[EntityStatus] ADD  CONSTRAINT [DF_EntityStatus_CodeName]  DEFAULT ('') FOR [CodeName]
GO
ALTER TABLE [dbo].[EntityStatusReason] ADD  CONSTRAINT [DF_EntityStatusReason_CodeName]  DEFAULT ('') FOR [CodeName]
GO
ALTER TABLE [dbo].[Language] ADD  CONSTRAINT [DF_Language_LanguageCode]  DEFAULT ('') FOR [LanguageCode]
GO
ALTER TABLE [dbo].[Language] ADD  CONSTRAINT [DF_Language_Dir]  DEFAULT ('ltr') FOR [Dir]
GO
ALTER TABLE [dbo].[Language] ADD  CONSTRAINT [DF_Language_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [dbo].[Language] ADD  CONSTRAINT [DF_Language_NativeName]  DEFAULT ('') FOR [NativeName]
GO
ALTER TABLE [dbo].[Language] ADD  CONSTRAINT [DF_Language_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [dbo].[LT_EntityStatus] ADD  CONSTRAINT [DF_LT_EntityStatus_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [dbo].[LT_EntityStatus] ADD  CONSTRAINT [DF_LT_EntityStatus_Description]  DEFAULT ('') FOR [Description]
GO
ALTER TABLE [dbo].[LT_EntityStatusReason] ADD  CONSTRAINT [DF_LT_EntityStatusReason_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [dbo].[LT_EntityStatusReason] ADD  CONSTRAINT [DF_LT_EntityStatusReason_Description]  DEFAULT ('') FOR [Description]
GO
ALTER TABLE [dbo].[Result] ADD  CONSTRAINT [DF_Result_LastQuest]  DEFAULT ((0)) FOR [PageNo]
GO
ALTER TABLE [dbo].[Result] ADD  CONSTRAINT [DF_Result_anonymous]  DEFAULT ((0)) FOR [Anonymous]
GO
ALTER TABLE [dbo].[Result] ADD  CONSTRAINT [DF_Result_ShowBack]  DEFAULT ((1)) FOR [ShowBack]
GO
ALTER TABLE [dbo].[Result] ADD  CONSTRAINT [DF_Result_ResultKey]  DEFAULT ('') FOR [ResultKey]
GO
ALTER TABLE [dbo].[Result] ADD  CONSTRAINT [DF_Result_Email]  DEFAULT ('') FOR [Email]
GO
ALTER TABLE [dbo].[Result] ADD  CONSTRAINT [DF_Result_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [dbo].[Result] ADD  DEFAULT (getdate()) FOR [LastUpdated]
GO
ALTER TABLE [dbo].[Result] ADD  DEFAULT ((0)) FOR [LastUpdatedBy]
GO
ALTER TABLE [dbo].[Result] ADD  CONSTRAINT [DF_Result_Deleted]  DEFAULT (NULL) FOR [Deleted]
GO
ALTER TABLE [dbo].[Result] ADD  CONSTRAINT [DF_Result_EntityStatus]  DEFAULT (NULL) FOR [EntityStatusID]
GO
ALTER TABLE [dbo].[Result] ADD  CONSTRAINT [DF_Result_EntityStatusReason]  DEFAULT (NULL) FOR [EntityStatusReasonID]
GO
ALTER TABLE [dbo].[TMP_Answer] ADD  DEFAULT ((0)) FOR [No]
GO
ALTER TABLE [dbo].[TMP_Answer] ADD  CONSTRAINT [TMP_DF_Answer_Free]  DEFAULT ('') FOR [Free]
GO
ALTER TABLE [dbo].[TMP_Answer] ADD  CONSTRAINT [TMP_DF_Answer_LastUpdated]  DEFAULT (getdate()) FOR [LastUpdated]
GO
ALTER TABLE [dbo].[TMP_Answer] ADD  CONSTRAINT [TMP_DF_Answer_LastUpdatedBy]  DEFAULT ((0)) FOR [LastUpdatedBy]
GO
ALTER TABLE [dbo].[TMP_EntityStatusReason] ADD  CONSTRAINT [TMP_DF_EntityStatusReason_CodeName]  DEFAULT ('') FOR [CodeName]
GO
ALTER TABLE [dbo].[TMP_Language] ADD  CONSTRAINT [TMP_DF_Language_LanguageCode]  DEFAULT ('') FOR [LanguageCode]
GO
ALTER TABLE [dbo].[TMP_Language] ADD  CONSTRAINT [TMP_DF_Language_Dir]  DEFAULT ('ltr') FOR [Dir]
GO
ALTER TABLE [dbo].[TMP_Language] ADD  CONSTRAINT [TMP_DF_Language_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [dbo].[TMP_Language] ADD  CONSTRAINT [TMP_DF_Language_NativeName]  DEFAULT ('') FOR [NativeName]
GO
ALTER TABLE [dbo].[TMP_Language] ADD  CONSTRAINT [TMP_DF_Language_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [dbo].[TMP_LT_EntityStatus] ADD  CONSTRAINT [TMP_DF_LT_EntityStatus_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [dbo].[TMP_LT_EntityStatus] ADD  CONSTRAINT [TMP_DF_LT_EntityStatus_Description]  DEFAULT ('') FOR [Description]
GO
ALTER TABLE [dbo].[TMP_LT_EntityStatusReason] ADD  CONSTRAINT [TMP_DF_LT_EntityStatusReason_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [dbo].[TMP_LT_EntityStatusReason] ADD  CONSTRAINT [TMP_DF_LT_EntityStatusReason_Description]  DEFAULT ('') FOR [Description]
GO
ALTER TABLE [dbo].[TMP_Result] ADD  CONSTRAINT [TMP_DF_Result_LastQuest]  DEFAULT ((0)) FOR [PageNo]
GO
ALTER TABLE [dbo].[TMP_Result] ADD  CONSTRAINT [TMP_DF_Result_anonymous]  DEFAULT ((0)) FOR [Anonymous]
GO
ALTER TABLE [dbo].[TMP_Result] ADD  CONSTRAINT [TMP_DF_Result_ShowBack]  DEFAULT ((1)) FOR [ShowBack]
GO
ALTER TABLE [dbo].[TMP_Result] ADD  CONSTRAINT [TMP_DF_Result_ResultKey]  DEFAULT ('') FOR [ResultKey]
GO
ALTER TABLE [dbo].[TMP_Result] ADD  CONSTRAINT [TMP_DF_Result_Email]  DEFAULT ('') FOR [Email]
GO
ALTER TABLE [dbo].[TMP_Result] ADD  CONSTRAINT [TMP_DF_Result_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [dbo].[TMP_Result] ADD  DEFAULT (getdate()) FOR [LastUpdated]
GO
ALTER TABLE [dbo].[TMP_Result] ADD  DEFAULT ((0)) FOR [LastUpdatedBy]
GO
ALTER TABLE [dbo].[TMP_Result] ADD  CONSTRAINT [TMP_DF_Result_Deleted]  DEFAULT (NULL) FOR [Deleted]
GO
ALTER TABLE [dbo].[TMP_Result] ADD  CONSTRAINT [TMP_DF_Result_EntityStatus]  DEFAULT (NULL) FOR [EntityStatusID]
GO
ALTER TABLE [dbo].[TMP_Result] ADD  CONSTRAINT [TMP_DF_Result_EntityStatusReason]  DEFAULT (NULL) FOR [EntityStatusReasonID]
GO
ALTER TABLE [org].[Customer] ADD  CONSTRAINT [DF_Customer_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [org].[Customer] ADD  CONSTRAINT [DF_Customer_Status]  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [org].[Customer] ADD  CONSTRAINT [DF_Customer_ExtID]  DEFAULT ('') FOR [ExtID]
GO
ALTER TABLE [org].[Customer] ADD  CONSTRAINT [DF_Customer_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [org].[Customer] ADD  CONSTRAINT [DF_Customer_HasUserIntegration]  DEFAULT ((0)) FOR [HasUserIntegration]
GO
ALTER TABLE [org].[Customer] ADD  CONSTRAINT [DF_Customer_CodeName]  DEFAULT ('') FOR [CodeName]
GO
ALTER TABLE [org].[Customer] ADD  CONSTRAINT [DF_Customer_Logo]  DEFAULT ('') FOR [Logo]
GO
ALTER TABLE [org].[Customer] ADD  CONSTRAINT [DF_Customer_CssVariables]  DEFAULT ('') FOR [CssVariables]
GO
ALTER TABLE [org].[Department] ADD  CONSTRAINT [DF_Department_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [org].[Department] ADD  CONSTRAINT [DF_Department_Description]  DEFAULT ('') FOR [Description]
GO
ALTER TABLE [org].[Department] ADD  CONSTRAINT [DF_Department_Adress]  DEFAULT ('') FOR [Adress]
GO
ALTER TABLE [org].[Department] ADD  CONSTRAINT [DF_Department_PostalCode]  DEFAULT ('') FOR [PostalCode]
GO
ALTER TABLE [org].[Department] ADD  CONSTRAINT [DF_Department_City]  DEFAULT ('') FOR [City]
GO
ALTER TABLE [org].[Department] ADD  CONSTRAINT [DF_Department_OrgNo]  DEFAULT ('') FOR [OrgNo]
GO
ALTER TABLE [org].[Department] ADD  CONSTRAINT [DF_Department_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [org].[Department] ADD  CONSTRAINT [DF_Department_ExtID]  DEFAULT ('') FOR [ExtID]
GO
ALTER TABLE [org].[Department] ADD  CONSTRAINT [DF_Department_Tag]  DEFAULT ('') FOR [Tag]
GO
ALTER TABLE [org].[Department] ADD  CONSTRAINT [DF__Departmen__Locke__1837881B]  DEFAULT ((0)) FOR [Locked]
GO
ALTER TABLE [org].[Department] ADD  CONSTRAINT [DF_Department_CountryCode]  DEFAULT ((0)) FOR [CountryCode]
GO
ALTER TABLE [org].[Department] ADD  CONSTRAINT [DF_Department_LastUpdated]  DEFAULT (getdate()) FOR [LastUpdated]
GO
ALTER TABLE [org].[Department] ADD  CONSTRAINT [DF_Department_LastUpdatedBy]  DEFAULT (NULL) FOR [LastUpdatedBy]
GO
ALTER TABLE [org].[Department] ADD  CONSTRAINT [DF_Department_LastSynchronized]  DEFAULT (getdate()) FOR [LastSynchronized]
GO
ALTER TABLE [org].[Department] ADD  CONSTRAINT [DF_Department_ArchetypeID]  DEFAULT (NULL) FOR [ArchetypeID]
GO
ALTER TABLE [org].[Department] ADD  CONSTRAINT [DF_Department_Deleted]  DEFAULT (NULL) FOR [Deleted]
GO
ALTER TABLE [org].[Department] ADD  CONSTRAINT [DF_Department_EntityStatus]  DEFAULT (NULL) FOR [EntityStatusID]
GO
ALTER TABLE [org].[Department] ADD  CONSTRAINT [DF_Department_EntityStatusReason]  DEFAULT (NULL) FOR [EntityStatusReasonID]
GO
ALTER TABLE [org].[DepartmentType] ADD  CONSTRAINT [DF_DepartmentType_Tag]  DEFAULT ('') FOR [ExtID]
GO
ALTER TABLE [org].[DepartmentType] ADD  CONSTRAINT [DF_DepartmentType_No]  DEFAULT ((0)) FOR [No]
GO
ALTER TABLE [org].[DepartmentType] ADD  CONSTRAINT [DF_DepartmentType_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [org].[DepartmentType] ADD  CONSTRAINT [DF_DepartmentType_ArchetypeID]  DEFAULT (NULL) FOR [ArchetypeID]
GO
ALTER TABLE [org].[DepartmentType] ADD  CONSTRAINT [DF_DepartmentType_ParentID]  DEFAULT (NULL) FOR [ParentID]
GO
ALTER TABLE [org].[H_D] ADD  CONSTRAINT [DF_H_D_Path]  DEFAULT ('') FOR [Path]
GO
ALTER TABLE [org].[H_D] ADD  CONSTRAINT [DF_H_D_PathName]  DEFAULT ('') FOR [PathName]
GO
ALTER TABLE [org].[H_D] ADD  CONSTRAINT [DF_H_D_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [org].[H_D] ADD  CONSTRAINT [DF_H_D_Deleted]  DEFAULT ((0)) FOR [Deleted]
GO
ALTER TABLE [org].[Hierarchy] ADD  CONSTRAINT [DF_Hierarchy_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [org].[Hierarchy] ADD  CONSTRAINT [DF_Hierarchy_Description]  DEFAULT ('') FOR [Description]
GO
ALTER TABLE [org].[Hierarchy] ADD  CONSTRAINT [DF_Hierarchy_Type]  DEFAULT ((0)) FOR [Type]
GO
ALTER TABLE [org].[Hierarchy] ADD  CONSTRAINT [DF_Hierarchy_Deleted]  DEFAULT ((0)) FOR [Deleted]
GO
ALTER TABLE [org].[Hierarchy] ADD  CONSTRAINT [DF_Hierarchy_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [org].[LT_DepartmentType] ADD  CONSTRAINT [DF_LT_DepartmentType_Description]  DEFAULT ('') FOR [Description]
GO
ALTER TABLE [org].[Owner] ADD  CONSTRAINT [DF_Owner_LanguageID]  DEFAULT ((1)) FOR [LanguageID]
GO
ALTER TABLE [org].[Owner] ADD  CONSTRAINT [DF_Owner_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [org].[Owner] ADD  CONSTRAINT [DF_Owner_ReportServer]  DEFAULT ('') FOR [ReportServer]
GO
ALTER TABLE [org].[Owner] ADD  CONSTRAINT [DF_Owner_ReportDB]  DEFAULT ('') FOR [ReportDB]
GO
ALTER TABLE [org].[Owner] ADD  CONSTRAINT [DF_Owner_OLAPServer]  DEFAULT ('') FOR [OLAPServer]
GO
ALTER TABLE [org].[Owner] ADD  CONSTRAINT [DF_Owner_OLAPDB]  DEFAULT ('') FOR [OLAPDB]
GO
ALTER TABLE [org].[Owner] ADD  CONSTRAINT [DF_Owner_Css]  DEFAULT ('') FOR [Css]
GO
ALTER TABLE [org].[Owner] ADD  CONSTRAINT [DF_Owner_Url]  DEFAULT ('') FOR [Url]
GO
ALTER TABLE [org].[Owner] ADD  CONSTRAINT [DF_Owner_LoginType]  DEFAULT ((0)) FOR [LoginType]
GO
ALTER TABLE [org].[Owner] ADD  CONSTRAINT [DF_Owner_Prefix]  DEFAULT ('') FOR [Prefix]
GO
ALTER TABLE [org].[Owner] ADD  CONSTRAINT [DF_Owner_Description]  DEFAULT ('') FOR [Description]
GO
ALTER TABLE [org].[Owner] ADD  CONSTRAINT [DF_Owner_Logging]  DEFAULT ((1)) FOR [Logging]
GO
ALTER TABLE [org].[TMP_Customer] ADD  CONSTRAINT [TMP_DF_Customer_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [org].[TMP_Customer] ADD  CONSTRAINT [TMP_DF_Customer_Status]  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [org].[TMP_Customer] ADD  CONSTRAINT [TMP_DF_Customer_ExtID]  DEFAULT ('') FOR [ExtID]
GO
ALTER TABLE [org].[TMP_Customer] ADD  CONSTRAINT [TMP_DF_Customer_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [org].[TMP_Customer] ADD  CONSTRAINT [TMP_DF_Customer_HasUserIntegration]  DEFAULT ((0)) FOR [HasUserIntegration]
GO
ALTER TABLE [org].[TMP_Customer] ADD  CONSTRAINT [TMP_DF_Customer_CodeName]  DEFAULT ('') FOR [CodeName]
GO
ALTER TABLE [org].[TMP_Customer] ADD  CONSTRAINT [TMP_DF_Customer_Logo]  DEFAULT ('') FOR [Logo]
GO
ALTER TABLE [org].[TMP_Customer] ADD  CONSTRAINT [TMP_DF_Customer_CssVariables]  DEFAULT ('') FOR [CssVariables]
GO
ALTER TABLE [org].[TMP_Department] ADD  CONSTRAINT [TMP_DF_Department_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [org].[TMP_Department] ADD  CONSTRAINT [TMP_DF_Department_Description]  DEFAULT ('') FOR [Description]
GO
ALTER TABLE [org].[TMP_Department] ADD  CONSTRAINT [TMP_DF_Department_Adress]  DEFAULT ('') FOR [Adress]
GO
ALTER TABLE [org].[TMP_Department] ADD  CONSTRAINT [TMP_DF_Department_PostalCode]  DEFAULT ('') FOR [PostalCode]
GO
ALTER TABLE [org].[TMP_Department] ADD  CONSTRAINT [TMP_DF_Department_City]  DEFAULT ('') FOR [City]
GO
ALTER TABLE [org].[TMP_Department] ADD  CONSTRAINT [TMP_DF_Department_OrgNo]  DEFAULT ('') FOR [OrgNo]
GO
ALTER TABLE [org].[TMP_Department] ADD  CONSTRAINT [TMP_DF_Department_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [org].[TMP_Department] ADD  CONSTRAINT [TMP_DF_Department_ExtID]  DEFAULT ('') FOR [ExtID]
GO
ALTER TABLE [org].[TMP_Department] ADD  CONSTRAINT [TMP_DF_Department_Tag]  DEFAULT ('') FOR [Tag]
GO
ALTER TABLE [org].[TMP_Department] ADD  CONSTRAINT [TMP_DF__Departmen__Locke__1837881B]  DEFAULT ((0)) FOR [Locked]
GO
ALTER TABLE [org].[TMP_Department] ADD  CONSTRAINT [TMP_DF_Department_LastUpdated]  DEFAULT (getdate()) FOR [LastUpdated]
GO
ALTER TABLE [org].[TMP_Department] ADD  CONSTRAINT [TMP_DF_Department_LastUpdatedBy]  DEFAULT (NULL) FOR [LastUpdatedBy]
GO
ALTER TABLE [org].[TMP_Department] ADD  CONSTRAINT [TMP_DF_Department_LastSynchronized]  DEFAULT (getdate()) FOR [LastSynchronized]
GO
ALTER TABLE [org].[TMP_Department] ADD  CONSTRAINT [TMP_DF_Department_ArchetypeID]  DEFAULT (NULL) FOR [ArchetypeID]
GO
ALTER TABLE [org].[TMP_Department] ADD  CONSTRAINT [TMP_DF_Department_Deleted]  DEFAULT (NULL) FOR [Deleted]
GO
ALTER TABLE [org].[TMP_Department] ADD  CONSTRAINT [TMP_DF_Department_EntityStatus]  DEFAULT (NULL) FOR [EntityStatusID]
GO
ALTER TABLE [org].[TMP_Department] ADD  CONSTRAINT [TMP_DF_Department_EntityStatusReason]  DEFAULT (NULL) FOR [EntityStatusReasonID]
GO
ALTER TABLE [org].[TMP_DepartmentType] ADD  CONSTRAINT [TMP_DF_DepartmentType_Tag]  DEFAULT ('') FOR [ExtID]
GO
ALTER TABLE [org].[TMP_DepartmentType] ADD  CONSTRAINT [TMP_DF_DepartmentType_No]  DEFAULT ((0)) FOR [No]
GO
ALTER TABLE [org].[TMP_DepartmentType] ADD  CONSTRAINT [TMP_DF_DepartmentType_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [org].[TMP_DepartmentType] ADD  CONSTRAINT [TMP_DF_DepartmentType_ArchetypeID]  DEFAULT (NULL) FOR [ArchetypeID]
GO
ALTER TABLE [org].[TMP_DepartmentType] ADD  CONSTRAINT [TMP_DF_DepartmentType_ParentID]  DEFAULT (NULL) FOR [ParentID]
GO
ALTER TABLE [org].[TMP_H_D] ADD  CONSTRAINT [TMP_DF_H_D_PathName]  DEFAULT ('') FOR [PathName]
GO
ALTER TABLE [org].[TMP_H_D] ADD  CONSTRAINT [TMP_DF_H_D_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [org].[TMP_H_D] ADD  CONSTRAINT [TMP_DF_H_D_Deleted]  DEFAULT ((0)) FOR [Deleted]
GO
ALTER TABLE [org].[TMP_Hierarchy] ADD  CONSTRAINT [TMP_DF_Hierarchy_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [org].[TMP_Hierarchy] ADD  CONSTRAINT [TMP_DF_Hierarchy_Description]  DEFAULT ('') FOR [Description]
GO
ALTER TABLE [org].[TMP_Hierarchy] ADD  CONSTRAINT [TMP_DF_Hierarchy_Type]  DEFAULT ((0)) FOR [Type]
GO
ALTER TABLE [org].[TMP_Hierarchy] ADD  CONSTRAINT [TMP_DF_Hierarchy_Deleted]  DEFAULT ((0)) FOR [Deleted]
GO
ALTER TABLE [org].[TMP_Hierarchy] ADD  CONSTRAINT [TMP_DF_Hierarchy_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [org].[TMP_LT_DepartmentType] ADD  CONSTRAINT [TMP_DF_LT_DepartmentType_Description]  DEFAULT ('') FOR [Description]
GO
ALTER TABLE [org].[TMP_Owner] ADD  CONSTRAINT [TMP_DF_Owner_LanguageID]  DEFAULT ((1)) FOR [LanguageID]
GO
ALTER TABLE [org].[TMP_Owner] ADD  CONSTRAINT [TMP_DF_Owner_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [org].[TMP_Owner] ADD  CONSTRAINT [TMP_DF_Owner_ReportServer]  DEFAULT ('') FOR [ReportServer]
GO
ALTER TABLE [org].[TMP_Owner] ADD  CONSTRAINT [TMP_DF_Owner_ReportDB]  DEFAULT ('') FOR [ReportDB]
GO
ALTER TABLE [org].[TMP_Owner] ADD  CONSTRAINT [TMP_DF_Owner_OLAPServer]  DEFAULT ('') FOR [OLAPServer]
GO
ALTER TABLE [org].[TMP_Owner] ADD  CONSTRAINT [TMP_DF_Owner_OLAPDB]  DEFAULT ('') FOR [OLAPDB]
GO
ALTER TABLE [org].[TMP_Owner] ADD  CONSTRAINT [TMP_DF_Owner_Css]  DEFAULT ('') FOR [Css]
GO
ALTER TABLE [org].[TMP_Owner] ADD  CONSTRAINT [TMP_DF_Owner_Url]  DEFAULT ('') FOR [Url]
GO
ALTER TABLE [org].[TMP_Owner] ADD  CONSTRAINT [TMP_DF_Owner_LoginType]  DEFAULT ((0)) FOR [LoginType]
GO
ALTER TABLE [org].[TMP_Owner] ADD  CONSTRAINT [TMP_DF_Owner_Prefix]  DEFAULT ('') FOR [Prefix]
GO
ALTER TABLE [org].[TMP_Owner] ADD  CONSTRAINT [TMP_DF_Owner_Description]  DEFAULT ('') FOR [Description]
GO
ALTER TABLE [org].[TMP_Owner] ADD  CONSTRAINT [TMP_DF_Owner_Logging]  DEFAULT ((1)) FOR [Logging]
GO
ALTER TABLE [org].[TMP_UGMember] ADD  CONSTRAINT [TMP_UDF_UG_U_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [org].[TMP_UGMember] ADD  CONSTRAINT [TMP_UDF_UG_U_CreatedBy]  DEFAULT (NULL) FOR [CreatedBy]
GO
ALTER TABLE [org].[TMP_UGMember] ADD  CONSTRAINT [TMP_UDF_UG_U_LastUpdated]  DEFAULT (getdate()) FOR [LastUpdated]
GO
ALTER TABLE [org].[TMP_UGMember] ADD  CONSTRAINT [TMP_UDF_UG_U_LastUpdatedBy]  DEFAULT (NULL) FOR [LastUpdatedBy]
GO
ALTER TABLE [org].[TMP_UGMember] ADD  CONSTRAINT [TMP_UDF_UG_U_LastSynchronized]  DEFAULT (getdate()) FOR [LastSynchronized]
GO
ALTER TABLE [org].[TMP_UGMember] ADD  CONSTRAINT [TMP_UDF_UG_U_EntityStatus]  DEFAULT (NULL) FOR [EntityStatusID]
GO
ALTER TABLE [org].[TMP_UGMember] ADD  CONSTRAINT [TMP_UDF_UG_U_EntityStatusReason]  DEFAULT (NULL) FOR [EntityStatusReasonID]
GO
ALTER TABLE [org].[TMP_UGMember] ADD  CONSTRAINT [TMP_UDF_UG_U_Customer]  DEFAULT (NULL) FOR [CustomerID]
GO
ALTER TABLE [org].[TMP_UGMember] ADD  CONSTRAINT [TMP_UDF_UG_U_Period]  DEFAULT (NULL) FOR [PeriodID]
GO
ALTER TABLE [org].[TMP_UGMember] ADD  CONSTRAINT [TMP_UDF_UG_U_ExtID]  DEFAULT ('') FOR [ExtID]
GO
ALTER TABLE [org].[TMP_UGMember] ADD  CONSTRAINT [TMP_UDF_UGMember_DisplayName]  DEFAULT ('') FOR [DisplayName]
GO
ALTER TABLE [org].[TMP_UGMember] ADD  CONSTRAINT [TMP_UDF_UGMember_ReferrerResource]  DEFAULT ('') FOR [ReferrerResource]
GO
ALTER TABLE [org].[TMP_UGMember] ADD  CONSTRAINT [TMP_UDF_UGMember_ReferrerToken]  DEFAULT ('') FOR [ReferrerToken]
GO
ALTER TABLE [org].[TMP_UGMember] ADD  CONSTRAINT [TMP_UDF_UGMember_ReferrerArchetypeID]  DEFAULT (NULL) FOR [ReferrerArchetypeID]
GO
ALTER TABLE [org].[TMP_User] ADD  CONSTRAINT [DF_ISAUser_DefLangID]  DEFAULT ((1)) FOR [LanguageID]
GO
ALTER TABLE [org].[TMP_User] ADD  CONSTRAINT [DF_User_LastName]  DEFAULT ('') FOR [LastName]
GO
ALTER TABLE [org].[TMP_User] ADD  CONSTRAINT [DF_User_FirstName]  DEFAULT ('') FOR [FirstName]
GO
ALTER TABLE [org].[TMP_User] ADD  CONSTRAINT [DF_User_Email]  DEFAULT ('') FOR [Email]
GO
ALTER TABLE [org].[TMP_User] ADD  CONSTRAINT [DF_User_Mobile]  DEFAULT ('') FOR [Mobile]
GO
ALTER TABLE [org].[TMP_User] ADD  CONSTRAINT [DF_User_EmpNo]  DEFAULT ('') FOR [ExtID]
GO
ALTER TABLE [org].[TMP_User] ADD  CONSTRAINT [DF_User_Tag]  DEFAULT ('') FOR [Tag]
GO
ALTER TABLE [org].[TMP_User] ADD  CONSTRAINT [DF__User__Locked__174363E2]  DEFAULT ((0)) FOR [Locked]
GO
ALTER TABLE [org].[TMP_User] ADD  CONSTRAINT [DF_ISAUser_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [org].[TMP_User] ADD  CONSTRAINT [DF_User_SSN]  DEFAULT ('') FOR [SSN]
GO
ALTER TABLE [org].[TMP_User] ADD  CONSTRAINT [DF_User_ChangePassword]  DEFAULT ((0)) FOR [ChangePassword]
GO
ALTER TABLE [org].[TMP_User] ADD  CONSTRAINT [DF_User_HashPassword]  DEFAULT ('') FOR [HashPassword]
GO
ALTER TABLE [org].[TMP_User] ADD  CONSTRAINT [DF_User_SaltPassword]  DEFAULT ('') FOR [SaltPassword]
GO
ALTER TABLE [org].[TMP_User] ADD  CONSTRAINT [DF_User_OneTimePassword]  DEFAULT ('') FOR [OneTimePassword]
GO
ALTER TABLE [org].[TMP_User] ADD  CONSTRAINT [DF_User_OTPExpireTime]  DEFAULT (getdate()) FOR [OTPExpireTime]
GO
ALTER TABLE [org].[TMP_User] ADD  CONSTRAINT [DF_User_ForceUserLoginAgain]  DEFAULT ((0)) FOR [ForceUserLoginAgain]
GO
ALTER TABLE [org].[TMP_User] ADD  CONSTRAINT [DF_User_LastUpdated]  DEFAULT (getdate()) FOR [LastUpdated]
GO
ALTER TABLE [org].[TMP_User] ADD  CONSTRAINT [DF_User_LastUpdatedBy]  DEFAULT (NULL) FOR [LastUpdatedBy]
GO
ALTER TABLE [org].[TMP_User] ADD  CONSTRAINT [DF_User_LastSynchronized]  DEFAULT (getdate()) FOR [LastSynchronized]
GO
ALTER TABLE [org].[TMP_User] ADD  CONSTRAINT [DF_User_ArchetypeID]  DEFAULT (NULL) FOR [ArchetypeID]
GO
ALTER TABLE [org].[TMP_User] ADD  CONSTRAINT [DF_User_Deleted]  DEFAULT (NULL) FOR [Deleted]
GO
ALTER TABLE [org].[TMP_User] ADD  CONSTRAINT [DF_User_EntityStatus]  DEFAULT (NULL) FOR [EntityStatusID]
GO
ALTER TABLE [org].[TMP_User] ADD  CONSTRAINT [DF_User_EntityStatusReason]  DEFAULT (NULL) FOR [EntityStatusReasonID]
GO
ALTER TABLE [org].[TMP_UserGroup] ADD  CONSTRAINT [DF_TMP_UserGroup_Description]  DEFAULT ('') FOR [Description]
GO
ALTER TABLE [org].[TMP_UserGroup] ADD  CONSTRAINT [DF_TMP_UserGroup_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [org].[TMP_UserGroup] ADD  CONSTRAINT [DF_TMP_UserGroup_ExtID]  DEFAULT ('') FOR [ExtID]
GO
ALTER TABLE [org].[TMP_UserGroup] ADD  CONSTRAINT [DF_TMP_UserGroup_Tag]  DEFAULT ('') FOR [Tag]
GO
ALTER TABLE [org].[TMP_UserGroup] ADD  CONSTRAINT [DF_TMP_UserGroup_LastUpdated]  DEFAULT (getdate()) FOR [LastUpdated]
GO
ALTER TABLE [org].[TMP_UserGroup] ADD  CONSTRAINT [DF_TMP_UserGroup_LastUpdatedBy]  DEFAULT (NULL) FOR [LastUpdatedBy]
GO
ALTER TABLE [org].[TMP_UserGroup] ADD  CONSTRAINT [DF_TMP_UserGroup_LastSynchronized]  DEFAULT (getdate()) FOR [LastSynchronized]
GO
ALTER TABLE [org].[TMP_UserGroup] ADD  CONSTRAINT [DF_TMP_Usergroup_ArchetypeID]  DEFAULT (NULL) FOR [ArchetypeID]
GO
ALTER TABLE [org].[TMP_UserGroup] ADD  CONSTRAINT [DF_TMP_UserGroup_Deleted]  DEFAULT (NULL) FOR [Deleted]
GO
ALTER TABLE [org].[TMP_UserGroup] ADD  CONSTRAINT [DF_TMP_UserGroup_EntityStatus]  DEFAULT (NULL) FOR [EntityStatusID]
GO
ALTER TABLE [org].[TMP_UserGroup] ADD  CONSTRAINT [DF_TMP_UserGroup_EntityStatusReason]  DEFAULT (NULL) FOR [EntityStatusReasonID]
GO
ALTER TABLE [org].[UGMember] ADD  CONSTRAINT [DF_UG_U_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [org].[UGMember] ADD  CONSTRAINT [DF_UG_U_CreatedBy]  DEFAULT (NULL) FOR [CreatedBy]
GO
ALTER TABLE [org].[UGMember] ADD  CONSTRAINT [DF_UG_U_LastUpdated]  DEFAULT (getdate()) FOR [LastUpdated]
GO
ALTER TABLE [org].[UGMember] ADD  CONSTRAINT [DF_UG_U_LastUpdatedBy]  DEFAULT (NULL) FOR [LastUpdatedBy]
GO
ALTER TABLE [org].[UGMember] ADD  CONSTRAINT [DF_UG_U_LastSynchronized]  DEFAULT (getdate()) FOR [LastSynchronized]
GO
ALTER TABLE [org].[UGMember] ADD  CONSTRAINT [DF_UG_U_EntityStatus]  DEFAULT (NULL) FOR [EntityStatusID]
GO
ALTER TABLE [org].[UGMember] ADD  CONSTRAINT [DF_UG_U_EntityStatusReason]  DEFAULT (NULL) FOR [EntityStatusReasonID]
GO
ALTER TABLE [org].[UGMember] ADD  CONSTRAINT [DF_UG_U_Customer]  DEFAULT (NULL) FOR [CustomerID]
GO
ALTER TABLE [org].[UGMember] ADD  CONSTRAINT [DF_UG_U_Period]  DEFAULT (NULL) FOR [PeriodID]
GO
ALTER TABLE [org].[UGMember] ADD  CONSTRAINT [DF_UG_U_ExtID]  DEFAULT ('') FOR [ExtID]
GO
ALTER TABLE [org].[UGMember] ADD  CONSTRAINT [DF_UGMember_DisplayName]  DEFAULT ('') FOR [DisplayName]
GO
ALTER TABLE [org].[UGMember] ADD  CONSTRAINT [DF_UGMember_ReferrerResource]  DEFAULT ('') FOR [ReferrerResource]
GO
ALTER TABLE [org].[UGMember] ADD  CONSTRAINT [DF_UGMember_ReferrerToken]  DEFAULT ('') FOR [ReferrerToken]
GO
ALTER TABLE [org].[UGMember] ADD  CONSTRAINT [DF_UGMember_ReferrerArchetypeID]  DEFAULT (NULL) FOR [ReferrerArchetypeID]
GO
ALTER TABLE [org].[User] ADD  CONSTRAINT [TMP_DF_ISAUser_DefLangID]  DEFAULT ((1)) FOR [LanguageID]
GO
ALTER TABLE [org].[User] ADD  CONSTRAINT [TMP_DF_User_LastName]  DEFAULT ('') FOR [LastName]
GO
ALTER TABLE [org].[User] ADD  CONSTRAINT [TMP_DF_User_FirstName]  DEFAULT ('') FOR [FirstName]
GO
ALTER TABLE [org].[User] ADD  CONSTRAINT [TMP_DF_User_Email]  DEFAULT ('') FOR [Email]
GO
ALTER TABLE [org].[User] ADD  CONSTRAINT [TMP_DF_User_Mobile]  DEFAULT ('') FOR [Mobile]
GO
ALTER TABLE [org].[User] ADD  CONSTRAINT [TMP_DF_User_EmpNo]  DEFAULT ('') FOR [ExtID]
GO
ALTER TABLE [org].[User] ADD  CONSTRAINT [TMP_DF_User_Tag]  DEFAULT ('') FOR [Tag]
GO
ALTER TABLE [org].[User] ADD  CONSTRAINT [TMP_DF__User__Locked__174363E2]  DEFAULT ((0)) FOR [Locked]
GO
ALTER TABLE [org].[User] ADD  CONSTRAINT [TMP_DF_ISAUser_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [org].[User] ADD  CONSTRAINT [TMP_DF_User_SSN]  DEFAULT ('') FOR [SSN]
GO
ALTER TABLE [org].[User] ADD  CONSTRAINT [TMP_DF_User_ChangePassword]  DEFAULT ((0)) FOR [ChangePassword]
GO
ALTER TABLE [org].[User] ADD  CONSTRAINT [TMP_DF_User_HashPassword]  DEFAULT ('') FOR [HashPassword]
GO
ALTER TABLE [org].[User] ADD  CONSTRAINT [TMP_DF_User_SaltPassword]  DEFAULT ('') FOR [SaltPassword]
GO
ALTER TABLE [org].[User] ADD  CONSTRAINT [TMP_DF_User_OneTimePassword]  DEFAULT ('') FOR [OneTimePassword]
GO
ALTER TABLE [org].[User] ADD  CONSTRAINT [TMP_DF_User_OTPExpireTime]  DEFAULT (getdate()) FOR [OTPExpireTime]
GO
ALTER TABLE [org].[User] ADD  CONSTRAINT [TMP_DF_User_ForceUserLoginAgain]  DEFAULT ((0)) FOR [ForceUserLoginAgain]
GO
ALTER TABLE [org].[User] ADD  CONSTRAINT [TMP_DF_User_LastUpdated]  DEFAULT (getdate()) FOR [LastUpdated]
GO
ALTER TABLE [org].[User] ADD  CONSTRAINT [TMP_DF_User_LastUpdatedBy]  DEFAULT (NULL) FOR [LastUpdatedBy]
GO
ALTER TABLE [org].[User] ADD  CONSTRAINT [TMP_DF_User_LastSynchronized]  DEFAULT (getdate()) FOR [LastSynchronized]
GO
ALTER TABLE [org].[User] ADD  CONSTRAINT [TMP_DF_User_ArchetypeID]  DEFAULT (NULL) FOR [ArchetypeID]
GO
ALTER TABLE [org].[User] ADD  CONSTRAINT [TMP_DF_User_Deleted]  DEFAULT (NULL) FOR [Deleted]
GO
ALTER TABLE [org].[User] ADD  CONSTRAINT [TMP_DF_User_EntityStatus]  DEFAULT (NULL) FOR [EntityStatusID]
GO
ALTER TABLE [org].[User] ADD  CONSTRAINT [TMP_DF_User_EntityStatusReason]  DEFAULT (NULL) FOR [EntityStatusReasonID]
GO
ALTER TABLE [org].[UserGroup] ADD  CONSTRAINT [DF_UserGroup_Description]  DEFAULT ('') FOR [Description]
GO
ALTER TABLE [org].[UserGroup] ADD  CONSTRAINT [DF_UserGroup_Created]  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [org].[UserGroup] ADD  CONSTRAINT [DF_UserGroup_ExtID]  DEFAULT ('') FOR [ExtID]
GO
ALTER TABLE [org].[UserGroup] ADD  CONSTRAINT [DF_UserGroup_Tag]  DEFAULT ('') FOR [Tag]
GO
ALTER TABLE [org].[UserGroup] ADD  CONSTRAINT [DF_UserGroup_LastUpdated]  DEFAULT (getdate()) FOR [LastUpdated]
GO
ALTER TABLE [org].[UserGroup] ADD  CONSTRAINT [DF_UserGroup_LastUpdatedBy]  DEFAULT (NULL) FOR [LastUpdatedBy]
GO
ALTER TABLE [org].[UserGroup] ADD  CONSTRAINT [DF_UserGroup_LastSynchronized]  DEFAULT (getdate()) FOR [LastSynchronized]
GO
ALTER TABLE [org].[UserGroup] ADD  CONSTRAINT [DF_Usergroup_ArchetypeID]  DEFAULT (NULL) FOR [ArchetypeID]
GO
ALTER TABLE [org].[UserGroup] ADD  CONSTRAINT [DF_UserGroup_Deleted]  DEFAULT (NULL) FOR [Deleted]
GO
ALTER TABLE [org].[UserGroup] ADD  CONSTRAINT [DF_UserGroup_EntityStatus]  DEFAULT (NULL) FOR [EntityStatusID]
GO
ALTER TABLE [org].[UserGroup] ADD  CONSTRAINT [DF_UserGroup_EntityStatusReason]  DEFAULT (NULL) FOR [EntityStatusReasonID]
GO
/****** Object:  StoredProcedure [at].[Truncate_Tables]    Script Date: 12.04.2019 13.14.37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [at].[Truncate_Tables]
AS
BEGIN
	
	SET XACT_ABORT,NOCOUNT ON;

    TRUNCATE TABLE [at].[TMP_Activity]
	TRUNCATE TABLE [at].[TMP_LT_Activity]
	TRUNCATE TABLE [at].[TMP_Survey]
	TRUNCATE TABLE [at].[TMP_LT_Survey]
	TRUNCATE TABLE [at].[TMP_Period]
	TRUNCATE TABLE [at].[TMP_Page]
	TRUNCATE TABLE [at].[TMP_LT_Page]
	TRUNCATE TABLE [at].[TMP_Question]
	TRUNCATE TABLE [at].[TMP_LT_Question]
	TRUNCATE TABLE [at].[TMP_Scale]
	TRUNCATE TABLE [at].[TMP_LT_Scale]
	TRUNCATE TABLE [at].[TMP_Alternative]
	TRUNCATE TABLE [at].[TMP_LT_Alternative]
	TRUNCATE TABLE [at].[TMP_Category]
	TRUNCATE TABLE [at].[TMP_LT_Category]
	TRUNCATE TABLE [at].[TMP_Q_C]
	TRUNCATE TABLE [at].[TMP_LevelLimit]
	TRUNCATE TABLE [at].[TMP_LT_LevelLimit]
	TRUNCATE TABLE [at].[TMP_LevelGroup]
	TRUNCATE TABLE [at].[TMP_LT_LevelGroup]
	TRUNCATE TABLE [at].[TMP_Role]
	TRUNCATE TABLE [at].[TMP_LT_Role]
	TRUNCATE TABLE [at].[TMP_Batch]

	TRUNCATE TABLE [org].[TMP_Owner]
	TRUNCATE TABLE [org].[TMP_Customer]
	TRUNCATE TABLE [org].[TMP_Department]
	TRUNCATE TABLE [org].[TMP_DepartmentType]
	TRUNCATE TABLE [org].[TMP_LT_DepartmentType]
	TRUNCATE TABLE [org].[TMP_Hierarchy]
	TRUNCATE TABLE [org].[TMP_H_D]
	TRUNCATE TABLE [org].[TMP_DT_D]
	TRUNCATE TABLE [org].[TMP_User]
	TRUNCATE TABLE [org].[TMP_UG_U]
	TRUNCATE TABLE [org].[TMP_UserGroup]
	TRUNCATE TABLE [org].[TMP_UserType]
	TRUNCATE TABLE [org].[TMP_LT_UserType]
	TRUNCATE TABLE [org].[TMP_UT_U]
	TRUNCATE TABLE [org].[TMP_UserGroupType]
	TRUNCATE TABLE [org].[TMP_LT_UserGroupType]
	TRUNCATE TABLE [org].[TMP_user_referrerToken]
	TRUNCATE TABLE [org].[TMP_UGMember]

	TRUNCATE TABLE [dbo].[TMP_Result]
	TRUNCATE TABLE [dbo].[TMP_Answer]
	TRUNCATE TABLE [dbo].[TMP_Archetype]
	TRUNCATE TABLE [dbo].[TMP_Language]
	TRUNCATE TABLE [dbo].[TMP_EntityStatus]
	TRUNCATE TABLE [dbo].[TMP_LT_EntityStatus]
	TRUNCATE TABLE [dbo].[TMP_EntityStatusReason]
	TRUNCATE TABLE [dbo].[TMP_LT_EntityStatusReason]

	Truncate Table [at].[TMP_XCategory]
	Truncate Table [at].[TMP_LT_XCategory]
	Truncate Table [at].[TMP_XC_A]


	----
	TRUNCATE TABLE [dbo].[TMP_Customer_Activity]
	TRUNCATE TABLE [dbo].[TMP_Customer_Category]
	TRUNCATE TABLE [dbo].[TMP_Customer_Page]
	TRUNCATE TABLE [dbo].[TMP_Customer_Question]


	truncate table dbo.TMP_ProveResultater
END



GO
/****** Object:  StoredProcedure [dbo].[Create_Indexes]    Script Date: 12.04.2019 13.14.37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[Create_Indexes]
as
BEGIN


IF EXISTS (SELECT 1
            FROM sys.indexes I
                INNER JOIN sys.tables T
                    ON I.object_id = T.object_id
                INNER JOIN sys.schemas S
                    ON S.schema_id = T.schema_id
            WHERE I.Name = 'IX_Customer' -- Index name
                AND T.Name = 'TMP_Result' -- Table name
                AND S.Name = 'dbo') --Schema Name
BEGIN
    DROP INDEX [IX_Customer] ON [dbo].[TMP_Result]
END

IF EXISTS (SELECT 1
            FROM sys.indexes I
                INNER JOIN sys.tables T
                    ON I.object_id = T.object_id
                INNER JOIN sys.schemas S
                    ON S.schema_id = T.schema_id
            WHERE I.Name = 'IX_Customer' -- Index name
                AND T.Name = 'TMP_Answer' -- Table name
                AND S.Name = 'dbo') --Schema Name
BEGIN
    DROP INDEX [IX_Customer] ON [dbo].[TMP_Answer]
END

CREATE NONCLUSTERED INDEX [IX_Customer] ON [dbo].[TMP_Result]
(
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


CREATE NONCLUSTERED INDEX [IX_Customer] ON [dbo].[TMP_Answer]
(
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


END



GO
/****** Object:  StoredProcedure [dbo].[Drop_Indexes]    Script Date: 12.04.2019 13.14.37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[Drop_Indexes]
as
BEGIN


/****** Object:  Index [IX_Customer]    Script Date: 10.05.2017 08.57.25 ******/
DROP INDEX [IX_Customer] ON [dbo].[TMP_Result]

DROP INDEX [IX_Customer] ON [dbo].[TMP_Answer]



END
 


GO
/****** Object:  StoredProcedure [dbo].[populate]    Script Date: 12.04.2019 13.14.37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







CREATE PROCEDURE [dbo].[populate]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	truncate table [dbo].[TMP_Customer_Activity]
    Insert into [dbo].[TMP_Customer_Activity] (CustomerID, ActivityID)
	Select distinct isnull(r.customerid,0) as CustomerID,a.activityID 
	from at.TMP_activity a
	Join at.TMP_survey s on a.activityID = s.activityID
	Join dbo.TMP_result r on s.surveyid = r.surveyid


	truncate table [dbo].[TMP_Customer_Page]

   insert into [dbo].[TMP_Customer_Page] (CustomerID, PageID)
Select Distinct  qR.customerid, q.pageid
from dbo.TMP_answer Qr (nolock)
Join at.TMP_Question q (nolock) on qr.Questionid = q.Questionid 
where q.PageID  in (select PageID from at.TMP_Page) and qr.customerid is not null
OPTION (MAXDOP 8)

 truncate table dbo.TMP_Customer_Question

Insert into dbo.TMP_Customer_Question (QuestionID, CustomerID)
Select distinct QuestionID, CustomerID 
from  TMP_Answer 
Where QuestionID in (Select QuestionID from at.TMP_Question)
and CustomerID is not null
order by Customerid
OPTION (MAXDOP 8)

Delete from TMP_result where departmentid = 18448

update TMP_result set departmentid = 
( Select hd2.departmentid from org.TMP_H_D  hd 
   Join org.TMP_H_D hd2 on hd.parentid = hd2.hdid
  where hd.departmentid = r.departmentid)
from TMP_result r
left outer join [external].[Department] d on d.departmentid = r.departmentid
where d.departmentid is null and r.departmentid <> 18448 and ( Select hd2.departmentid from org.TMP_H_D  hd 
   Join org.TMP_H_D hd2 on hd.parentid = hd2.hdid
  where hd.departmentid = r.departmentid) is not null






--Delete from dbo.TMP_result where userid in (
--Select userid from org.[TMP_user] where entitystatusid = 3
--)


--Delete from org.TMP_ug_u where userid in (
--Select userid from org.[TMP_user] where entitystatusid = 3
--)

--Delete from org.[TMP_user] where userid in (
--Select userid from org.[TMP_user] where entitystatusid = 3
--)



Delete from org.TMP_H_D  where deleted  = 1

Delete from org.TMP_department where entitystatusid = 3

Delete from org.[TMP_user] where departmentid not in
(
 Select departmentid from  org.TMP_department
)


Delete from org.TMP_usergroup where entitystatusid = 3

Delete from org.TMP_ug_u where usergroupid not in
(
 select usergroupid from org.TMP_usergroup
)


Delete from org.TMP_ug_u where userid not in
(
 select userid from org.[TMP_user]
)

Delete from dbo.TMP_result where entitystatusid = 3
--Delete from dbo.TMP_result where userid not in
--(
-- select userid from org.[TMP_user]
--)

Delete from dbo.TMP_answer where resultid not in
(
 select resultid from dbo.TMP_result
)


--insert into [org].[TMP_user_referrerToken] (userid,refererToken)
--Select userid,refererToken from [org].[TMP_user_refererToken]

--insert into [org].[TMP_user_referrerToken] (userid,refererToken)
--Select userid,'2001:100' from [org].[TMP_user] where EntityStatusID = 1 and deleted is null

insert into [dbo].[TMP_ProveResultater] (Periode, Customerid, Kunde, Skole,departmentid, Forlag, Hovedområde, Underområde, [Prøve/Verktøy], Trinn, Måned, År, Resultater)
select p.ExtID AS Periode, c.Customerid,
       Replace(c.Name, ',', '') AS Kunde, 
       Replace(b.Name, ',', '') AS Skole,
	   b.departmentid as Departmentid, 
       isnull(lxForlag.Name, '') AS Forlag,
       isnull(lxHoved.Name, '') AS Hovedområde,
       isnull(lxUnder.Name, '') AS Underområde,
       Replace(la.Name, ',', '') AS 'Prøve/Verktøy', 
	   lr.Name AS Trinn,
       DATEPART(month, r.Created) AS Måned, DATEPART(year, r.Created) AS År, 
       count(r.ResultID) AS Resultater

from dbo.Result r
JOIN at.Survey s ON r.SurveyID = s.SurveyID
JOIN at.LT_Activity la ON s.ActivityID = la.ActivityID AND la.LanguageID = 1
JOIN at.Period p ON s.PeriodID = p.PeriodID
JOIN at.Batch b ON r.BatchID = b.BatchID
Join org.Department d on d.DepartmentID = r.DepartmentID
JOIN org.Customer c ON d.CustomerID = c.CustomerID
JOIN at.LT_Role lr ON r.RoleID = lr.RoleID AND lr.LanguageID = 1
LEFT JOIN at.XC_A xForlag ON s.ActivityID = xForlag.ActivityID AND xForlag.XCID IN (105, 109, 115)
LEFT JOIN at.LT_XCategory lxForlag ON xForlag.XCID = lxForlag.XCID AND lxForlag.LanguageID = 1
LEFT JOIN at.XC_A xHoved ON s.ActivityID = xHoved.ActivityID AND xHoved.XCID IN (80, 154, 147, 159, 175, 307, 319)
LEFT JOIN at.LT_XCategory lxHoved ON xHoved.XCID = lxHoved.XCID AND lxHoved.LanguageID = 1
LEFT JOIN at.XC_A xUnder ON s.ActivityID = xUnder.ActivityID AND xUnder.XCID IN (SELECT x.XCID FROM at.XCategory x WHERE x.ParentID = xHoved.XCID )
LEFT JOIN at.LT_XCategory lxUnder ON xUnder.XCID = lxUnder.XCID AND lxUnder.LanguageID = 1
WHERE  r.EntityStatusID = 1 AND r.StatusTypeID IN (1,4,6)
GROUP BY p.ExtID,c.customerid, c.Name, b.Name,b.departmentid, lxForlag.Name, lxHoved.Name, isnull(lxUnder.Name, ''), la.Name, lr.Name, DATEPART(month, r.Created), DATEPART(year, r.Created)




END




GO
/****** Object:  StoredProcedure [dbo].[Rename_Tables]    Script Date: 12.04.2019 13.14.37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[Rename_Tables]
	
AS
BEGIN
	
	SET NOCOUNT ON;

	EXEC sp_rename 'at.Activity', 'Activity2'
	EXEC sp_rename 'at.TMP_Activity', 'Activity'
	EXEC sp_rename 'at.Activity2', 'TMP_Activity'

	EXEC sp_rename 'at.Alternative', 'Alternative2'
	EXEC sp_rename 'at.TMP_Alternative', 'Alternative'
	EXEC sp_rename 'at.Alternative2', 'TMP_Alternative'

	EXEC sp_rename 'at.Batch', 'Batch2'
	EXEC sp_rename 'at.TMP_Batch', 'Batch'
	EXEC sp_rename 'at.Batch2', 'TMP_Batch'

	EXEC sp_rename 'at.Category', 'Category2'
	EXEC sp_rename 'at.TMP_Category', 'Category'
	EXEC sp_rename 'at.Category2', 'TMP_Category'

	EXEC sp_rename 'at.LevelGroup', 'LevelGroup2'
	EXEC sp_rename 'at.TMP_LevelGroup', 'LevelGroup'
	EXEC sp_rename 'at.LevelGroup2', 'TMP_LevelGroup'

	EXEC sp_rename 'at.LevelLimit', 'LevelLimit2'
	EXEC sp_rename 'at.TMP_LevelLimit', 'LevelLimit'
	EXEC sp_rename 'at.LevelLimit2', 'TMP_LevelLimit'

	EXEC sp_rename 'at.LT_Activity', 'LT_Activity2'
	EXEC sp_rename 'at.TMP_LT_Activity', 'LT_Activity'
	EXEC sp_rename 'at.LT_Activity2', 'TMP_LT_Activity'

	EXEC sp_rename 'at.LT_Alternative', 'LT_Alternative2'
	EXEC sp_rename 'at.TMP_LT_Alternative', 'LT_Alternative'
	EXEC sp_rename 'at.LT_Alternative2', 'TMP_LT_Alternative'

	EXEC sp_rename 'at.LT_Category', 'LT_Category2'
	EXEC sp_rename 'at.TMP_LT_Category', 'LT_Category'
	EXEC sp_rename 'at.LT_Category2', 'TMP_LT_Category'

	EXEC sp_rename 'at.LT_LevelGroup', 'LT_LevelGroup2'
	EXEC sp_rename 'at.TMP_LT_LevelGroup', 'LT_LevelGroup'
	EXEC sp_rename 'at.LT_LevelGroup2', 'TMP_LT_LevelGroup'

	EXEC sp_rename 'at.LT_LevelLimit', 'LT_LevelLimit2'
	EXEC sp_rename 'at.TMP_LT_LevelLimit', 'LT_LevelLimit'
	EXEC sp_rename 'at.LT_LevelLimit2', 'TMP_LT_LevelLimit'

	EXEC sp_rename 'at.LT_Page', 'LT_Page2'
	EXEC sp_rename 'at.TMP_LT_Page', 'LT_Page'
	EXEC sp_rename 'at.LT_Page2', 'TMP_LT_Page'

	EXEC sp_rename 'at.LT_Question', 'LT_Question2'
	EXEC sp_rename 'at.TMP_LT_Question', 'LT_Question'
	EXEC sp_rename 'at.LT_Question2', 'TMP_LT_Question'
	
	EXEC sp_rename 'at.LT_Role', 'LT_Role2'
	EXEC sp_rename 'at.TMP_LT_Role', 'LT_Role'
	EXEC sp_rename 'at.LT_Role2', 'TMP_LT_Role'

	EXEC sp_rename 'at.LT_Scale', 'LT_Scale2'
	EXEC sp_rename 'at.TMP_LT_Scale', 'LT_Scale'
	EXEC sp_rename 'at.LT_Scale2', 'TMP_LT_Scale'

	EXEC sp_rename 'at.LT_Survey', 'LT_Survey2'
	EXEC sp_rename 'at.TMP_LT_Survey', 'LT_Survey'
	EXEC sp_rename 'at.LT_Survey2', 'TMP_LT_Survey'

	EXEC sp_rename 'at.Page', 'Page2'
	EXEC sp_rename 'at.TMP_Page', 'Page'
	EXEC sp_rename 'at.Page2', 'TMP_Page'

	EXEC sp_rename 'at.Period', 'Period2'
	EXEC sp_rename 'at.TMP_Period', 'Period'
	EXEC sp_rename 'at.Period2', 'TMP_Period'

	EXEC sp_rename 'at.Q_C', 'Q_C2'
	EXEC sp_rename 'at.TMP_Q_C', 'Q_C'
	EXEC sp_rename 'at.Q_C2', 'TMP_Q_C'

	EXEC sp_rename 'at.Question', 'Question2'
	EXEC sp_rename 'at.TMP_Question', 'Question'
	EXEC sp_rename 'at.Question2', 'TMP_Question'

	EXEC sp_rename 'at.Role', 'Role2'
	EXEC sp_rename 'at.TMP_Role', 'Role'
	EXEC sp_rename 'at.Role2', 'TMP_Role'

	EXEC sp_rename 'at.Scale', 'Scale2'
	EXEC sp_rename 'at.TMP_Scale', 'Scale'
	EXEC sp_rename 'at.Scale2', 'TMP_Scale'

	EXEC sp_rename 'at.Survey', 'Survey2'
	EXEC sp_rename 'at.TMP_Survey', 'Survey'
	EXEC sp_rename 'at.Survey2', 'TMP_Survey'

	EXEC sp_rename 'dbo.Answer', 'Answer2'
	EXEC sp_rename 'dbo.TMP_Answer', 'Answer'
	EXEC sp_rename 'dbo.Answer2', 'TMP_Answer'

	EXEC sp_rename 'dbo.Archetype', 'Archetype2'
	EXEC sp_rename 'dbo.TMP_Archetype', 'Archetype'
	EXEC sp_rename 'dbo.Archetype2', 'TMP_Archetype'

	EXEC sp_rename 'dbo.Customer_Activity', 'Customer_Activity2'
	EXEC sp_rename 'dbo.TMP_Customer_Activity', 'Customer_Activity'
	EXEC sp_rename 'dbo.Customer_Activity2', 'TMP_Customer_Activity'

	EXEC sp_rename 'dbo.Customer_Category', 'Customer_Category2'
	EXEC sp_rename 'dbo.TMP_Customer_Category', 'Customer_Category'
	EXEC sp_rename 'dbo.Customer_Category2', 'TMP_Customer_Category'

	EXEC sp_rename 'dbo.Customer_Page', 'Customer_Page2'
	EXEC sp_rename 'dbo.TMP_Customer_Page', 'Customer_Page'
	EXEC sp_rename 'dbo.Customer_Page2', 'TMP_Customer_Page'

	EXEC sp_rename 'dbo.Customer_Question', 'Customer_Question2'
	EXEC sp_rename 'dbo.TMP_Customer_Question', 'Customer_Question'
	EXEC sp_rename 'dbo.Customer_Question2', 'TMP_Customer_Question'

	EXEC sp_rename 'dbo.EntityStatus', 'EntityStatus2'
	EXEC sp_rename 'dbo.TMP_EntityStatus', 'EntityStatus'
	EXEC sp_rename 'dbo.EntityStatus2', 'TMP_EntityStatus'

	EXEC sp_rename 'dbo.EntityStatusReason', 'EntityStatusReason2'
	EXEC sp_rename 'dbo.TMP_EntityStatusReason', 'EntityStatusReason'
	EXEC sp_rename 'dbo.EntityStatusReason2', 'TMP_EntityStatusReason'

	EXEC sp_rename 'dbo.Language', 'Language2'
	EXEC sp_rename 'dbo.TMP_Language', 'Language'
	EXEC sp_rename 'dbo.Language2', 'TMP_Language'

	EXEC sp_rename 'dbo.LT_EntityStatus', 'LT_EntityStatus2'
	EXEC sp_rename 'dbo.TMP_LT_EntityStatus', 'LT_EntityStatus'
	EXEC sp_rename 'dbo.LT_EntityStatus2', 'TMP_LT_EntityStatus'

	EXEC sp_rename 'dbo.LT_EntityStatusReason', 'LT_EntityStatusReason2'
	EXEC sp_rename 'dbo.TMP_LT_EntityStatusReason', 'LT_EntityStatusReason'
	EXEC sp_rename 'dbo.LT_EntityStatusReason2', 'TMP_LT_EntityStatusReason'

	EXEC sp_rename 'dbo.Result', 'Result2'
	EXEC sp_rename 'dbo.TMP_Result', 'Result'
	EXEC sp_rename 'dbo.Result2', 'TMP_Result'

	EXEC sp_rename 'org.Customer', 'Customer2'
	EXEC sp_rename 'org.TMP_Customer', 'Customer'
	EXEC sp_rename 'org.Customer2', 'TMP_Customer'

	EXEC sp_rename 'org.Department', 'Department2'
	EXEC sp_rename 'org.TMP_Department', 'Department'
	EXEC sp_rename 'org.Department2', 'TMP_Department'

	
	EXEC sp_rename 'org.DepartmentType', 'DepartmentType2'
	EXEC sp_rename 'org.TMP_DepartmentType', 'DepartmentType'
	EXEC sp_rename 'org.DepartmentType2', 'TMP_DepartmentType'

	EXEC sp_rename 'org.DT_D', 'DT_D2'
	EXEC sp_rename 'org.TMP_DT_D', 'DT_D'
	EXEC sp_rename 'org.DT_D2', 'TMP_DT_D'

	EXEC sp_rename 'org.H_D', 'H_D2'
	EXEC sp_rename 'org.TMP_H_D', 'H_D'
	EXEC sp_rename 'org.H_D2', 'TMP_H_D'

	EXEC sp_rename 'org.Hierarchy', 'Hierarchy2'
	EXEC sp_rename 'org.TMP_Hierarchy', 'Hierarchy'
	EXEC sp_rename 'org.Hierarchy2', 'TMP_Hierarchy'

	EXEC sp_rename 'org.LT_DepartmentType', 'LT_DepartmentType2'
	EXEC sp_rename 'org.TMP_LT_DepartmentType', 'LT_DepartmentType'
	EXEC sp_rename 'org.LT_DepartmentType2', 'TMP_LT_DepartmentType'

	EXEC sp_rename 'org.Owner', 'Owner2'
	EXEC sp_rename 'org.TMP_Owner', 'Owner'
	EXEC sp_rename 'org.Owner2', 'TMP_Owner'

	EXEC sp_rename 'org.UserGroup', 'UserGroup2'
	EXEC sp_rename 'org.TMP_UserGroup', 'UserGroup'
	EXEC sp_rename 'org.UserGroup2', 'TMP_UserGroup'


	EXEC sp_rename 'org.UG_U', 'UG_U2'
	EXEC sp_rename 'org.TMP_UG_U', 'UG_U'
	EXEC sp_rename 'org.UG_U2', 'TMP_UG_U'

	EXEC sp_rename 'org.UGMember', 'UGMember2'
	EXEC sp_rename 'org.TMP_UGMember', 'UGMember'
	EXEC sp_rename 'org.UGMember2', 'TMP_UGMember'


	EXEC sp_rename 'org.User', 'User2'
	EXEC sp_rename 'org.TMP_User', 'User'
	EXEC sp_rename 'org.User2', 'TMP_User'


	EXEC sp_rename 'org.UT_U', 'UT_U2'
	EXEC sp_rename 'org.TMP_UT_U', 'UT_U'
	EXEC sp_rename 'org.UT_U2', 'TMP_UT_U'

	EXEC sp_rename 'org.UserType', 'UserType2'
	EXEC sp_rename 'org.TMP_UserType', 'UserType'
	EXEC sp_rename 'org.UserType2', 'TMP_UserType'

	
	EXEC sp_rename 'org.LT_UserType', 'LT_UserType2'
	EXEC sp_rename 'org.TMP_LT_UserType', 'LT_UserType'
	EXEC sp_rename 'org.LT_UserType2', 'TMP_LT_UserType'

	EXEC sp_rename 'org.UserGroupType', 'UserGroupType2'
	EXEC sp_rename 'org.TMP_UserGroupType', 'UserGroupType'
	EXEC sp_rename 'org.UserGroupType2', 'TMP_UserGroupType'

	EXEC sp_rename 'org.LT_UserGroupType', 'LT_UserGroupType2'
	EXEC sp_rename 'org.TMP_LT_UserGroupType', 'LT_UserGroupType'
	EXEC sp_rename 'org.LT_UserGroupType2', 'TMP_LT_UserGroupType'






    EXEC sp_rename 'org.user_referrerToken', 'user_referrerToken2'
	EXEC sp_rename 'org.TMP_user_referrerToken', 'user_referrerToken'
	EXEC sp_rename 'org.user_referrerToken2', 'TMP_user_referrerToken'


	EXEC sp_rename 'at.XCategory', 'XCategory2'
	EXEC sp_rename 'at.TMP_XCategory', 'XCategory'
	EXEC sp_rename 'at.XCategory2', 'TMP_XCategory'

	EXEC sp_rename 'at.LT_XCategory', 'LT_XCategory2'
	EXEC sp_rename 'at.TMP_LT_XCategory', 'LT_XCategory'
	EXEC sp_rename 'at.LT_XCategory2', 'TMP_LT_XCategory'

	EXEC sp_rename 'at.XC_A', 'XC_A2'
	EXEC sp_rename 'at.TMP_XC_A', 'XC_A'
	EXEC sp_rename 'at.XC_A2', 'TMP_XC_A'

		EXEC sp_rename 'dbo.ProveResultater', 'ProveResultater2'
	EXEC sp_rename 'dbo.TMP_ProveResultater', 'ProveResultater'
	EXEC sp_rename 'dbo.ProveResultater2', 'TMP_ProveResultater'

	

END



GO
